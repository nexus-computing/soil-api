module.exports = {
	'*.{js,ts}': ['eslint --fix', () => 'yarn test'],
	'*.ts': () => 'tsc -p tsconfig.json --noEmit',
};

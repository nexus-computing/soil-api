import axios from 'axios';
import * as yargs from 'yargs';
import { createField } from '../src/resources/field';
import { createMockArea } from '../src/test-utils/mockGenerators';
import { promises as fs } from 'fs';
const { readFile } = fs;

const argvPromise = yargs
	.command('create-fields', 'Creates a field with an associated stratification.', {
		'api-url': {
			alias: 'u',
			description: 'The Soil API url. (ex. "http://localhost:3000")',
			type: 'string',
			demandOption: true,
			requiresArg: true,
		},
		auth: {
			alias: 'a',
			description: 'The authorization header value to pass to the Soil API. (ex. "name@example.com abcd1234")',
			type: 'string',
			demandOption: true,
			requiresArg: true,
		},
		'group-id': {
			alias: 'gid',
			description: 'The id of the Surveystack group to create the survey under.',
			type: 'string',
			demandOption: true,
			requiresArg: true,
		},
		'file-path': {
			alias: 'i',
			description: 'Path to heartland projects export JSON file.',
			type: 'string',
			demandOption: true,
			requiresArg: true,
		},
		'dry-run': {
			alias: 'd',
			description: 'Prevent survey creation. Only log args.',
			type: 'boolean',
			default: false,
		},
	})
	.demandCommand(1, 'You must specify one of the above commands.')
	.help()
	.alias('help', 'h')
	.parse();

const successes = [];
const failures = [];

const parseFile = async (filePath) => {
	return JSON.parse(await readFile(filePath, 'utf-8'));
};

const getFieldsFromHeartlandExport = (heartlandExport) => {
	const fields = heartlandExport.flatMap(({ producers }) => producers.flatMap(({ fields }) => fields));
	return fields;
};

const createFieldRequest = (heartlandField) => {
	const area = createMockArea({
		geometry: heartlandField.boundaryGeoJSON,
		properties: {},
	});
	const field: any = createField({
		name: heartlandField.fieldByProjectId,
		meta: {
			referenceIds: [{ owner: 'heartland', id: heartlandField.id }],
		},
	});
	field.areas = [area];
	return field;
};

const postField = async ({ fieldRequestBody, apiUrl, groupId, auth }) => {
	let fieldId: string;
	let areaId: string;
	try {
		const fieldsResponse = await axios.post(`${apiUrl}/fields?groupId=${groupId}`, fieldRequestBody, {
			headers: {
				'X-Authorization': auth,
			},
		});
		fieldId = fieldsResponse.data.fields[0].id;
		areaId = fieldsResponse.data.areas[0].id;
		console.log(`Successfully created field (${fieldRequestBody.name})`, fieldId);
		console.log(`Successfully created area (${fieldRequestBody.name})`, areaId);
		successes.push({ name: fieldRequestBody.name, fieldId, areaId });
	} catch (error) {
		console.log(`Failed to create field (${fieldRequestBody.name})`);
		failures.push({ name: fieldRequestBody.name });
		return;
	}
};

async function main() {
	const argv = await argvPromise;
	if (argv._.includes('create-fields')) {
		const apiUrl = argv.apiUrl as string;
		const auth = argv.auth as string;
		const groupId = argv.groupId as string;
		const filePath = argv.filePath as string;
		const dryRun = argv.dryRun as boolean;
		console.log('Received Args:', {
			apiUrl,
			auth,
			groupId,
			filePath,
			dryRun,
		});

		const heartlandExport = await parseFile(filePath);
		const heartlandFields = getFieldsFromHeartlandExport(heartlandExport);
		const soilApiFields = heartlandFields.map(createFieldRequest);

		if (dryRun) {
			console.log('This is a dry run.');
			console.log('The fields that would be created are:');
			soilApiFields.forEach((field) => console.log(`\n${JSON.stringify(field)}`));
			console.log(`\nFound ${soilApiFields.length} fields.`);
			return;
		}

		for (const fieldRequestBody of soilApiFields) {
			await postField({
				fieldRequestBody,
				apiUrl,
				groupId,
				auth,
			});
		}

		console.log('\n---FINAL RESULTS---');
		console.log('Number of Fields: ', successes.length + failures.length);
		console.log('Successful: ', successes.length);
		console.log('Failed: ', failures.length);
		failures.forEach((failure) => console.log(`${failure.name}`));
	}
}

main();

/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
	target: 'node',
	entry: './src/index.ts',
	module: {
		rules: [
			{
				test: /\.ts$/,
				use: 'ts-loader?configFile=tsconfig.webpack.json',
				exclude: /node_modules/,
			},
		],
	},
	resolve: {
		extensions: ['.ts', '.js'],
	},
	output: {
		filename: 'bundle.js',
		path: path.resolve(__dirname, 'dist'),
	},
	plugins: [
		new CopyWebpackPlugin({
			patterns: [
				{ from: './node_modules/swagger-ui-dist/swagger-ui.css' },
				{ from: './node_modules/swagger-ui-dist/swagger-ui-bundle.js' },
				{ from: './node_modules/swagger-ui-dist/swagger-ui-standalone-preset.js' },
				{ from: './node_modules/swagger-ui-dist/favicon-16x16.png' },
				{ from: './node_modules/swagger-ui-dist/favicon-32x32.png' },
			],
		}),
	],
};

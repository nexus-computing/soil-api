interface MatrixQuestionContentItem {
	value: string;
	label: string;
	type: string;
}
interface SurveyControlOutline {
	name: string;
	label: string;
	type: string;
	children?: SurveyControlOutline[];
	content?: MatrixQuestionContentItem[];
}
interface SurveyDefinitionOutline {
	name: string;
	controls: SurveyControlOutline[];
}

interface SurveyGroup {
	id: string;
	path: string;
}

interface SurveyControlContent {
	label: string;
	value: string;
	tags: string;
	type: string;
	resource: string;
	multiple: boolean;
	required: boolean;
	redacted: boolean;
	scaleWidth: number;
}

interface SurveyControl {
	name: string;
	label: string;
	type: string;
	options: {
		readOnly: boolean;
		required: boolean;
		redacted: boolean;
		relevance: {
			enabled: boolean;
			code: string;
		};
		constraint: {
			enabled: boolean;
			code: string;
		};
		calculate: {
			enabled: boolean;
			code: string;
		};
		apiCompose: {
			enabled: boolean;
			code: string;
		};
		geoJSON?: {
			showPolygon: boolean;
			showLine: boolean;
			showCircle: boolean;
			showPoint: boolean;
		} | null;
		source?: {
			config: {
				addRowLabel: string;
			};
			content: SurveyControlContent[] | null;
		} | null;
	};
	id: string;
	value: null;
	children?: SurveyControl[] | null;
}

interface SurveyRevision {
	dateCreated: string;
	version: number;
	controls: SurveyControl[];
}

interface SurveyDefinition {
	_id: string;
	name: string;
	latestVersion: number;
	meta: {
		dateCreated: string;
		dateModified: string;
		submissions: string;
		group: SurveyGroup;
		specVersion: 4;
	};
	resources: [];
	revisions: SurveyRevision[];
}

export {
	MatrixQuestionContentItem,
	SurveyControlOutline,
	SurveyControl,
	SurveyDefinitionOutline,
	SurveyGroup,
	SurveyDefinition,
};

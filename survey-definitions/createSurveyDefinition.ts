import ObjectId from 'bson-objectid';
import {
	MatrixQuestionContentItem,
	SurveyControlOutline,
	SurveyControl,
	SurveyDefinitionOutline,
	SurveyGroup,
	SurveyDefinition,
} from './types';

function createSurveyControlContent({ label, value, type }: MatrixQuestionContentItem) {
	return {
		value,
		label,
		type,
		tags: '',
		resource: '',
		multiple: false,
		required: false,
		redacted: false,
		scaleWidth: 100,
	};
}

function createSurveyControl({ name, label, type, children, content }: SurveyControlOutline): SurveyControl {
	return {
		name,
		label,
		type,
		children: children ? children.map(createSurveyControl) : null,
		options: {
			source:
				type === 'matrix'
					? {
							config: {
								addRowLabel: 'Add row',
							},
							content: content ? content.map(createSurveyControlContent) : null,
					  }
					: null,
			readOnly: false,
			required: false,
			redacted: true,
			relevance: {
				enabled: false,
				code: '',
			},
			constraint: {
				enabled: false,
				code: '',
			},
			calculate: {
				enabled: false,
				code: '',
			},
			apiCompose: {
				enabled: false,
				code: '',
			},
			geoJSON:
				type === 'geoJSON'
					? {
							showPolygon: true,
							showLine: true,
							showCircle: true,
							showPoint: true,
					  }
					: null,
		},
		id: new ObjectId().toHexString(),
		value: null,
	};
}

function createSurveyDefinition({ name, controls }: SurveyDefinitionOutline, group: SurveyGroup): SurveyDefinition {
	const currentTime = new Date().toISOString();
	return {
		_id: new ObjectId().toHexString(),
		name,
		latestVersion: 1,
		meta: {
			dateCreated: currentTime,
			dateModified: currentTime,
			submissions: 'user',
			group,
			specVersion: 4,
		},
		resources: [],
		revisions: [
			{
				dateCreated: currentTime,
				version: 1,
				controls: controls.map(createSurveyControl),
			},
		],
	};
}

export { createSurveyDefinition };

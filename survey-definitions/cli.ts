import axios from 'axios';
import yargs from 'yargs';
import { SurveyDefinition, SurveyDefinitionOutline } from './types';
import { createSurveyDefinition } from './createSurveyDefinition';
import {
	sampleDefinitionOutline,
	samplingDefinitionOutline,
	samplingCollectionDefinitionOutline,
	areaDefinitionOutline,
	fieldDefinitionOutline,
	locationDefinitionOutline,
	locationCollectionDefinitionOutline,
	stratificationDefinitionOutline,
} from './definition-outlines/index';

type resourceType =
	| 'sample'
	| 'sampling'
	| 'samplingCollection'
	| 'area'
	| 'field'
	| 'location'
	| 'locationCollection'
	| 'stratification'
	| 'all';
const resourceTypes: ReadonlyArray<resourceType> = [
	'sample',
	'sampling',
	'samplingCollection',
	'area',
	'field',
	'location',
	'locationCollection',
	'stratification',
	'all',
];

const argvPromise = yargs
	.command('create', 'Creates survey(s) in Surveystack.', {
		resource: {
			alias: 'r',
			description: 'The resource to create a survey for.',
			choices: resourceTypes,
			demandOption: true,
			requiresArg: true,
		},
		'api-url': {
			alias: 'u',
			description: 'The Surveystack API url. (ex. "https://dev.surveystack.io/api")',
			type: 'string',
			demandOption: true,
			requiresArg: true,
		},
		auth: {
			alias: 'a',
			description: 'The authorization header value to pass to Surveystack. (ex. "name@example.com abcd1234")',
			type: 'string',
			demandOption: true,
			requiresArg: true,
		},
		'group-id': {
			alias: 'gid',
			description: 'The id of the Surveystack group to create the survey under.',
			type: 'string',
			demandOption: true,
			requiresArg: true,
		},
		'group-path': {
			alias: 'gpath',
			description: 'The path of the Surveystack group to create the survey under.',
			type: 'string',
			demandOption: true,
			requiresArg: true,
		},
		'dry-run': {
			alias: 'd',
			description: 'Prevent survey creation. Only log args.',
			type: 'boolean',
			default: false,
		},
	})
	.demandCommand(1, 'You must specify one of the above commands.')
	.help()
	.alias('help', 'h')
	.parse();

const SURVEYS_PATH = '/surveys';

function createSurvey(surveyDefinition: SurveyDefinition, apiUrl: string, authHeaderValue: string) {
	return axios.post(`${apiUrl}${SURVEYS_PATH}`, surveyDefinition, {
		headers: {
			Authorization: authHeaderValue,
		},
	});
}

const outlines: { [K in resourceType]: SurveyDefinitionOutline[] } = {
	sample: [sampleDefinitionOutline],
	sampling: [samplingDefinitionOutline],
	samplingCollection: [samplingCollectionDefinitionOutline],
	area: [areaDefinitionOutline],
	field: [fieldDefinitionOutline],
	location: [locationDefinitionOutline],
	locationCollection: [locationCollectionDefinitionOutline],
	stratification: [stratificationDefinitionOutline],
	all: [
		sampleDefinitionOutline,
		samplingDefinitionOutline,
		samplingCollectionDefinitionOutline,
		areaDefinitionOutline,
		fieldDefinitionOutline,
		locationDefinitionOutline,
		locationCollectionDefinitionOutline,
		stratificationDefinitionOutline,
	],
};

async function main() {
	const argv = await argvPromise;
	if (argv._.includes('create')) {
		const apiUrl = argv.apiUrl as string;
		const auth = argv.auth as string;
		const groupId = argv.groupId as string;
		const groupPath = argv.groupPath as string;
		const resource = argv.resource as resourceType;
		const dryRun = argv.dryRun as boolean;
		console.log('Received Args:', {
			apiUrl,
			auth,
			groupId,
			groupPath,
			resource,
			dryRun,
		});

		if (dryRun) {
			return;
		}

		const results = [];
		const envFileEntries = [`surveystackApiUrl=${apiUrl}`];

		const definitions = outlines[resource].map((outline) =>
			createSurveyDefinition(outline, {
				id: groupId as string,
				path: groupPath as string,
			})
		);

		for (const definition of definitions) {
			console.log(`\nAttempting to create survey: ${definition.name}`);
			try {
				const response = await createSurvey(definition, apiUrl, auth);
				console.log(`Successfully created survey: ${definition.name}!`);
				results.push(`SUCCESS:: ${definition.name}: ${response.data.insertedId}`);
				envFileEntries.push(
					`${definition.name.split('soilstack__')[1]}SurveyId=${response.data.insertedId}`,
					`${definition.name.split('soilstack__')[1]}SurveyVersion=1`
				);
			} catch (error) {
				console.log(`ERROR creating survey: ${definition.name}!`);
				results.push(`FAILURE:: ${definition.name}`);
			}
		}

		console.log('\nResults:', '\n---------');
		results.forEach((msg) => console.log(msg));

		envFileEntries.push(`soilstackBaseGroupPath=${groupPath}`);
		console.log('\nExample .env file entries:', '\n---------');
		console.log(envFileEntries.join('\n'));
	}
}

main();

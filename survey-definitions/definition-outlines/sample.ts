import { SurveyDefinitionOutline } from '../types';
import { idControl } from './common';

const sampleDefinitionOutline: SurveyDefinitionOutline = {
	name: 'soilstack__sample',
	controls: [
		idControl,
		{
			name: 'name',
			label: 'Name',
			type: 'string',
		},
		{
			name: 'sample_of',
			label: 'Sample of (field)',
			type: 'string',
		},
		{
			name: 'results',
			label: 'Results',
			type: 'string',
		},
		{
			name: 'result_of',
			label: 'Result of (sampling)',
			type: 'string',
		},
		{
			name: 'so_depth',
			label: 'SoDepth',
			type: 'string',
		},
	],
};

export default sampleDefinitionOutline;

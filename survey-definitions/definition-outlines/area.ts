import { SurveyDefinitionOutline } from '../types';
import { idControl } from './common';

const areaDefinitionOutline: SurveyDefinitionOutline = {
	name: 'soilstack__area',
	controls: [
		idControl,
		{
			name: 'geojson',
			label: 'geoJSON',
			type: 'geoJSON',
		},
	],
};

export default areaDefinitionOutline;

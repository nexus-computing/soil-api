import { SurveyDefinitionOutline } from '../types';
import { idControl } from './common';

const samplingDefinitionOutline: SurveyDefinitionOutline = {
	name: 'soilstack__sampling',
	controls: [
		idControl,
		{
			name: 'name',
			label: 'Name',
			type: 'string',
		},
		{
			name: 'result_time',
			label: 'Result Time',
			type: 'date',
		},
		{
			name: 'geometry',
			label: 'Geometry',
			type: 'geoJSON',
		},
		{
			name: 'feature_of_interest',
			label: 'Feature of Interest (location)',
			type: 'string',
		},
		{
			name: 'member_of',
			label: 'Member of (sampling collection)',
			type: 'string',
		},
		{
			name: 'results',
			label: 'Results (samples)',
			type: 'string',
		},
		{
			name: 'comment',
			label: 'Comment',
			type: 'string',
		},
		{
			name: 'procedures',
			label: 'Procedures',
			type: 'string',
		},
		{
			name: 'properties',
			label: 'Properties',
			type: 'string',
		},
		{
			name: 'so_depth',
			label: 'SoDepth',
			type: 'string',
		},
	],
};

export default samplingDefinitionOutline;

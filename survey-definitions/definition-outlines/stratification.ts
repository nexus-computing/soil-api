import { SurveyDefinitionOutline } from '../types';
import { idControl } from './common';

const stratificationDefinitionOutline: SurveyDefinitionOutline = {
	name: 'soilstack__stratification',
	controls: [
		idControl,
		{
			name: 'name',
			label: 'Name',
			type: 'string',
		},
		{
			name: 'agent',
			label: 'Agent',
			type: 'string',
		},
		{
			name: 'date_created',
			label: 'Date Created',
			type: 'date',
		},
		{
			name: 'provider',
			label: 'Provider',
			type: 'string',
		},
		{
			name: 'object',
			label: 'Object (area)',
			type: 'string',
		},
		{
			name: 'algorithm',
			label: 'Algorithm',
			type: 'group',
			children: [
				{
					name: 'name',
					label: 'Name',
					type: 'string',
				},
				{
					name: 'alternate_name',
					label: 'Alternate Name',
					type: 'string',
				},
				{
					name: 'code_repository',
					label: 'Code Repository',
					type: 'string',
				},
				{
					name: 'version',
					label: 'Version',
					type: 'string',
				},
				{
					name: 'doi',
					label: 'doi',
					type: 'string',
				},
			],
		},
		{
			name: 'input',
			label: 'Input',
			type: 'matrix',
			content: [
				{
					value: 'name',
					label: 'Name',
					type: 'text',
				},
				{
					value: 'value',
					label: 'Value',
					type: 'text',
				},
			],
		},
		{
			name: 'result',
			label: 'Result',
			type: 'matrix',
			content: [
				{
					value: 'name',
					label: 'Name',
					type: 'text',
				},
				{
					value: 'value',
					label: 'Value',
					type: 'text',
				},
			],
		},
	],
};

export default stratificationDefinitionOutline;

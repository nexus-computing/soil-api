import { SurveyDefinitionOutline } from '../types';
import { idControl } from './common';

const samplingCollectionDefinitionOutline: SurveyDefinitionOutline = {
	name: 'soilstack__samplingCollection',
	controls: [
		idControl,
		{
			name: 'name',
			label: 'Name',
			type: 'string',
		},
		{
			name: 'comment',
			label: 'Comment',
			type: 'string',
		},
		{
			name: 'creator',
			label: 'Creator',
			type: 'string',
		},
		{
			name: 'feature_of_interest',
			label: 'Feature Of Interest (field)',
			type: 'string',
		},
		{
			name: 'participants',
			label: 'Participants',
			type: 'string',
		},
		{
			name: 'object',
			label: 'Object (location collection)',
			type: 'string',
		},
		{
			name: 'members',
			label: 'Members (samplings)',
			type: 'string',
		},
		{
			name: 'so_depth',
			label: 'SoDepth',
			type: 'string',
		},
		{
			name: 'sample_depths',
			label: 'Sample Depths',
			type: 'string',
		},
		{
			name: 'date_created',
			label: 'Date Created',
			type: 'date',
		},
		{
			name: 'date_modified',
			label: 'Date Modified',
			type: 'date',
		},
		{
			name: 'meta',
			label: 'Meta',
			type: 'group',
			children: [
				{
					name: 'reference_ids',
					label: 'Reference Ids',
					type: 'matrix',
					content: [
						{
							value: 'owner',
							label: 'Owner',
							type: 'text',
						},
						{
							value: 'id',
							label: 'Id',
							type: 'text',
						},
					],
				},
			],
		},
	],
};

export default samplingCollectionDefinitionOutline;

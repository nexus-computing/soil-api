import { SurveyDefinitionOutline } from '../types';
import { idControl } from './common';

const fieldDefinitionOutline: SurveyDefinitionOutline = {
	name: 'soilstack__field',
	controls: [
		idControl,
		{
			name: 'name',
			label: 'Field Name',
			type: 'string',
		},
		{
			name: 'areas',
			label: 'Areas (field boundaries)',
			type: 'string',
		},
		{
			name: 'contact_points',
			label: 'Contact Points',
			type: 'string',
		},
		{
			name: 'address',
			label: 'Address',
			type: 'string',
		},
		{
			name: 'alternate_name',
			label: 'Alternate Name',
			type: 'string',
		},
		{
			name: 'producer_name',
			label: 'Producer Name',
			type: 'string',
		},
		{
			name: 'meta',
			label: 'Meta',
			type: 'group',
			children: [
				{
					name: 'reference_ids',
					label: 'Reference Ids',
					type: 'matrix',
					content: [
						{
							value: 'owner',
							label: 'Owner',
							type: 'text',
						},
						{
							value: 'id',
							label: 'Id',
							type: 'text',
						},
					],
				},
			],
		},
	],
};

export default fieldDefinitionOutline;

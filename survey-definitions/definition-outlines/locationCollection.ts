import { SurveyDefinitionOutline } from '../types';
import { idControl } from './common';

const locationCollectionDefinitionOutline: SurveyDefinitionOutline = {
	name: 'soilstack__locationCollection',
	controls: [
		idControl,
		{
			name: 'object',
			label: 'Object (area)',
			type: 'string',
		},
		{
			name: 'result_of',
			label: 'Result Of (stratification)',
			type: 'string',
		},
		{
			name: 'feature_of_interest',
			label: 'Feature of Interest (field)',
			type: 'string',
		},
		{
			name: 'features',
			label: 'Features (locations)',
			type: 'string',
		},
	],
};

export default locationCollectionDefinitionOutline;

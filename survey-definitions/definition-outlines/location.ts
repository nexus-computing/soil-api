import { SurveyDefinitionOutline } from '../types';
import { idControl } from './common';

const locationDefinitionOutline: SurveyDefinitionOutline = {
	name: 'soilstack__location',
	controls: [
		idControl,
		{
			name: 'geojson',
			label: 'geoJSON',
			type: 'geoJSON',
		},
	],
};

export default locationDefinitionOutline;

FROM node:16

COPY ./dist /app/dist

USER node
EXPOSE 3000
ENV NODE_ENV production
WORKDIR /app
CMD ["node", "./dist/bundle.js"]

import { ResourceTypes } from '../constants';
import { createPoint, createPolygon } from '../resources';
import { SurveyStackGroup, SurveyStackMembership, SurveyStackSubmissionResponseBody } from '../types';
import { createMockAddress, createMockContactPoint, createMockPoint, createMockSoDepth } from './mockGenerators';

const createLegacyFieldData = ({
	id = 'field-id',
	name = 'Field Name',
	alternateName = 'field-alternate-name',
	address = JSON.stringify(createMockAddress()),
	contactPoints = JSON.stringify([createMockContactPoint()]),
	areas = JSON.stringify(['area-id']),
	meta = [
		{
			owner: {
				value: 'heartland',
			},
			id: {
				value: 'id1',
			},
		},
	],
} = {}) => {
	const dateModified = new Date(0).toISOString();
	return {
		id: {
			value: id,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		name: {
			value: name,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		alternate_name: {
			value: alternateName,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		address: {
			value: address,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		contact_points: {
			value: contactPoints,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		areas: {
			value: areas,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		meta: {
			reference_ids: {
				meta: {
					dateModified,
					type: 'matrix',
					permissions: ['admin'],
				},
				value: meta,
			},
			meta: {
				dateModified,
				type: 'group',
				permissions: ['admin'],
			},
		},
	};
};

const createFieldData = ({
	id = 'field-id',
	name = 'Field Name',
	alternateName = 'field-alternate-name',
	producerName = 'field-producer-name',
	address = JSON.stringify(createMockAddress()),
	contactPoints = JSON.stringify([createMockContactPoint()]),
	areas = JSON.stringify(['area-id']),
	meta = [
		{
			owner: {
				value: 'heartland',
			},
			id: {
				value: 'id1',
			},
		},
	],
} = {}) => {
	const dateModified = new Date(0).toISOString();
	return {
		id: {
			value: id,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		name: {
			value: name,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		alternate_name: {
			value: alternateName,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		producer_name: {
			value: producerName,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		address: {
			value: address,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		contact_points: {
			value: contactPoints,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		areas: {
			value: areas,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		meta: {
			reference_ids: {
				meta: {
					dateModified,
					type: 'matrix',
					permissions: ['admin'],
				},
				value: meta,
			},
			meta: {
				dateModified,
				type: 'group',
				permissions: ['admin'],
			},
		},
	};
};

const createAreaData = ({
	id = 'area-id',
	geometry = createPolygon(),
	properties = {
		name: 'Area Name',
		description: 'Area Description',
		featureOfInterest: 'field-id',
	},
} = {}) => {
	const dateModified = new Date(0).toISOString();
	return {
		id: {
			value: id,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		geojson: {
			value: {
				type: 'FeatureCollection',
				features: [
					{
						type: 'Feature',
						geometry,
						properties,
					},
				],
			},
			meta: {
				dateModified,
				type: 'geoJSON',
				permissions: ['admin'],
			},
		},
	};
};

const createStratificationData = ({
	id = 'stratification-id',
	name = 'Stratification Name',
	agent = 'Mz. Stratifier',
	dateCreated = new Date(0).toISOString(),
	provider = 'stratification provider',
	object = 'area-id',
	algorithm = {
		name: 'mock algorithm name',
		alternateName: 'mock algorithm alternateName',
		codeRepository: 'mock algorithm codeRepository',
		version: 'mock algorithm version',
		doi: 'mock algorithm doi',
	},
	input = [
		{
			name: { value: 'INPUT_NAME', meta: { permissions: ['admin'] } },
			value: { value: JSON.stringify('INPUT_VALUE'), meta: { permissions: ['admin'] } },
		},
	],
	result = [
		{
			name: { value: 'RESULT_NAME', meta: { permissions: ['admin'] } },
			value: { value: JSON.stringify('RESULT_VALUE'), meta: { permissions: ['admin'] } },
		},
	],
} = {}) => {
	const dateModified = new Date(0).toISOString();
	return {
		id: {
			value: id,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		name: {
			value: name,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		agent: {
			value: agent,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		date_created: {
			value: dateCreated,
			meta: {
				dateModified,
				type: 'date',
				permissions: ['admin'],
			},
		},
		provider: {
			value: provider,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		object: {
			value: object,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		algorithm: {
			name: {
				value: algorithm.name,
				meta: {
					dateModified,
					type: 'string',
					permissions: ['admin'],
				},
			},
			alternate_name: {
				value: algorithm.alternateName,
				meta: {
					dateModified,
					type: 'string',
					permissions: ['admin'],
				},
			},
			code_repository: {
				value: algorithm.codeRepository,
				meta: {
					dateModified,
					type: 'string',
					permissions: ['admin'],
				},
			},
			version: {
				value: algorithm.version,
				meta: {
					dateModified,
					type: 'string',
					permissions: ['admin'],
				},
			},
			doi: {
				value: algorithm.doi,
				meta: {
					dateModified,
					type: 'string',
					permissions: ['admin'],
				},
			},
			meta: {
				dateModified,
				type: 'group',
				permissions: ['admin'],
			},
		},
		input: {
			value: input,
			meta: {
				dateModified,
				type: 'matrix',
				permissions: ['admin'],
			},
		},
		result: {
			value: result,
			meta: {
				dateModified,
				type: 'matrix',
				permissions: ['admin'],
			},
		},
	};
};

const createLocationData = ({
	id = 'location-id',
	geometry = createPoint(),
	properties = {
		stratum: 0,
	},
} = {}) => {
	const dateModified = new Date(0).toISOString();
	return {
		id: {
			value: id,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		geojson: {
			value: {
				type: 'FeatureCollection',
				features: [
					{
						type: 'Feature',
						geometry,
						properties,
					},
				],
			},
			meta: {
				dateModified,
				type: 'geoJSON',
				permissions: ['admin'],
			},
		},
	};
};

const createLocationCollectionData = ({
	id = 'location-collection-id',
	object = 'area-id',
	resultOf = 'stratification-id',
	featureOfInterest = 'field-id',
	features = JSON.stringify(['location-id']),
} = {}) => {
	const dateModified = new Date(0).toISOString();
	return {
		id: {
			value: id,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		object: {
			value: object,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		result_of: {
			value: resultOf,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		feature_of_interest: {
			value: featureOfInterest,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		features: {
			value: features,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
	};
};

const createSampleData = ({
	id = 'sample-id',
	name = 'Sample Name',
	sampleOf = 'field-id',
	results = JSON.stringify(['lab result']),
	resultOf = 'sampling-id',
	soDepth = JSON.stringify(createMockSoDepth()),
} = {}) => {
	const dateModified = new Date(0).toISOString();
	return {
		id: {
			value: id,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		name: {
			value: name,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		sample_of: {
			value: sampleOf,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		results: {
			value: results,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		result_of: {
			value: resultOf,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		so_depth: {
			value: soDepth,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
	};
};

const createSamplingData = ({
	id = 'sampling-id',
	name = 'Sampling Name',
	resultTime = new Date(0).toISOString(),
	geometry = createMockPoint(),
	featureOfInterest = 'location-id',
	memberOf = 'sampling-collection-id',
	results = JSON.stringify(['sample-id']),
	comment = 'this is quality dirt',
	procedures = JSON.stringify(['soil procedure']),
	properties = JSON.stringify({}),
	soDepth = JSON.stringify(createMockSoDepth()),
} = {}) => {
	const dateModified = new Date(0).toISOString();
	return {
		id: {
			value: id,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		name: {
			value: name,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		result_time: {
			value: resultTime,
			meta: {
				dateModified,
				type: 'date',
				permissions: ['admin'],
			},
		},
		geometry: {
			value: {
				type: 'FeatureCollection',
				features: [
					{
						type: 'Feature',
						geometry,
						properties,
					},
				],
			},
			meta: {
				dateModified,
				type: 'geoJSON',
				permissions: ['admin'],
			},
		},
		feature_of_interest: {
			value: featureOfInterest,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		member_of: {
			value: memberOf,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		results: {
			value: results,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		comment: {
			value: comment,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		procedures: {
			value: procedures,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		properties: {
			value: properties,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		so_depth: {
			value: soDepth,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
	};
};

const createSamplingCollectionData = ({
	id = 'sampling-collection-id',
	name = 'Sampling Collection name',
	comment = 'nice sampling collection',
	creator = 'user-id',
	featureOfInterest = 'field-id',
	participants = JSON.stringify(['user-id']),
	object = 'location-collection-id',
	members = JSON.stringify(['sampling-id']),
	soDepth = JSON.stringify(createMockSoDepth()),
	sampleDepths = JSON.stringify([createMockSoDepth()]),
	dateCreated = new Date(0).toISOString(),
	dateModified = new Date(0).toISOString(),
	meta = [
		{
			owner: {
				value: 'soilstack-draft',
			},
			id: {
				value: 'id1',
			},
		},
	],
} = {}) => {
	const metaDateModified = new Date(0).toISOString();
	return {
		id: {
			value: id,
			meta: {
				dateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		name: {
			value: name,
			meta: {
				dateModified: metaDateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		comment: {
			value: comment,
			meta: {
				dateModified: metaDateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		creator: {
			value: creator,
			meta: {
				dateModified: metaDateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		feature_of_interest: {
			value: featureOfInterest,
			meta: {
				dateModified: metaDateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		participants: {
			value: participants,
			meta: {
				dateModified: metaDateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		object: {
			value: object,
			meta: {
				dateModified: metaDateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		members: {
			value: members,
			meta: {
				dateModified: metaDateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		so_depth: {
			value: soDepth,
			meta: {
				dateModified: metaDateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		sample_depths: {
			value: sampleDepths,
			meta: {
				dateModified: metaDateModified,
				type: 'string',
				permissions: ['admin'],
			},
		},
		date_created: {
			value: dateCreated,
			meta: {
				dateModified: metaDateModified,
				type: 'date',
				permissions: ['admin'],
			},
		},
		date_modified: {
			value: dateModified,
			meta: {
				dateModified: metaDateModified,
				type: 'date',
				permissions: ['admin'],
			},
		},
		meta: {
			reference_ids: {
				meta: {
					dateModified,
					type: 'matrix',
					permissions: ['admin'],
				},
				value: meta,
			},
			meta: {
				dateModified,
				type: 'group',
				permissions: ['admin'],
			},
		},
	};
};

const createResourceData = {
	[ResourceTypes.Field]: createFieldData,
	[ResourceTypes.Area]: createAreaData,
	[ResourceTypes.Stratification]: createStratificationData,
	[ResourceTypes.LocationCollection]: createLocationCollectionData,
	[ResourceTypes.Location]: createLocationData,
	[ResourceTypes.Sample]: createSampleData,
	[ResourceTypes.Sampling]: createSamplingData,
	[ResourceTypes.SamplingCollection]: createSamplingCollectionData,
};

/*
	field.producerName was added in February 2022.
	Submissions prior to that date will not have producerName.
	This function creates a mock surveystack field submission response without producerName.
*/
const createLegacyMockFieldResponseSubmission = (): SurveyStackSubmissionResponseBody => ({
	_id: 'mock id',
	meta: {
		survey: {
			id: '',
			version: 0,
		},
		group: {
			id: '',
			path: '',
		},
		revision: 1,
		specVersion: 3,
		dateCreated: new Date(0).toISOString(),
		dateModified: new Date(1).toISOString(),
		dateSubmitted: new Date(2).toISOString(),
		creator: '',
	},
	data: createLegacyFieldData(),
});

const createMockResponseSubmission = ({
	id = 'mock id',
	resourceType,
	redacted = false,
}: {
	id?: string;
	resourceType: ResourceTypes;
	redacted?: boolean;
}): SurveyStackSubmissionResponseBody => ({
	_id: id,
	meta: {
		survey: {
			id: '',
			version: 0,
		},
		group: {
			id: '',
			path: '',
		},
		revision: 1,
		specVersion: 3,
		dateCreated: new Date(0).toISOString(),
		dateModified: new Date(1).toISOString(),
		dateSubmitted: new Date(2).toISOString(),
		creator: '',
	},
	data: redacted ? {} : createResourceData[resourceType](),
});

const createMockSubmissionsResponse = ({
	resourceType,
}: {
	resourceType: ResourceTypes;
}): { data: Array<SurveyStackSubmissionResponseBody> } => ({
	data: [createMockResponseSubmission({ resourceType })],
});

const createMockGroupsResponse = (): { data: Array<SurveyStackGroup> } => ({
	data: [
		{
			_id: 'mock group id 1',
			meta: {
				archived: false,
				specVersion: 2,
				invitationOnly: true,
			},
			name: 'soil-subgroup-1',
			slug: 'soil-subgroup-1',
			dir: '/soil-app/',
			path: '/soil-app/soil-subgroup-1/',
			surveys: {
				pinned: [],
			},
		},
		{
			_id: 'mock group id 2',
			meta: {
				archived: false,
				specVersion: 2,
				invitationOnly: true,
			},
			name: 'my-subgroup',
			slug: 'my-subgroup',
			dir: '/soil-app/soil-subgroup-1/',
			path: '/soil-app/soil-subgroup-1/my-subgroup/',
			surveys: {
				pinned: [],
			},
		},
	],
});

const createMockMembershipsTreeResponse = (): { data: Array<SurveyStackMembership> } => ({
	data: [
		{
			_id: '6136bbd81d009100015b79c2',
			group: {
				_id: 'mock group id 0',
				name: 'soil-app',
				slug: 'soil-app',
				dir: '/',
				path: '/soil-app/',
			},
			roles: ['user'],
		},
		{
			_id: '6136bbee1d009100015b79c3',
			group: {
				_id: 'mock group id 1',
				name: 'soil-subgroup-1',
				slug: 'soil-subgroup-1',
				dir: '/soil-app/',
				path: '/soil-app/soil-subgroup-1/',
			},
			roles: ['admin'],
		},
	],
});

export {
	createLegacyMockFieldResponseSubmission,
	createMockGroupsResponse,
	createMockMembershipsTreeResponse,
	createMockResponseSubmission,
	createMockSubmissionsResponse,
};

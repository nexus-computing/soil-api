import { Point, Polygon } from 'geojson';
import {
	Address,
	Algorithm,
	Area,
	ContactPoint,
	Field,
	Location,
	LocationCollection,
	Sample,
	Sampling,
	SamplingCollection,
	SoDepth,
	SoDepthValue,
	Stratification,
	StratificationInput,
	StratificationResult,
} from '../types';

export function createMockPolygon({
	coordinates = [
		[
			[40, 80],
			[41, 81],
		],
	],
} = {}): Polygon {
	return {
		type: 'Polygon',
		coordinates,
	};
}

export function createMockPoint({ coordinates = [40, 80] } = {}): Point {
	return {
		type: 'Point',
		coordinates,
	};
}

function createMockSoDepthValue({ value = 10, unit = 'unit:CM' } = {}): SoDepthValue {
	return {
		value,
		unit,
	};
}

export function createMockSoDepth({
	minValue = createMockSoDepthValue(),
	maxValue = createMockSoDepthValue(),
} = {}): SoDepth {
	return {
		minValue,
		maxValue,
	};
}

export function createMockSample({
	id,
	name = 'mock name',
	sampleOf = 'mock field id',
	results = ['mock result'],
	resultOf = 'mock sampling id',
	soDepth = createMockSoDepth(),
	meta,
}: Partial<Sample> = {}): Sample {
	return id
		? {
				id,
				name,
				sampleOf,
				results,
				resultOf,
				soDepth,
				...(meta ? { meta } : {}),
		  }
		: {
				name,
				sampleOf,
				results,
				resultOf,
				soDepth,
				...(meta ? { meta } : {}),
		  };
}

export function createMockSampling({
	id,
	name = 'mock sampling name',
	resultTime = new Date(0).toISOString(),
	geometry = createMockPoint(),
	featureOfInterest = 'mock location id',
	memberOf = 'mock sampling collection id',
	results = ['mock sample id'],
	comment = 'mock comment',
	procedures = ['mock procedure'],
	properties = {},
	soDepth = createMockSoDepth(),
	meta,
}: Partial<Sampling> = {}): Sampling {
	return id
		? {
				id,
				name,
				resultTime,
				geometry,
				featureOfInterest,
				memberOf,
				results,
				comment,
				procedures,
				properties,
				soDepth,
				...(meta ? { meta } : {}),
		  }
		: {
				name,
				resultTime,
				geometry,
				featureOfInterest,
				memberOf,
				results,
				comment,
				procedures,
				properties,
				soDepth,
				...(meta ? { meta } : {}),
		  };
}

export function createMockSamplingCollection({
	id,
	name = 'mock sampling collection',
	comment = 'mock comment',
	creator = 'mock creator id',
	featureOfInterest = 'mock field id',
	participants = ['mock participant id'],
	object = 'mock location collection id',
	members = ['mock sampling id'],
	soDepth = createMockSoDepth(),
	sampleDepths = [createMockSoDepth()],
	dateCreated = new Date(0).toISOString(),
	dateModified = new Date(0).toISOString(),
	meta = {},
}: Partial<SamplingCollection> = {}): SamplingCollection {
	return id
		? {
				id,
				name,
				comment,
				creator,
				featureOfInterest,
				participants,
				object,
				members,
				soDepth,
				sampleDepths,
				dateCreated,
				dateModified,
				meta,
		  }
		: {
				name,
				comment,
				creator,
				featureOfInterest,
				participants,
				object,
				members,
				soDepth,
				sampleDepths,
				dateCreated,
				dateModified,
				meta,
		  };
}

export function createMockArea({
	id,
	properties = { featureOfInterest: 'mock feature of interest' },
	geometry = createMockPolygon(),
	meta,
}: Partial<Area> = {}): Area {
	return id
		? {
				id,
				type: 'Feature',
				properties,
				geometry,
				...(meta ? { meta } : {}),
		  }
		: {
				type: 'Feature',
				properties,
				geometry,
				...(meta ? { meta } : {}),
		  };
}

export function createMockLocation({
	id,
	properties = { stratum: 0 },
	geometry = createMockPoint(),
	meta,
}: Partial<Location> = {}): Location {
	return id
		? {
				id,
				type: 'Feature',
				properties,
				geometry,
				...(meta ? { meta } : {}),
		  }
		: {
				type: 'Feature',
				properties,
				geometry,
				...(meta ? { meta } : {}),
		  };
}

export function createMockAddress({
	streetAddress = 'mock streetAddress',
	addressLocality = 'mock addressLocality',
	addressRegion = 'mock addressRegion',
	addressCountry = 'mock addressCountry',
	postalCode = 'mock postalCode',
} = {}): Address {
	return {
		streetAddress,
		addressLocality,
		addressRegion,
		addressCountry,
		postalCode,
	};
}

export function createMockContactPoint({
	telephone = 'mock telephone',
	email = 'mock email',
	name = 'mock name',
	contactType = 'mock contactType',
	organization = 'mock organization',
} = {}): ContactPoint {
	return {
		telephone,
		email,
		name,
		contactType,
		organization,
	};
}

export function createMockField({
	id,
	name = 'mock field name',
	alternateName = 'mock alternate name',
	producerName = 'mock producer name',
	address = createMockAddress(),
	contactPoints = [createMockContactPoint()],
	areas = [] as string[],
	meta = {},
}: Partial<Field> = {}): Field {
	return id
		? {
				id,
				name,
				alternateName,
				producerName,
				address,
				contactPoints,
				areas,
				meta,
		  }
		: {
				name,
				alternateName,
				producerName,
				address,
				contactPoints,
				areas,
				meta,
		  };
}

function createMockAlgorithm({
	name = 'mock name',
	alternateName = 'mock alternateName',
	codeRepository = 'mock codeRepository',
	version = 'mock version',
	doi = 'mock doi',
} = {}): Algorithm {
	return {
		name,
		alternateName,
		codeRepository,
		version,
		doi,
	};
}

function createMockStratificationInput({ name = 'mock name', value = 'mock value' } = {}): StratificationInput {
	return {
		name,
		value,
	};
}

function createMockStratificationResult({ name = 'mock name', value = 'mock value' } = {}): StratificationResult {
	return {
		name,
		value,
	};
}

export function createMockStratification({
	id,
	name = 'mock name',
	agent = 'mock agent',
	dateCreated = new Date(0).toISOString(),
	provider = 'mock provider',
	object = 'mock object',
	algorithm = createMockAlgorithm(),
	input = [createMockStratificationInput()],
	result = [createMockStratificationResult()],
	meta,
}: Partial<Stratification> = {}): Stratification {
	return id
		? {
				id,
				name,
				agent,
				dateCreated,
				provider,
				object,
				algorithm,
				input,
				result,
				...(meta ? { meta } : {}),
		  }
		: {
				name,
				agent,
				dateCreated,
				provider,
				object,
				algorithm,
				input,
				result,
				...(meta ? { meta } : {}),
		  };
}

export function createMockLocationCollection({
	id,
	object = 'mock object',
	resultOf = 'mock resultOf',
	featureOfInterest = 'mock featureOfInterest',
	features = ['mock feature'],
	meta,
}: Partial<LocationCollection> = {}): LocationCollection {
	return id
		? {
				id,
				object,
				resultOf,
				featureOfInterest,
				features,
				...(meta ? { meta } : {}),
		  }
		: {
				object,
				resultOf,
				featureOfInterest,
				features,
				...(meta ? { meta } : {}),
		  };
}

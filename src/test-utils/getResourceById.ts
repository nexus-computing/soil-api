import axios from 'axios';
import request from 'supertest';
import app from '../app';
import { ResourceTypes } from '../constants';
import { createMockResponseSubmission, createMockSubmissionsResponse } from './surveystackMockGenerators';
import { expect200AndValidateResponse, expect404AndValidateResponse } from './validation';

// You must call jest.mock('axios') in the test file you use this in.
const testGetResourceByIdEndpoint = ({
	endpointPath,
	resourceName,
	resourceType,
}: {
	endpointPath: string;
	resourceName: string;
	resourceType: ResourceTypes;
}): void => {
	describe(`${endpointPath}/:id`, () => {
		it(`returns 200 when a ${resourceName} is found`, (done) => {
			(axios.get as jest.Mock).mockResolvedValueOnce(createMockSubmissionsResponse({ resourceType }));

			request(app)
				.get(`${endpointPath}/123456789012345678901234`)
				.set('X-Authorization', 'name@example.com abcd1234')
				.send()
				.end(expect200AndValidateResponse(done));
		});

		it(`returns 404 when an ${resourceName} is not found`, (done) => {
			(axios.get as jest.Mock).mockResolvedValueOnce({ data: [] });

			request(app)
				.get(`${endpointPath}/123456789012345678901234`)
				.set('X-Authorization', 'name@example.com abcd1234')
				.send()
				.end(expect404AndValidateResponse(done));
		});

		it(`returns 404 when the ${resourceName} is redacted`, (done) => {
			(axios.get as jest.Mock).mockResolvedValueOnce({
				data: [createMockResponseSubmission({ resourceType, redacted: true })],
			});

			request(app)
				.get(`${endpointPath}/123456789012345678901234`)
				.set('X-Authorization', 'name@example.com abcd1234')
				.send()
				.end(expect404AndValidateResponse(done));
		});

		it(`returns 400 if ${resourceName} id in path is not a valid mongo ObjectId (length of 24)`, (done) => {
			request(app)
				.get(`${endpointPath}/123`)
				.set('X-Authorization', 'name@example.com abcd1234')
				.send()
				.end((err, resp) => {
					expect(resp.status).toEqual(400);
					done();
				});
		});
	});
};

export { testGetResourceByIdEndpoint };

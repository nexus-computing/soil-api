import { NextFunction, Request, Response, Router } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import { getAllResources } from '../services/surveystack';
import { Field, Stratification } from '../types';

const router = Router();

router.get('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const authorizationHeaderValue = String(req.headers['x-authorization']);
		const [
			fields,
			areas,
			stratifications,
			locationCollections,
			locations,
			samples,
			samplings,
			samplingCollections,
		] = await getAllResources({ authorizationHeaderValue });

		const responseStratifications = (stratifications as Stratification[]).map((stratification) => ({
			...stratification,
			featureOfInterest: (fields as Field[]).find((field) => field.areas.includes(stratification.object))?.id,
		}));

		res.status(200).json({
			fields,
			areas,
			stratifications: responseStratifications,
			locationCollections,
			locations,
			samples,
			samplings,
			samplingCollections,
		});
	} catch (error) {
		next(error);
	}
});

const openApiPaths: OpenAPIV3.PathsObject = {
	'/resources': {
		get: {
			'x-internal': true,
			summary: 'Get all resources.',
			security: [{ Authorization: [] }],
			responses: {
				'200': {
					description:
						'An object keyed by resource, with values that are arrays containing all the resources of each type you have access to.',
					content: {
						'application/json': {
							schema: {
								type: 'object',
								required: [
									'fields',
									'areas',
									'stratifications',
									'locationCollections',
									'locations',
									'samples',
									'samplings',
									'samplingCollections',
								],
								properties: {
									fields: { type: 'array', items: { $ref: '#/components/schemas/ResponseField' } },
									areas: { type: 'array', items: { $ref: '#/components/schemas/ResponseArea' } },
									stratifications: { type: 'array', items: { $ref: '#/components/schemas/ResponseStratification' } },
									locationCollections: {
										type: 'array',
										items: { $ref: '#/components/schemas/ResponseLocationCollection' },
									},
									locations: { type: 'array', items: { $ref: '#/components/schemas/ResponseLocation' } },
									samples: { type: 'array', items: { $ref: '#/components/schemas/ResponseSample' } },
									samplings: { type: 'array', items: { $ref: '#/components/schemas/ResponseSampling' } },
									samplingCollections: {
										type: 'array',
										items: { $ref: '#/components/schemas/ResponseSamplingCollection' },
									},
								},
							},
						},
					},
				},
			},
		} as OpenAPIV3.OperationObject,
	},
};

export { openApiPaths, router };

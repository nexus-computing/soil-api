import axios from 'axios';
import request from 'supertest';
import app from '../app';
import { SAMPLE_SURVEY_ID, SAMPLE_SURVEY_VERSION, SURVEYSTACK_API_URL } from '../constants';
import { ResourceTypes } from '../constants';
import {
	createMockSample,
	createMockSubmissionsResponse,
	expect200AndValidateResponse,
	expect204AndValidateResponse,
	expect404AndValidateResponse,
	testGetResourceByIdEndpoint,
} from '../test-utils';
import { Sample } from '../types';

jest.mock('axios');

describe('Sample Endpoints', () => {
	const surveyStackRequestHeaders = { Authorization: 'name@example.com abcd1234' };

	describe('GET', () => {
		describe('/samples', () => {
			it('GETs sample submissions from surveystack', (done) => {
				request(app)
					.get('/samples')
					.set('X-Authorization', 'name@example.com abcd1234')
					.end(() => {
						expect(axios.get as jest.Mock).toHaveBeenCalledWith(
							`${SURVEYSTACK_API_URL}/submissions?survey=${SAMPLE_SURVEY_ID}`,
							{
								headers: surveyStackRequestHeaders,
							}
						);
						done();
					});
			});

			it('returns 200 on success when response is empty', (done) => {
				(axios.get as jest.Mock).mockResolvedValue({ data: [] });

				request(app)
					.get('/samples')
					.set('X-Authorization', 'name@example.com abcd1234')
					.send()
					.end(expect200AndValidateResponse(done));
			});

			it('returns 200 on success when resource is present', (done) => {
				(axios.get as jest.Mock).mockResolvedValue(
					createMockSubmissionsResponse({ resourceType: ResourceTypes.Sample })
				);

				request(app)
					.get('/samples')
					.set('X-Authorization', 'name@example.com abcd1234')
					.send()
					.end(expect200AndValidateResponse(done));
			});

			it('returns 500 when surveystack returns an error', (done) => {
				(axios.get as jest.Mock).mockRejectedValueOnce('error');

				request(app).get(`/samples`).set('X-Authorization', 'name@example.com abcd1234').expect(500, done);
			});
		});

		testGetResourceByIdEndpoint({
			endpointPath: '/samples',
			resourceName: 'sample',
			resourceType: ResourceTypes.Sample,
		});
	});

	describe('PATCH', () => {
		describe('/samples/:id', () => {
			const sampleId = '123456789012345678901234';
			const mockSample = createMockSample({ id: sampleId });
			const { name, soDepth } = mockSample;
			const mockEditableSample: Partial<Sample> = {
				name,
				soDepth: {
					minValue: { ...soDepth.minValue, unit: 'unit:CM' },
					maxValue: { ...soDepth.maxValue, unit: 'unit:CM' },
				},
			};

			it(`returns 204 when a sample is saved`, (done) => {
				(axios.get as jest.Mock).mockResolvedValueOnce(
					createMockSubmissionsResponse({ resourceType: ResourceTypes.Sample })
				);

				request(app)
					.patch(`/samples/${sampleId}`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send(mockEditableSample)
					.end(expect204AndValidateResponse(done));
			});

			it(`saves 'name', 'soDepth' only`, (done) => {
				(axios.get as jest.Mock).mockResolvedValueOnce(
					createMockSubmissionsResponse({ resourceType: ResourceTypes.Sample })
				);

				Date.now = jest.fn(() => 0);

				const updatedSample: Partial<Sample> = {
					name: 'modified name',
					soDepth: {
						minValue: {
							unit: 'unit:CM',
							value: 15,
						},
						maxValue: {
							unit: 'unit:CM',
							value: 35,
						},
					},
				};

				const surveyStackRequestBody = expect.objectContaining({
					_id: 'sample-id',
					meta: expect.objectContaining({
						survey: {
							id: SAMPLE_SURVEY_ID,
							version: SAMPLE_SURVEY_VERSION,
						},
						group: {
							id: '',
							path: null,
						},
						specVersion: 3,
						dateModified: new Date(0),
						archivedReason: 'RESUBMIT',
					}),
					data: expect.objectContaining({
						id: {
							value: 'sample-id',
							meta: expect.anything(),
						},
						name: {
							value: 'modified name',
							meta: expect.anything(),
						},
						sample_of: {
							value: 'field-id',
							meta: expect.anything(),
						},
						results: {
							value: JSON.stringify(['lab result']),
							meta: expect.anything(),
						},
						result_of: {
							value: 'sampling-id',
							meta: expect.anything(),
						},
						so_depth: {
							value: JSON.stringify({
								minValue: {
									unit: 'unit:CM',
									value: 15,
								},
								maxValue: {
									unit: 'unit:CM',
									value: 35,
								},
							}),
							meta: expect.anything(),
						},
					}),
				});

				request(app)
					.patch(`/samples/${sampleId}`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send(updatedSample)
					.end(() => {
						expect(axios.put as jest.Mock).toHaveBeenCalledWith(
							`${SURVEYSTACK_API_URL}/submissions/sample-id`,
							surveyStackRequestBody,
							{ headers: surveyStackRequestHeaders }
						);
						done();
					});
			});

			it(`returns 404 when a sample is not found`, (done) => {
				(axios.get as jest.Mock).mockResolvedValueOnce({ data: [] });

				request(app)
					.patch(`/samples/${sampleId}`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send(mockEditableSample)
					.end(expect404AndValidateResponse(done));
			});

			it("returns 500 when surveystack's get submission endpoint returns an error", (done) => {
				(axios.get as jest.Mock).mockRejectedValue('error');

				request(app)
					.patch(`/samples/${sampleId}`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send(mockEditableSample)
					.expect(500, done);
			});

			it("returns 500 when surveystack's resubmit endpoint returns an error", (done) => {
				(axios.get as jest.Mock).mockResolvedValueOnce(
					createMockSubmissionsResponse({ resourceType: ResourceTypes.Sample })
				);
				(axios.put as jest.Mock).mockRejectedValueOnce('error');

				request(app)
					.patch(`/samples/${sampleId}`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send(mockEditableSample)
					.expect(500, done);
			});
		});
	});
});

import ObjectId from 'bson-objectid';
import { NextFunction, Request, Response, Router } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import { ResourceTypes } from '../constants';
import { groupIdQueryParam } from '../open-api-spec/common/queryParameters';
import { createLocation, createLocationCollection, createStratification } from '../resources';
import {
	bulkArchive,
	createMatchAfterDateSubmitted,
	createMatchFieldsByAreaIds,
	createMatchLocationCollectionsByStratificationId,
	createMatchSamplingCollectionsByLocationCollectionIds,
	createSubmissions,
	getSubmission,
	getSubmissions,
} from '../services/surveystack';
import { Area, Field, Location, LocationCollection, SamplingCollection, Stratification } from '../types';

const router = Router();

router.post('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const locations: Location[] = req.body.locationCollection.features.map((location: Location) =>
			createLocation({
				id: new ObjectId().toHexString(),
				...location,
			})
		);

		const stratification = createStratification({
			id: new ObjectId().toHexString(),
			...req.body.stratification,
		});

		const locationCollection = createLocationCollection({
			id: new ObjectId().toHexString(),
			...req.body.locationCollection,
			features: locations.map(({ id }) => id),
			resultOf: stratification.id,
		});

		const [[field], area] = await Promise.all([
			getSubmissions<Field>({
				resourceType: ResourceTypes.Field,
				authorizationHeaderValue: req.get('X-Authorization') ?? '',
				matchExpression: createMatchFieldsByAreaIds([stratification.object]),
			}),
			getSubmission<Area>({
				resourceType: ResourceTypes.Area,
				authorizationHeaderValue: req.get('X-Authorization') ?? '',
				id: stratification.object,
			}),
		]);

		if (!area) {
			res.status(400).json({ message: `Area ${stratification.object} not found.` });
			return;
		}
		if (!field) {
			res.status(400).json({ message: `Field referencing area ${stratification.object} not found.` });
			return;
		}

		await createSubmissions({
			resources: [
				{ resource: stratification, resourceType: ResourceTypes.Stratification },
				{ resource: locationCollection, resourceType: ResourceTypes.LocationCollection },
				...locations.map((location) => ({ resource: location, resourceType: ResourceTypes.Location })),
			],
			groupId: String(req.query.groupId),
			authorizationHeaderValue: req.get('X-Authorization') ?? '',
		});

		const responseStratification = {
			...stratification,
			featureOfInterest: field.id,
		};

		res
			.status(201)
			.json({ stratifications: [responseStratification], locationCollections: [locationCollection], locations });
	} catch (error) {
		next(error);
	}
});

router.get('/', async (req: Request, res: Response, next: NextFunction) => {
	const submittedAfter = req.query.submittedAfter;
	try {
		const stratifications = await getSubmissions<Stratification>({
			resourceType: ResourceTypes.Stratification,
			authorizationHeaderValue: req.get('X-Authorization') ?? '',
			...(submittedAfter !== undefined
				? { matchExpression: createMatchAfterDateSubmitted(new Date(submittedAfter as string)) }
				: {}),
		});

		const fields = await getSubmissions<Field>({
			resourceType: ResourceTypes.Field,
			authorizationHeaderValue: req.get('X-Authorization') ?? '',
		});

		const stratifiedAreaIds = stratifications.map(({ object }) => object);
		const stratifiedFields = fields.filter(({ areas }) => areas.some((area) => stratifiedAreaIds.includes(area)));

		const responseStratifications = stratifications.map((stratification) => ({
			...stratification,
			featureOfInterest: stratifiedFields.find((field) => field.areas.includes(stratification.object))?.id,
		}));

		res.status(200).json(responseStratifications);
	} catch (error) {
		next(error);
	}
});

router.get('/:id', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const id = req.params.id;

		const stratification = await getSubmission<Stratification>({
			resourceType: ResourceTypes.Stratification,
			authorizationHeaderValue: req.get('X-Authorization') ?? '',
			id,
		});

		if (!stratification) {
			res.status(404).send();
			return;
		}

		const stratifiedField = (
			await getSubmissions<Field>({
				resourceType: ResourceTypes.Field,
				authorizationHeaderValue: req.get('X-Authorization') ?? '',
				matchExpression: createMatchFieldsByAreaIds([stratification.object]),
			})
		)[0];

		const responseStratification = {
			...stratification,
			featureOfInterest: stratifiedField.id,
		};
		res.status(200).json(responseStratification);
	} catch (error) {
		next(error);
	}
});

router.delete('/:id', async (req: Request, res: Response) => {
	try {
		const id = req.params.id;
		const authorizationHeaderValue = req.get('X-Authorization') ?? '';

		const stratification = await getSubmission<Stratification>({
			resourceType: ResourceTypes.Stratification,
			authorizationHeaderValue,
			id,
		});

		if (!stratification) {
			res.status(404).send();
			return;
		}

		const locationCollections = await getSubmissions<LocationCollection>({
			resourceType: ResourceTypes.LocationCollection,
			authorizationHeaderValue,
			matchExpression: createMatchLocationCollectionsByStratificationId(id),
		});

		const locationCollectionIds = locationCollections.map((locationCollection) => locationCollection.id ?? '');
		const locationIds = locationCollections.flatMap((locationCollection) => locationCollection.features);

		if (locationCollectionIds.length > 0) {
			const samplingCollections = await getSubmissions<SamplingCollection>({
				resourceType: ResourceTypes.SamplingCollection,
				authorizationHeaderValue,
				matchExpression: createMatchSamplingCollectionsByLocationCollectionIds(locationCollectionIds),
			});

			if (samplingCollections.length > 0) {
				res.status(409).send({
					message:
						'Cannot delete a stratification that has a samplingCollection, samplings, or samples that depend on it',
				});
				return;
			}
		}

		// Delete
		await bulkArchive({ authorizationHeaderValue, ids: [...locationIds, ...locationCollectionIds, id] });
		res.status(204).send();
	} catch (error) {
		return res.status(500).send();
	}
});

const openApiPaths: OpenAPIV3.PathsObject = {
	'/stratifications': {
		post: {
			tags: ['Stratifications'],
			summary: 'Saves a stratification',
			parameters: [groupIdQueryParam],
			security: [{ Authorization: [] }],
			requestBody: {
				description: 'The stratification object and its location collection object to be saved.',
				content: {
					'application/json': {
						schema: {
							type: 'object',
							required: ['stratification', 'locationCollection'],
							properties: {
								stratification: { $ref: '#/components/schemas/Stratification' },
								locationCollection: { $ref: '#/components/schemas/PopulatedLocationCollection' },
							},
						},
					},
				},
				required: true,
			},
			responses: {
				'201': {
					description: 'Successfully saved a stratification.',
					content: {
						'application/json': {
							schema: {
								type: 'object',
								required: ['stratifications', 'locationCollections', 'locations'],
								properties: {
									stratifications: {
										type: 'array',
										items: { $ref: '#/components/schemas/ResponseStratification' },
									},
									locationCollections: {
										type: 'array',
										items: { $ref: '#/components/schemas/ResponseLocationCollection' },
									},
									locations: {
										type: 'array',
										items: { $ref: '#/components/schemas/ResponseLocation' },
									},
								},
							},
						},
					},
				},
				'400': {
					description: 'Bad request.',
					content: {
						'application/json': {
							schema: {
								type: 'object',
								required: ['message'],
								properties: {
									message: { type: 'string' },
								},
							},
						},
					},
				},
			},
		},
		get: {
			tags: ['Stratifications'],
			summary: 'Get all stratifications',
			security: [{ Authorization: [] }],
			parameters: [
				{
					name: 'submittedAfter',
					in: 'query',
					description: 'Only return stratifications submitted after this ISO8601 timestamp.',
					required: false,
					schema: {
						type: 'string',
						format: 'date-time',
					},
				},
			],
			responses: {
				'200': {
					description: 'An array of the stratifications you have access to.',
					content: {
						'application/json': {
							schema: {
								type: 'array',
								items: { $ref: '#/components/schemas/ResponseStratification' },
							},
						},
					},
				},
			},
		},
	},
	'/stratifications/{stratificationId}': {
		get: {
			tags: ['Stratifications'],
			summary: 'Get a stratification by its id.',
			security: [{ Authorization: [] }],
			parameters: [
				{
					name: 'stratificationId',
					in: 'path',
					description: 'The id of the stratification to get.',
					required: true,
					schema: { type: 'string', minLength: 24, maxLength: 24 },
				},
			],
			responses: {
				'200': {
					description: 'The stratification with the given id.',
					content: {
						'application/json': {
							schema: {
								$ref: '#/components/schemas/ResponseStratification',
							},
						},
					},
				},
				'404': {
					description: 'No stratification with the given id exists.',
				},
			},
		},
		delete: {
			'x-internal': true,
			tags: ['Stratifications'],
			summary: 'Delete a stratification by its id.',
			security: [{ Authorization: [], 'X-Super-Authorization': [] }],
			parameters: [
				{
					name: 'stratificationId',
					in: 'path',
					description: 'The id of the stratification to delete.',
					required: true,
					schema: { type: 'string', minLength: 24, maxLength: 24 },
				},
			],
			responses: {
				'204': {
					description: 'The stratification was deleted successfully.',
				},
				'404': {
					description: 'No stratification with the given id exists.',
				},
				'409': {
					description: 'Cannot delete a stratification that has resources that depend on it.',
					content: {
						'application/json': {
							schema: {
								$ref: '#/components/schemas/Error',
							},
						},
					},
				},
			},
		} as OpenAPIV3.OperationObject,
	},
};

export { openApiPaths, router };

import { NextFunction, Request, Response } from 'express';
import { ResourceTypes } from '../../constants';
import { getSubmission } from '../../services/surveystack';
import { Resource } from '../../types';

const createGetResourceByIdController = <T extends Resource>(resourceType: ResourceTypes) => async (
	req: Request,
	res: Response,
	next: NextFunction
): Promise<any> => {
	try {
		const id = req.params.id;

		const resource = await getSubmission<T>({
			resourceType,
			authorizationHeaderValue: req.get('X-Authorization') ?? '',
			id,
		});

		if (resource) {
			res.status(200).json(resource);
		} else {
			res.status(404).send();
		}
	} catch (error) {
		next(error);
	}
};

export default createGetResourceByIdController;

import axios from 'axios';
import request from 'supertest';
import app from '../app';
import {
	AREA_SURVEY_ID,
	FIELD_SURVEY_ID,
	LOCATION_COLLECTION_SURVEY_ID,
	LOCATION_SURVEY_ID,
	ResourceTypes,
	SAMPLE_SURVEY_ID,
	SAMPLING_COLLECTION_SURVEY_ID,
	SAMPLING_SURVEY_ID,
	STRATIFICATION_SURVEY_ID,
	SURVEYSTACK_API_URL,
} from '../constants';
import { createMockSubmissionsResponse, expect200AndValidateResponse } from '../test-utils';

jest.mock('axios');

describe('/resources', () => {
	const surveyStackRequestHeaders = { Authorization: 'name@example.com abcd1234' };

	describe('GET', () => {
		const resourcesToSurveyIds = {
			field: FIELD_SURVEY_ID,
			area: AREA_SURVEY_ID,
			stratification: STRATIFICATION_SURVEY_ID,
			locationCollection: LOCATION_COLLECTION_SURVEY_ID,
			location: LOCATION_SURVEY_ID,
			sample: SAMPLE_SURVEY_ID,
			sampling: SAMPLING_SURVEY_ID,
			samplingCollection: SAMPLING_COLLECTION_SURVEY_ID,
		};

		Object.entries(resourcesToSurveyIds).forEach(([resource, surveyId]) => {
			it(`GETs ${resource} submissions from surveystack`, (done) => {
				request(app)
					.get(`/resources`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.end((err, res) => {
						expect(axios.get as jest.Mock).toHaveBeenCalledWith(
							`${SURVEYSTACK_API_URL}/submissions?survey=${surveyId}`,
							{
								headers: surveyStackRequestHeaders,
							}
						);
						done();
					});
			});
		});

		it('returns 200 on success', (done) => {
			(axios.get as jest.Mock).mockResolvedValue({ data: [] });

			request(app)
				.get(`/resources`)
				.set('X-Authorization', 'name@example.com abcd1234')
				.send()
				.end(expect200AndValidateResponse(done));
		});

		describe('when resources are present', () => {
			beforeEach(() => {
				(axios.get as jest.Mock).mockImplementation((url) => {
					if (url.includes(FIELD_SURVEY_ID)) {
						return Promise.resolve(createMockSubmissionsResponse({ resourceType: ResourceTypes.Field }));
					}
					if (url.includes(AREA_SURVEY_ID)) {
						return Promise.resolve(createMockSubmissionsResponse({ resourceType: ResourceTypes.Area }));
					}
					if (url.includes(STRATIFICATION_SURVEY_ID)) {
						return Promise.resolve(createMockSubmissionsResponse({ resourceType: ResourceTypes.Stratification }));
					}
					if (url.includes(LOCATION_COLLECTION_SURVEY_ID)) {
						return Promise.resolve(createMockSubmissionsResponse({ resourceType: ResourceTypes.LocationCollection }));
					}
					if (url.includes(LOCATION_SURVEY_ID)) {
						return Promise.resolve(createMockSubmissionsResponse({ resourceType: ResourceTypes.Location }));
					}
					if (url.includes(SAMPLE_SURVEY_ID)) {
						return Promise.resolve(createMockSubmissionsResponse({ resourceType: ResourceTypes.Sample }));
					}
					if (url.includes(SAMPLING_SURVEY_ID)) {
						return Promise.resolve(createMockSubmissionsResponse({ resourceType: ResourceTypes.Sampling }));
					}
					if (url.includes(SAMPLING_COLLECTION_SURVEY_ID)) {
						return Promise.resolve(createMockSubmissionsResponse({ resourceType: ResourceTypes.SamplingCollection }));
					}
				});
			});

			it('returns 200 on success', (done) => {
				request(app)
					.get(`/resources`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send()
					.end(expect200AndValidateResponse(done));
			});

			it('looks up the featureOfInterest property for each stratification', (done) => {
				request(app)
					.get(`/resources`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.end((err, res) => {
						const stratification = JSON.parse(res.text).stratifications[0];
						expect(stratification).toHaveProperty('featureOfInterest', 'field-id');
						done();
					});
			});
		});

		it('returns 500 when surveystack returns an error', (done) => {
			(axios.get as jest.Mock).mockRejectedValueOnce('error');

			request(app).get(`/resources`).set('X-Authorization', 'name@example.com abcd1234').expect(500, done);
		});
	});
});

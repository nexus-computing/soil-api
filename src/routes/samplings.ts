import { NextFunction, Request, Response, Router } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import { ResourceTypes } from '../constants';
import { getSubmissions } from '../services/surveystack';
import { Sampling } from '../types';
import createGetResourceByIdController from './common/createGetResourceByIdController';

const router = Router();

router.get('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const samplings = await getSubmissions<Sampling>({
			resourceType: ResourceTypes.Sampling,
			authorizationHeaderValue: req.get('X-Authorization') ?? '',
		});
		res.status(200).json(samplings);
	} catch (error) {
		next(error);
	}
});

router.get('/:id', createGetResourceByIdController(ResourceTypes.Sampling));

const openApiPaths: OpenAPIV3.PathsObject = {
	'/samplings': {
		get: {
			tags: ['Samplings'],
			summary: 'Get all samplings.',
			security: [{ Authorization: [] }],
			responses: {
				'200': {
					description: 'An array of the samplings you have access to.',
					content: {
						'application/json': {
							schema: {
								type: 'array',
								items: { $ref: '#/components/schemas/ResponseSampling' },
							},
						},
					},
				},
			},
		},
	},
	'/samplings/{samplingId}': {
		get: {
			tags: ['Samplings'],
			summary: 'Get a sampling by its id.',
			security: [{ Authorization: [] }],
			parameters: [
				{
					name: 'samplingId',
					in: 'path',
					description: 'The id of the sampling to get.',
					required: true,
					schema: { type: 'string', minLength: 24, maxLength: 24 },
				},
			],
			responses: {
				'200': {
					description: 'The sampling with the given id.',
					content: {
						'application/json': {
							schema: {
								$ref: '#/components/schemas/ResponseSampling',
							},
						},
					},
				},
				'404': {
					description: 'No sampling with the given id exists.',
				},
			},
		},
	},
};

export { openApiPaths, router };

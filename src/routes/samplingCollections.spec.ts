import axios from 'axios';
import request from 'supertest';
import app from '../app';
import {
	ResourceTypes,
	SAMPLE_SURVEY_ID,
	SAMPLE_SURVEY_VERSION,
	SAMPLING_COLLECTION_SURVEY_ID,
	SAMPLING_COLLECTION_SURVEY_VERSION,
	SAMPLING_SURVEY_ID,
	SAMPLING_SURVEY_VERSION,
	SURVEYSTACK_API_URL,
} from '../constants';
import {
	createMockSample,
	createMockSampling,
	createMockSamplingCollection,
	createMockSubmissionsResponse,
	expect200AndValidateResponse,
	expect201AndValidateResponse,
	expect400AndValidateResponse,
	testGetResourceByIdEndpoint,
} from '../test-utils';
import { sampleToSubmissionData, samplingCollectionToSubmissionData, samplingToSubmissionData } from '../transforms';

jest.mock('axios');

describe('Sampling Collection Endpoints', () => {
	const mockGroupId = 'exampleGroupId';
	const surveyStackRequestHeaders = { Authorization: 'name@example.com abcd1234' };

	describe('POST', () => {
		describe('/sampling-collections', () => {
			const mockSamplingCollection = createMockSamplingCollection();
			const mockSampling = createMockSampling();
			const mockSample = createMockSample();
			const mockPopulatedSamplingCollection = {
				...mockSamplingCollection,
				members: [
					{
						...mockSampling,
						results: [mockSample],
					},
				],
			};

			it("POSTS an array of submissions (sampling collection, samplings and samples) to surveystack's submissions endpoint", (done) => {
				(axios.get as jest.Mock).mockResolvedValueOnce(
					createMockSubmissionsResponse({ resourceType: ResourceTypes.LocationCollection })
				);

				Date.now = jest.fn(() => 0);
				const surveyStackRequestBody = [
					expect.objectContaining({
						meta: {
							survey: {
								id: SAMPLING_COLLECTION_SURVEY_ID,
								version: SAMPLING_COLLECTION_SURVEY_VERSION,
							},
							group: {
								id: mockGroupId,
								path: null,
							},
							revision: 1,
							specVersion: 3,
							dateCreated: new Date(0),
							dateModified: new Date(0),
						},
						data: expect.objectContaining({
							...samplingCollectionToSubmissionData(mockSamplingCollection),
							members: expect.anything(),
							id: expect.anything(),
						}),
					}),
					expect.objectContaining({
						meta: {
							survey: {
								id: SAMPLING_SURVEY_ID,
								version: SAMPLING_SURVEY_VERSION,
							},
							group: {
								id: mockGroupId,
								path: null,
							},
							revision: 1,
							specVersion: 3,
							dateCreated: new Date(0),
							dateModified: new Date(0),
						},
						data: expect.objectContaining({
							...samplingToSubmissionData(mockSampling),
							results: expect.anything(),
							member_of: expect.anything(),
							id: expect.anything(),
						}),
					}),
					expect.objectContaining({
						meta: {
							survey: {
								id: SAMPLE_SURVEY_ID,
								version: SAMPLE_SURVEY_VERSION,
							},
							group: {
								id: mockGroupId,
								path: null,
							},
							revision: 1,
							specVersion: 3,
							dateCreated: new Date(0),
							dateModified: new Date(0),
						},
						data: expect.objectContaining({
							...sampleToSubmissionData(mockSample),
							result_of: expect.anything(),
							id: expect.anything(),
						}),
					}),
				];

				request(app)
					.post(`/sampling-collections?groupId=${mockGroupId}`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send(mockPopulatedSamplingCollection)
					.end(() => {
						expect(axios.post as jest.Mock).toHaveBeenCalledWith(
							`${SURVEYSTACK_API_URL}/submissions`,
							surveyStackRequestBody,
							{ headers: surveyStackRequestHeaders }
						);
						done();
					});
			});

			it('returns 201 on success', (done) => {
				(axios.post as jest.Mock).mockResolvedValueOnce({ data: {} });
				(axios.get as jest.Mock).mockResolvedValueOnce(
					createMockSubmissionsResponse({ resourceType: ResourceTypes.LocationCollection })
				);

				request(app)
					.post(`/sampling-collections?groupId=${mockGroupId}`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send(mockPopulatedSamplingCollection)
					.end(expect201AndValidateResponse(done));
			});

			it('returns 400 and does not create the sampling collection if the location collection referenced by samplingCollection.object does not exist', (done) => {
				(axios.get as jest.Mock).mockResolvedValueOnce({ data: [] });

				request(app)
					.post(`/sampling-collections?groupId=${mockGroupId}`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send(mockPopulatedSamplingCollection)
					.end((err, res) => {
						expect(res.status).toBe(400);
						expect(axios.post).not.toHaveBeenCalled();
						done();
					});
			});

			it('returns 500 when surveystack returns an error', (done) => {
				(axios.post as jest.Mock).mockRejectedValueOnce('error');

				request(app)
					.post(`/sampling-collections?groupId=${mockGroupId}`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send(mockPopulatedSamplingCollection)
					.expect(500, done);
			});
		});
	});

	describe('GET', () => {
		describe('/sampling-collections', () => {
			it('GETs sampling collection submissions from surveystack', (done) => {
				request(app)
					.get('/sampling-collections')
					.set('X-Authorization', 'name@example.com abcd1234')
					.end(() => {
						expect(axios.get as jest.Mock).toHaveBeenCalledWith(
							`${SURVEYSTACK_API_URL}/submissions?survey=${SAMPLING_COLLECTION_SURVEY_ID}`,
							{
								headers: surveyStackRequestHeaders,
							}
						);
						done();
					});
			});

			it('returns 200 on success when response is empty', (done) => {
				(axios.get as jest.Mock).mockResolvedValue({ data: [] });

				request(app)
					.get('/sampling-collections')
					.set('X-Authorization', 'name@example.com abcd1234')
					.send()
					.end(expect200AndValidateResponse(done));
			});

			it('returns 200 on success when resource is present', (done) => {
				(axios.get as jest.Mock).mockResolvedValue(
					createMockSubmissionsResponse({ resourceType: ResourceTypes.SamplingCollection })
				);

				request(app)
					.get('/sampling-collections')
					.set('X-Authorization', 'name@example.com abcd1234')
					.send()
					.end(expect200AndValidateResponse(done));
			});

			it('returns 500 when surveystack returns an error', (done) => {
				(axios.get as jest.Mock).mockRejectedValueOnce('error');

				request(app).get(`/sampling-collections`).set('X-Authorization', 'name@example.com abcd1234').expect(500, done);
			});

			it("returns 200 when valid ISO8601 timestamps are passed in the 'submittedAfter' query parameter", (done) => {
				(axios.get as jest.Mock).mockResolvedValue({ data: [] });

				request(app)
					.get('/sampling-collections')
					.set('X-Authorization', 'name@example.com abcd1234')
					.query({ submittedAfter: '2020-01-01T00:00:00.000Z' })
					.send()
					.end(expect200AndValidateResponse(done));
			});

			const invalidSubmittedAfterValues = [
				{ description: 'empty string', submittedAfter: '' },
				{ description: 'invalid date', submittedAfter: '2020-01-01T00:00:00.000' },
				{ description: 'ms since epoch', submittedAfter: '1577836800000' },
			];

			invalidSubmittedAfterValues.forEach(({ description, submittedAfter }) => {
				it(`returns 400 when ${description} is passed in the \'submittedAfter\' query paramter`, (done) => {
					(axios.get as jest.Mock).mockResolvedValue({ data: [] });

					request(app)
						.get('/sampling-collections')
						.set('X-Authorization', 'name@example.com abcd1234')
						.query({ submittedAfter })
						.send()
						.end(expect400AndValidateResponse(done));
				});
			});
		});

		testGetResourceByIdEndpoint({
			endpointPath: '/sampling-collections',
			resourceName: 'sampling collection',
			resourceType: ResourceTypes.SamplingCollection,
		});
	});
});

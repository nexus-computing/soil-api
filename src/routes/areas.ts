import { NextFunction, Request, Response, Router } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import { ResourceTypes } from '../constants';
import {
	createMatchStratificationsByAreaIds,
	getSubmission,
	getSubmissions,
	updateSubmission,
} from '../services/surveystack';
import { Area, Stratification } from '../types';
import createGetResourceByIdController from './common/createGetResourceByIdController';

const router = Router();

router.get('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const areas = await getSubmissions<Area>({
			resourceType: ResourceTypes.Area,
			authorizationHeaderValue: req.get('X-Authorization') ?? '',
		});

		res.status(200).json(areas);
	} catch (error) {
		next(error);
	}
});

router.get('/:id', createGetResourceByIdController(ResourceTypes.Area));

router.patch('/:id', async (req: Request, res: Response) => {
	try {
		const id = req.params.id;
		const authorizationHeaderValue = req.get('X-Authorization') ?? '';

		const [area, stratifications] = await Promise.all([
			getSubmission<Area>({
				resourceType: ResourceTypes.Area,
				authorizationHeaderValue,
				id,
			}),
			getSubmissions<Stratification>({
				resourceType: ResourceTypes.Stratification,
				authorizationHeaderValue,
				matchExpression: createMatchStratificationsByAreaIds([id]),
			}),
		]);

		// Validate if area is exist
		if (!area) {
			return res.status(404).send();
		}

		// Validate conflicts
		if (stratifications.length > 0) {
			return res.status(409).send({
				message: 'Cannot edit an area that has a stratification depending on it',
			});
		}

		// Patch
		const updatedArea: Area = { ...area, ...req.body };
		if (area.properties.drawn) {
			updatedArea.properties.drawn = true;
		}
		await updateSubmission({
			resourceType: ResourceTypes.Area,
			resource: updatedArea,
			groupId: updatedArea.meta?.groupId ?? '',
			authorizationHeaderValue,
		});

		return res.status(204).send();
	} catch (error) {
		return res.status(500).send();
	}
});

const openApiPaths: OpenAPIV3.PathsObject = {
	'/areas': {
		get: {
			tags: ['Areas'],
			summary: 'Get all areas.',
			security: [{ Authorization: [] }],
			responses: {
				'200': {
					description: 'An array of the areas you have access to.',
					content: {
						'application/json': {
							schema: {
								type: 'array',
								items: { $ref: '#/components/schemas/ResponseArea' },
							},
						},
					},
				},
			},
		},
	},
	'/areas/{areaId}': {
		get: {
			tags: ['Areas'],
			summary: 'Get an area by its id.',
			security: [{ Authorization: [] }],
			parameters: [
				{
					name: 'areaId',
					in: 'path',
					description: 'The id of the area to get.',
					required: true,
					schema: { type: 'string', minLength: 24, maxLength: 24 },
				},
			],
			responses: {
				'200': {
					description: 'The area with the given id.',
					content: {
						'application/json': {
							schema: {
								$ref: '#/components/schemas/ResponseArea',
							},
						},
					},
				},
				'404': {
					description: 'No area with the given id exists.',
				},
			},
		},
		patch: {
			tags: ['Areas'],
			summary: 'Updates an area.',
			security: [{ Authorization: [] }],
			parameters: [
				{
					name: 'areaId',
					in: 'path',
					description: 'The id of the area to edit.',
					required: true,
					schema: { type: 'string', minLength: 24, maxLength: 24 },
				},
			],
			requestBody: {
				description: 'The area object to be saved.',
				required: true,
				content: {
					'application/json': {
						schema: {
							$ref: '#/components/schemas/EditableArea',
						},
					},
				},
			},
			responses: {
				'204': {
					description: 'The field was saved successfully.',
				},
				'404': {
					description: 'No field with the given id exists.',
				},
				'409': {
					description: 'Cannot edit an area that has resources that depend on it.',
					content: {
						'application/json': {
							schema: {
								$ref: '#/components/schemas/Error',
							},
						},
					},
				},
			},
		},
	},
};

export { openApiPaths, router };

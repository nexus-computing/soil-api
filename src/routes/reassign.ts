import { NextFunction, Request, Response, Router } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import { getResourceSubmissionIdsRelatedToField } from '../resources/utils';
import { bulkReassign, getAllResources } from '../services/surveystack';

const router = Router();

router.patch('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const authorizationHeaderValue = String(req.headers['x-authorization']);
		const [
			fields,
			areas,
			stratifications,
			locationCollections,
			locations,
			samples,
			samplings,
			samplingCollections,
		]: any = await getAllResources({ authorizationHeaderValue });

		const resourceSubmissionIdsToReassign = getResourceSubmissionIdsRelatedToField(
			{
				fields,
				areas,
				stratifications,
				locationCollections,
				locations,
				samples,
				samplings,
				samplingCollections,
			},
			req.body.fieldId
		);

		await bulkReassign({
			submissionIds: resourceSubmissionIdsToReassign,
			groupId: req.body.groupId,
			authorizationHeaderValue,
		});

		res.status(200).send();
	} catch (error) {
		next(error);
	}
});

const openApiPaths: OpenAPIV3.PathsObject = {
	'/reassign': {
		patch: {
			summary: 'Reassigns a field and all related resources to a group.',
			security: [{ Authorization: [] }],
			requestBody: {
				description: 'The id of the field to be reassigned, and the id of the group to reassign it to.',
				content: {
					'application/json': {
						schema: {
							type: 'object',
							required: ['fieldId', 'groupId'],
							additionalProperties: false,
							properties: {
								fieldId: {
									type: 'string',
								},
								groupId: {
									type: 'string',
								},
							},
						},
					},
				},
				required: true,
			},
			responses: {
				'200': {
					description: 'Successfully reassigned the field and its related resources to the group.',
				},
			},
		},
	},
};

export { openApiPaths, router };

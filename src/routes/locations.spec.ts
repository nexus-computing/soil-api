import axios from 'axios';
import request from 'supertest';
import app from '../app';
import { LOCATION_SURVEY_ID, ResourceTypes, SURVEYSTACK_API_URL } from '../constants';
import {
	createMockSubmissionsResponse,
	expect200AndValidateResponse,
	testGetResourceByIdEndpoint,
} from '../test-utils';

jest.mock('axios');

describe('Location Endpoints', () => {
	describe('GET', () => {
		describe('/locations', () => {
			it('GETs location submissions from surveystack', (done) => {
				const surveyStackRequestHeaders = { Authorization: 'name@example.com abcd1234' };
				request(app)
					.get('/locations')
					.set('X-Authorization', 'name@example.com abcd1234')
					.end(() => {
						expect(axios.get as jest.Mock).toHaveBeenCalledWith(
							`${SURVEYSTACK_API_URL}/submissions?survey=${LOCATION_SURVEY_ID}`,
							{
								headers: surveyStackRequestHeaders,
							}
						);
						done();
					});
			});

			it('returns 200 on success when response is empty', (done) => {
				(axios.get as jest.Mock).mockResolvedValue({ data: [] });

				request(app)
					.get('/locations')
					.set('X-Authorization', 'name@example.com abcd1234')
					.send()
					.end(expect200AndValidateResponse(done));
			});

			it('returns 200 on success when resource is present', (done) => {
				(axios.get as jest.Mock).mockResolvedValue(
					createMockSubmissionsResponse({ resourceType: ResourceTypes.Location })
				);

				request(app)
					.get('/locations')
					.set('X-Authorization', 'name@example.com abcd1234')
					.send()
					.end(expect200AndValidateResponse(done));
			});

			it('returns 500 when surveystack returns an error', (done) => {
				(axios.get as jest.Mock).mockRejectedValueOnce('error');

				request(app).get(`/locations`).set('X-Authorization', 'name@example.com abcd1234').expect(500, done);
			});
		});

		testGetResourceByIdEndpoint({
			endpointPath: '/locations',
			resourceName: 'location',
			resourceType: ResourceTypes.Location,
		});
	});
});

import { NextFunction, Request, Response, Router } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import { getGroups } from '../services/surveystack';

const router = Router();

router.get('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const groups = await getGroups({
			authorizationHeaderValue: req.get('X-Authorization') ?? '',
		});
		res.status(200).json(groups);
	} catch (error) {
		next(error);
	}
});

const openApiPaths: OpenAPIV3.PathsObject = {
	'/groups': {
		get: {
			summary: 'Get all groups that you are an admin of.',
			security: [{ Authorization: [] }],
			responses: {
				200: {
					description: 'An array of groups.',
					content: {
						'application/json': {
							schema: {
								type: 'array',
								items: { $ref: '#/components/schemas/Group' },
							},
						},
					},
				},
			},
		},
	},
};

export { openApiPaths, router };

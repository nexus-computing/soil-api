import axios from 'axios';
import request from 'supertest';
import app from '../app';
import {
	AREA_SURVEY_ID,
	AREA_SURVEY_VERSION,
	FIELD_SURVEY_ID,
	FIELD_SURVEY_VERSION,
	ResourceTypes,
	SAMPLING_COLLECTION_SURVEY_ID,
	STRATIFICATION_SURVEY_ID,
	SURVEYSTACK_API_URL,
} from '../constants';
import {
	createMockAddress,
	createMockArea,
	createMockContactPoint,
	createMockField,
	createMockResponseSubmission,
	createMockSubmissionsResponse,
	expect200AndValidateResponse,
	expect201AndValidateResponse,
	expect204AndValidateResponse,
	expect404AndValidateResponse,
	expect409AndValidateResponse,
	testGetResourceByIdEndpoint,
} from '../test-utils';
import { areaToSubmissionData, fieldToSubmissionData } from '../transforms';
import { Field } from '../types';

jest.mock('axios');

describe('Field Endpoints', () => {
	const mockGroupId = 'exampleGroupId';
	const surveyStackRequestHeaders = { Authorization: 'name@example.com abcd1234' };

	describe('POST', () => {
		describe('/fields', () => {
			const mockField = createMockField();
			const mockArea = {
				...createMockArea(),
				properties: {
					...createMockArea().properties,
					drawn: true,
				},
			};
			const mockPopulatedField = {
				...mockField,
				areas: [mockArea],
			};

			it("POSTS an array of submissions (field and areas) to surveystack's submissions endpoint", (done) => {
				Date.now = jest.fn(() => 0);
				const surveyStackRequestBody = [
					expect.objectContaining({
						meta: {
							survey: {
								id: FIELD_SURVEY_ID,
								version: FIELD_SURVEY_VERSION,
							},
							group: {
								id: mockGroupId,
								path: null,
							},
							revision: 1,
							specVersion: 3,
							dateCreated: new Date(0),
							dateModified: new Date(0),
						},
						data: expect.objectContaining({
							...fieldToSubmissionData(mockField),
							areas: expect.anything(),
							id: expect.anything(),
						}),
					}),
					expect.objectContaining({
						meta: {
							survey: {
								id: AREA_SURVEY_ID,
								version: AREA_SURVEY_VERSION,
							},
							group: {
								id: mockGroupId,
								path: null,
							},
							revision: 1,
							specVersion: 3,
							dateCreated: new Date(0),
							dateModified: new Date(0),
						},
						data: expect.objectContaining({
							...areaToSubmissionData(mockArea),
							id: expect.anything(),
						}),
					}),
				];

				request(app)
					.post(`/fields?groupId=${mockGroupId}`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send(mockPopulatedField)
					.end(() => {
						expect(axios.post as jest.Mock).toHaveBeenCalledWith(
							`${SURVEYSTACK_API_URL}/submissions`,
							surveyStackRequestBody,
							{ headers: surveyStackRequestHeaders }
						);
						done();
					});
			});

			it('returns 201 on success', (done) => {
				(axios.post as jest.Mock).mockResolvedValueOnce({ data: {} });

				request(app)
					.post(`/fields?groupId=${mockGroupId}`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send(mockPopulatedField)
					.end(expect201AndValidateResponse(done));
			});

			it('returns 500 when surveystack returns an error', (done) => {
				(axios.post as jest.Mock).mockRejectedValueOnce('error');

				request(app)
					.post(`/fields?groupId=${mockGroupId}`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send(mockPopulatedField)
					.expect(500, done);
			});
		});
	});

	describe('GET', () => {
		describe('/fields', () => {
			it('GETs field submissions from surveystack', (done) => {
				request(app)
					.get(`/fields`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.end(() => {
						expect(axios.get as jest.Mock).toHaveBeenCalledWith(
							`${SURVEYSTACK_API_URL}/submissions?survey=${FIELD_SURVEY_ID}`,
							{
								headers: surveyStackRequestHeaders,
							}
						);
						done();
					});
			});

			it('returns 200 on success when no resources are present', (done) => {
				(axios.get as jest.Mock).mockResolvedValue({ data: [] });

				request(app)
					.get(`/fields`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send()
					.end(expect200AndValidateResponse(done));
			});

			it('returns 200 on success when resources are present', (done) => {
				(axios.get as jest.Mock).mockResolvedValueOnce(
					createMockSubmissionsResponse({ resourceType: ResourceTypes.Field })
				);

				request(app)
					.get(`/fields`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send()
					.end(expect200AndValidateResponse(done));
			});

			it('returns 500 when surveystack returns an error', (done) => {
				(axios.get as jest.Mock).mockRejectedValueOnce('error');

				request(app).get(`/fields`).set('X-Authorization', 'name@example.com abcd1234').expect(500, done);
			});
		});

		testGetResourceByIdEndpoint({
			endpointPath: '/fields',
			resourceName: 'field',
			resourceType: ResourceTypes.Field,
		});
	});

	describe('PATCH', () => {
		describe('/fields/:id', () => {
			const fieldId = '123456789012345678901234';
			const mockField = createMockField({ id: fieldId });
			const { name, producerName, address, contactPoints } = mockField;
			const mockEditableField: Partial<Field> = {
				name,
				producerName,
				address,
				contactPoints,
			};

			it(`returns 204 when a field is saved`, (done) => {
				(axios.get as jest.Mock).mockResolvedValueOnce(
					createMockSubmissionsResponse({ resourceType: ResourceTypes.Field })
				);

				request(app)
					.patch(`/fields/${fieldId}`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send(mockEditableField)
					.end(expect204AndValidateResponse(done));
			});

			it(`saves 'name', 'producerName', 'address' and 'contactPoints' properties only`, (done) => {
				(axios.get as jest.Mock).mockResolvedValueOnce({
					data: [createMockResponseSubmission({ resourceType: ResourceTypes.Field })],
				});

				Date.now = jest.fn(() => 0);

				const updatedField: Partial<Field> = {
					name: 'modified name',
					producerName: 'modified producerName',
					address: createMockAddress({
						streetAddress: 'modified streetAddress',
						addressLocality: 'modified addressLocality',
						addressRegion: 'modified addressRegion',
						addressCountry: 'modified addressCountry',
						postalCode: 'modified postalCode',
					}),
					contactPoints: [
						createMockContactPoint({
							telephone: 'modified telephone',
							email: 'modified email',
							name: 'modified name',
							contactType: 'modified contactType',
							organization: 'modified organization',
						}),
					],
				};

				const surveyStackRequestBody = expect.objectContaining({
					_id: 'field-id',
					meta: expect.objectContaining({
						survey: {
							id: FIELD_SURVEY_ID,
							version: FIELD_SURVEY_VERSION,
						},
						group: {
							id: '',
							path: null,
						},
						specVersion: 3,
						dateModified: new Date(0),
						archivedReason: 'RESUBMIT',
					}),
					data: expect.objectContaining({
						...fieldToSubmissionData({ ...mockField, ...updatedField, id: 'field-id' }),
						alternate_name: {
							value: 'field-alternate-name',
							meta: expect.anything(),
						},
						areas: expect.objectContaining({
							value: JSON.stringify(['area-id']),
							meta: expect.anything(),
						}),
						meta: expect.anything(),
					}),
				});

				request(app)
					.patch(`/fields/${fieldId}`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send(updatedField)
					.end(() => {
						expect(axios.put as jest.Mock).toHaveBeenCalledWith(
							`${SURVEYSTACK_API_URL}/submissions/field-id`,
							surveyStackRequestBody,
							{ headers: surveyStackRequestHeaders }
						);
						done();
					});
			});

			it(`returns 404 when a field is not found`, (done) => {
				(axios.get as jest.Mock).mockResolvedValueOnce({ data: [] });

				request(app)
					.patch(`/fields/${fieldId}`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send(mockEditableField)
					.end(expect404AndValidateResponse(done));
			});

			it("returns 500 when surveystack's get submission endpoint returns an error", (done) => {
				(axios.get as jest.Mock).mockRejectedValueOnce('error');

				request(app)
					.patch(`/fields/${fieldId}`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send(mockEditableField)
					.expect(500, done);
			});

			it("returns 500 when surveystack's resubmit endpoint returns an error", (done) => {
				(axios.get as jest.Mock).mockResolvedValueOnce(
					createMockSubmissionsResponse({ resourceType: ResourceTypes.Field })
				);
				(axios.put as jest.Mock).mockRejectedValueOnce('error');

				request(app)
					.patch(`/fields/${fieldId}`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send(mockEditableField)
					.expect(500, done);
			});
		});
	});

	describe('DELETE', () => {
		describe('/fields/:id', () => {
			const mockField = createMockResponseSubmission({ resourceType: ResourceTypes.Field });

			it("POSTS the submissionIds (field, areas) to surveystack's submissions/bulk-archive endpoint", (done) => {
				(axios.get as jest.Mock).mockImplementation((url) => {
					if (url.includes(FIELD_SURVEY_ID)) {
						return Promise.resolve({
							data: [
								{
									...mockField,
									data: {
										...mockField.data,
										id: { value: '123456789012345678901234' },
										areas: { value: JSON.stringify(['area-1', 'area-2']) },
									},
								},
							],
						});
					}
					if (url.includes(STRATIFICATION_SURVEY_ID)) {
						return Promise.resolve({ data: [] });
					}
					if (url.includes(SAMPLING_COLLECTION_SURVEY_ID)) {
						return Promise.resolve({ data: [] });
					}
				});

				request(app)
					.delete('/fields/123456789012345678901234')
					.set('X-Authorization', 'name@example.com abcd1234')
					.set('X-Super-Authorization', 'password')
					.send()
					.end(() => {
						expect(axios.post as jest.Mock).toHaveBeenCalledWith(
							`${SURVEYSTACK_API_URL}/submissions/bulk-archive?set=true&reason=INCORRECT_DATA`,
							{ ids: ['area-1', 'area-2', '123456789012345678901234'] },
							{ headers: surveyStackRequestHeaders }
						);
						done();
					});
			});

			it('returns 204 on success when the field exists and has no dependent resources', (done) => {
				(axios.get as jest.Mock).mockImplementation((url) => {
					if (url.includes(FIELD_SURVEY_ID)) {
						return Promise.resolve({ data: [mockField] });
					}
					if (url.includes(STRATIFICATION_SURVEY_ID) || url.includes(SAMPLING_COLLECTION_SURVEY_ID)) {
						return Promise.resolve({ data: [] });
					}
				});

				request(app)
					.delete('/fields/123456789012345678901234')
					.set('X-Authorization', 'name@example.com abcd1234')
					.set('X-Super-Authorization', 'password')
					.send()
					.end(expect204AndValidateResponse(done));
			});

			it('returns 404 when no matching field exists', (done) => {
				(axios.get as jest.Mock).mockImplementation((url) => {
					if (url.includes(FIELD_SURVEY_ID)) {
						return Promise.resolve({ data: [] });
					}
				});

				request(app)
					.delete('/fields/123456789012345678901234')
					.set('X-Authorization', 'name@example.com abcd1234')
					.set('X-Super-Authorization', 'password')
					.send()
					.end(expect404AndValidateResponse(done));
			});

			it('returns 409 when any stratification depends on the field', (done) => {
				(axios.get as jest.Mock).mockImplementation((url) => {
					if (url.includes(FIELD_SURVEY_ID)) {
						return Promise.resolve({ data: [mockField] });
					}
					if (url.includes(STRATIFICATION_SURVEY_ID)) {
						return Promise.resolve(createMockSubmissionsResponse({ resourceType: ResourceTypes.Stratification }));
					}
					if (url.includes(SAMPLING_COLLECTION_SURVEY_ID)) {
						return Promise.resolve({ data: [] });
					}
				});

				request(app)
					.delete('/fields/123456789012345678901234')
					.set('X-Authorization', 'name@example.com abcd1234')
					.set('X-Super-Authorization', 'password')
					.send()
					.end(expect409AndValidateResponse(done));
			});

			it('returns 409 when any samplingCollection depends on the field', (done) => {
				(axios.get as jest.Mock).mockImplementation((url) => {
					if (url.includes(FIELD_SURVEY_ID)) {
						return Promise.resolve({ data: [mockField] });
					}
					if (url.includes(STRATIFICATION_SURVEY_ID)) {
						return Promise.resolve({ data: [] });
					}
					if (url.includes(SAMPLING_COLLECTION_SURVEY_ID)) {
						return Promise.resolve(createMockSubmissionsResponse({ resourceType: ResourceTypes.SamplingCollection }));
					}
				});

				request(app)
					.delete('/fields/123456789012345678901234')
					.set('X-Authorization', 'name@example.com abcd1234')
					.set('X-Super-Authorization', 'password')
					.send()
					.end(expect409AndValidateResponse(done));
			});

			it('returns 500 when surveystack returns an error', (done) => {
				(axios.get as jest.Mock).mockRejectedValueOnce('error');

				request(app)
					.delete('/fields/123456789012345678901234')
					.set('X-Authorization', 'name@example.com abcd1234')
					.set('X-Super-Authorization', 'password')
					.send()
					.expect(500, done);
			});
		});
	});
});

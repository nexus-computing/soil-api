import axios from 'axios';
import request from 'supertest';
import app from '../app';
import { expect200AndValidateResponse } from '../test-utils';

jest.mock('axios');

describe('/groups', () => {
	describe('GET', () => {
		it('returns 200 on success', (done) => {
			(axios.get as jest.Mock).mockResolvedValue({ data: [] });

			request(app)
				.get(`/groups`)
				.set('X-Authorization', 'name@example.com abcd1234')
				.send()
				.end(expect200AndValidateResponse(done));
		});

		it('returns 500 when surveystack returns an error', (done) => {
			(axios.get as jest.Mock).mockRejectedValueOnce('error');

			request(app).get(`/groups`).set('X-Authorization', 'name@example.com abcd1234').expect(500, done);
		});
	});
});

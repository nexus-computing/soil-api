import ObjectID from 'bson-objectid';
import { NextFunction, Request, Response, Router } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import { ResourceTypes } from '../constants';
import { groupIdQueryParam } from '../open-api-spec/common/queryParameters';
import { createArea, createField } from '../resources';
import {
	bulkArchive,
	createMatchSamplingCollectionsByFieldId,
	createMatchStratificationsByAreaIds,
	createSubmissions,
	getSubmission,
	getSubmissions,
	updateSubmission,
} from '../services/surveystack';
import { Area, Field, SamplingCollection, Stratification } from '../types';
import createGetResourceByIdController from './common/createGetResourceByIdController';

const router = Router();

router.post('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const areas: Area[] = req.body.areas.map((area: Area) =>
			createArea({
				id: new ObjectID().toHexString(),
				...area,
			})
		);

		const field = createField({
			id: new ObjectID().toHexString(),
			...req.body,
			areas: areas.map(({ id }) => id),
		});

		await createSubmissions({
			resources: [
				{ resource: field, resourceType: ResourceTypes.Field },
				...areas.map((area) => ({ resource: area, resourceType: ResourceTypes.Area })),
			],
			groupId: String(req.query.groupId),
			authorizationHeaderValue: req.get('X-Authorization') ?? '',
		});

		return res.status(201).json({ fields: [field], areas });
	} catch (error) {
		next(error);
	}
});

router.get('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const fields = await getSubmissions<Field>({
			resourceType: ResourceTypes.Field,
			authorizationHeaderValue: req.get('X-Authorization') ?? '',
		});

		return res.status(200).json(fields);
	} catch (error) {
		next(error);
	}
});

router.get('/:id', createGetResourceByIdController(ResourceTypes.Field));

router.delete('/:id', async (req: Request, res: Response) => {
	try {
		const authorizationHeaderValue = req.get('X-Authorization') ?? '';

		// Fetch field to be deleted
		const id = req.params.id;
		const field = await getSubmission<Field>({
			resourceType: ResourceTypes.Field,
			authorizationHeaderValue,
			id,
		});

		if (!field) {
			return res.status(404).send();
		}

		// Validate conflicts
		const [stratifications, samplingCollections] = await Promise.all([
			getSubmissions<Stratification>({
				resourceType: ResourceTypes.Stratification,
				authorizationHeaderValue,
				matchExpression: createMatchStratificationsByAreaIds(field.areas),
			}),
			getSubmissions<SamplingCollection>({
				resourceType: ResourceTypes.SamplingCollection,
				authorizationHeaderValue,
				matchExpression: createMatchSamplingCollectionsByFieldId(field.id ?? ''),
			}),
		]);

		if ([...stratifications, ...samplingCollections].length > 0) {
			return res.status(409).send({
				message:
					'Cannot delete a field that has a stratification, locationCollection, locations, samplingCollection, samplings, or samples that depend on it',
			});
		}

		// Delete
		await bulkArchive({ authorizationHeaderValue, ids: [...field.areas, id] });
		return res.status(204).send();
	} catch (error) {
		return res.status(500).send();
	}
});

router.patch('/:id', async (req: Request, res: Response) => {
	try {
		const id = req.params.id;
		const authorizationHeaderValue = req.get('X-Authorization') ?? '';

		// Validate if field is exist
		const field = await getSubmission<Field>({
			resourceType: ResourceTypes.Field,
			authorizationHeaderValue,
			id,
		});

		if (!field) {
			return res.status(404).send();
		}

		// Patch
		const updatedField: Field = { ...field, ...req.body };
		await updateSubmission({
			resourceType: ResourceTypes.Field,
			resource: updatedField,
			groupId: updatedField.meta.groupId ?? '',
			authorizationHeaderValue,
		});

		return res.status(204).send();
	} catch (error) {
		return res.status(500).send();
	}
});

const openApiPaths: OpenAPIV3.PathsObject = {
	'/fields': {
		post: {
			tags: ['Fields'],
			summary: 'Saves a field.',
			parameters: [groupIdQueryParam],
			security: [{ Authorization: [] }],
			requestBody: {
				description: 'The field object to be saved.',
				content: {
					'application/json': {
						schema: {
							$ref: '#/components/schemas/PopulatedField',
						},
					},
				},
				required: true,
			},
			responses: {
				'201': {
					description: 'Successfully saved a field.',
					content: {
						'application/json': {
							schema: {
								type: 'object',
								required: ['fields', 'areas'],
								properties: {
									fields: { type: 'array', items: { $ref: '#/components/schemas/ResponseField' } },
									areas: { type: 'array', items: { $ref: '#/components/schemas/ResponseArea' } },
								},
							},
						},
					},
				},
			},
		},
		get: {
			tags: ['Fields'],
			summary: 'Get all fields.',
			security: [{ Authorization: [] }],
			responses: {
				'200': {
					description: 'An array of the fields you have access to.',
					content: {
						'application/json': {
							schema: {
								type: 'array',
								items: { $ref: '#/components/schemas/ResponseField' },
							},
						},
					},
				},
			},
		},
	},
	'/fields/{fieldId}': {
		get: {
			tags: ['Fields'],
			summary: 'Get a field by its id.',
			security: [{ Authorization: [] }],
			parameters: [
				{
					name: 'fieldId',
					in: 'path',
					description: 'The id of the field to get.',
					required: true,
					schema: { type: 'string', minLength: 24, maxLength: 24 },
				},
			],
			responses: {
				'200': {
					description: 'The field with the given id.',
					content: {
						'application/json': {
							schema: {
								$ref: '#/components/schemas/ResponseField',
							},
						},
					},
				},
				'404': {
					description: 'No field with the given id exists.',
				},
			},
		},
		patch: {
			tags: ['Fields'],
			summary: 'Updates a field.',
			security: [{ Authorization: [] }],
			parameters: [
				{
					name: 'fieldId',
					in: 'path',
					description: 'The id of the field to edit.',
					required: true,
					schema: { type: 'string', minLength: 24, maxLength: 24 },
				},
			],
			requestBody: {
				description: 'The field object to be saved.',
				required: true,
				content: {
					'application/json': {
						schema: {
							$ref: '#/components/schemas/EditableField',
						},
					},
				},
			},
			responses: {
				'204': {
					description: 'The field was saved successfully.',
				},
				'404': {
					description: 'No field with the given id exists.',
				},
			},
		},
		delete: {
			'x-internal': true,
			tags: ['Fields'],
			summary: 'Delete a field by its id.',
			security: [{ Authorization: [], 'X-Super-Authorization': [] }],
			parameters: [
				{
					name: 'fieldId',
					in: 'path',
					description: 'The id of the field to delete.',
					required: true,
					schema: { type: 'string', minLength: 24, maxLength: 24 },
				},
			],
			responses: {
				'204': {
					description: 'The field was deleted successfully.',
				},
				'404': {
					description: 'No field with the given id exists.',
				},
				'409': {
					description: 'Cannot delete a field that has resources that depend on it.',
					content: {
						'application/json': {
							schema: {
								$ref: '#/components/schemas/Error',
							},
						},
					},
				},
			},
		} as OpenAPIV3.OperationObject,
	},
};

export { openApiPaths, router };

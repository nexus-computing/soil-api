import axios from 'axios';
import request from 'supertest';
import app from '../app';
import { ResourceTypes, SAMPLING_SURVEY_ID, SURVEYSTACK_API_URL } from '../constants';
import {
	createMockSubmissionsResponse,
	expect200AndValidateResponse,
	testGetResourceByIdEndpoint,
} from '../test-utils';

jest.mock('axios');

describe('Sampling Endpoints', () => {
	const surveyStackRequestHeaders = { Authorization: 'name@example.com abcd1234' };

	describe('GET', () => {
		describe('/samplings', () => {
			it('GETs sampling submissions from surveystack', (done) => {
				request(app)
					.get('/samplings')
					.set('X-Authorization', 'name@example.com abcd1234')
					.end(() => {
						expect(axios.get as jest.Mock).toHaveBeenCalledWith(
							`${SURVEYSTACK_API_URL}/submissions?survey=${SAMPLING_SURVEY_ID}`,
							{
								headers: surveyStackRequestHeaders,
							}
						);
						done();
					});
			});

			it('returns 200 on success when response is empty', (done) => {
				(axios.get as jest.Mock).mockResolvedValueOnce({ data: [] });

				request(app)
					.get('/samplings')
					.set('X-Authorization', 'name@example.com abcd1234')
					.send()
					.end(expect200AndValidateResponse(done));
			});

			it('returns 200 on success when resource is present', (done) => {
				(axios.get as jest.Mock).mockResolvedValueOnce(
					createMockSubmissionsResponse({ resourceType: ResourceTypes.Sampling })
				);

				request(app)
					.get('/samplings')
					.set('X-Authorization', 'name@example.com abcd1234')
					.send()
					.end(expect200AndValidateResponse(done));
			});

			it('returns 500 when surveystack returns an error', (done) => {
				(axios.get as jest.Mock).mockRejectedValueOnce('error');

				request(app).get(`/samplings`).set('X-Authorization', 'name@example.com abcd1234').expect(500, done);
			});
		});

		testGetResourceByIdEndpoint({
			endpointPath: '/samplings',
			resourceName: 'sampling',
			resourceType: ResourceTypes.Sampling,
		});
	});
});

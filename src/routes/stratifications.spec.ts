import axios from 'axios';
import request from 'supertest';
import app from '../app';
import {
	AREA_SURVEY_ID,
	FIELD_SURVEY_ID,
	LOCATION_COLLECTION_SURVEY_ID,
	LOCATION_COLLECTION_SURVEY_VERSION,
	LOCATION_SURVEY_ID,
	LOCATION_SURVEY_VERSION,
	ResourceTypes,
	SAMPLING_COLLECTION_SURVEY_ID,
	STRATIFICATION_SURVEY_ID,
	STRATIFICATION_SURVEY_VERSION,
	SURVEYSTACK_API_URL,
} from '../constants';
import {
	createMockLocation,
	createMockLocationCollection,
	createMockResponseSubmission,
	createMockStratification,
	createMockSubmissionsResponse,
	expect200AndValidateResponse,
	expect201AndValidateResponse,
	expect204AndValidateResponse,
	expect400AndValidateResponse,
	expect404AndValidateResponse,
	expect409AndValidateResponse,
} from '../test-utils';
import {
	locationCollectionToSubmissionData,
	locationToSubmissionData,
	stratificationToSubmissionData,
} from '../transforms';

jest.mock('axios');

describe('Stratification Endpoints', () => {
	const mockGroupId = 'exampleGroupId';
	const surveyStackRequestHeaders = { Authorization: 'name@example.com abcd1234' };

	describe('POST', () => {
		describe('/stratifications', () => {
			const stratificationObjectValue = '123';
			const mockStratification = createMockStratification({ object: stratificationObjectValue });
			const mockLocationCollection = createMockLocationCollection();
			const mockLocation = createMockLocation();
			const mockRequestBody = {
				stratification: mockStratification,
				locationCollection: {
					...mockLocationCollection,
					features: [mockLocation],
				},
			};

			it("POSTS an array of submissions (stratification, location collection, locations) to surveystack's submissions endpoint", (done) => {
				(axios.get as jest.Mock).mockImplementation(async (url) => {
					if (url.includes(FIELD_SURVEY_ID)) {
						return createMockSubmissionsResponse({ resourceType: ResourceTypes.Field });
					}
					if (url.includes(AREA_SURVEY_ID)) {
						return createMockSubmissionsResponse({ resourceType: ResourceTypes.Area });
					}
				});

				Date.now = jest.fn(() => 0);
				const surveyStackRequestBody = [
					expect.objectContaining({
						meta: {
							survey: {
								id: STRATIFICATION_SURVEY_ID,
								version: STRATIFICATION_SURVEY_VERSION,
							},
							group: {
								id: mockGroupId,
								path: null,
							},
							revision: 1,
							specVersion: 3,
							dateCreated: new Date(0),
							dateModified: new Date(0),
						},
						data: expect.objectContaining({
							...stratificationToSubmissionData(mockStratification),
							id: expect.anything(),
						}),
					}),
					expect.objectContaining({
						meta: {
							survey: {
								id: LOCATION_COLLECTION_SURVEY_ID,
								version: LOCATION_COLLECTION_SURVEY_VERSION,
							},
							group: {
								id: mockGroupId,
								path: null,
							},
							revision: 1,
							specVersion: 3,
							dateCreated: new Date(0),
							dateModified: new Date(0),
						},
						data: {
							...locationCollectionToSubmissionData(mockLocationCollection),
							result_of: expect.anything(),
							features: expect.anything(),
							id: expect.anything(),
						},
					}),
					expect.objectContaining({
						meta: {
							survey: {
								id: LOCATION_SURVEY_ID,
								version: LOCATION_SURVEY_VERSION,
							},
							group: {
								id: mockGroupId,
								path: null,
							},
							revision: 1,
							specVersion: 3,
							dateCreated: new Date(0),
							dateModified: new Date(0),
						},
						data: expect.objectContaining({
							...locationToSubmissionData(mockLocation),
							id: expect.anything(),
						}),
					}),
				];

				request(app)
					.post(`/stratifications?groupId=${mockGroupId}`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send(mockRequestBody)
					.end(() => {
						expect(axios.post as jest.Mock).toHaveBeenCalledWith(
							`${SURVEYSTACK_API_URL}/submissions`,
							surveyStackRequestBody,
							{ headers: surveyStackRequestHeaders }
						);
						done();
					});
			});

			it("gets the field submission that contains the area referenced in the stratification's object property, in order to populate the stratification's featureOfInterest property in the response", (done) => {
				request(app)
					.post(`/stratifications?groupId=${mockGroupId}`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send(mockRequestBody)
					.end(() => {
						expect(axios.get).toHaveBeenCalledWith(
							expect.stringContaining(stratificationObjectValue),
							expect.anything()
						);
						expect(axios.get).toHaveBeenCalledWith(expect.stringContaining(FIELD_SURVEY_ID), expect.anything());
						done();
					});
			});

			it('returns 400 and does not create the stratification if the referenced area (stratification.object) does not exist', (done) => {
				(axios.get as jest.Mock).mockImplementation(async (url) => {
					if (url.includes(FIELD_SURVEY_ID)) {
						return createMockSubmissionsResponse({ resourceType: ResourceTypes.Field });
					}
					if (url.includes(AREA_SURVEY_ID)) {
						return { data: [] };
					}
				});

				request(app)
					.post(`/stratifications?groupId=${mockGroupId}`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send(mockRequestBody)
					.end((err, res) => {
						expect(res.status).toBe(400);
						expect(axios.post).not.toHaveBeenCalled();
						done();
					});
			});

			it('returns 400 and does not create the stratification if the field referencing the area (stratification.object) does not exist', (done) => {
				(axios.get as jest.Mock).mockImplementation(async (url) => {
					if (url.includes(FIELD_SURVEY_ID)) {
						return { data: [] };
					}
					if (url.includes(AREA_SURVEY_ID)) {
						return createMockSubmissionsResponse({ resourceType: ResourceTypes.Area });
					}
				});

				request(app)
					.post(`/stratifications?groupId=${mockGroupId}`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send(mockRequestBody)
					.end((err, res) => {
						expect(res.status).toBe(400);
						expect(axios.post).not.toHaveBeenCalled();
						done();
					});
			});

			it('returns 201 on success', (done) => {
				(axios.post as jest.Mock).mockResolvedValueOnce({ data: { insertedId: '456' } });
				(axios.get as jest.Mock).mockImplementation(async (url) => {
					if (url.includes(FIELD_SURVEY_ID)) {
						return createMockSubmissionsResponse({ resourceType: ResourceTypes.Field });
					}
					if (url.includes(AREA_SURVEY_ID)) {
						return createMockSubmissionsResponse({ resourceType: ResourceTypes.Area });
					}
				});

				request(app)
					.post(`/stratifications?groupId=${mockGroupId}`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send(mockRequestBody)
					.end(expect201AndValidateResponse(done));
			});

			it('returns 500 when surveystack returns an error', (done) => {
				(axios.post as jest.Mock).mockRejectedValueOnce('error');
				(axios.get as jest.Mock).mockRejectedValueOnce('error');

				request(app)
					.post(`/stratifications?groupId=${mockGroupId}`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send(mockRequestBody)
					.expect(500, done);
			});
		});
	});

	describe('GET', () => {
		describe('/stratifications', () => {
			it('GETs stratification submissions from surveystack', (done) => {
				request(app)
					.get('/stratifications')
					.set('X-Authorization', 'name@example.com abcd1234')
					.end(() => {
						expect(axios.get as jest.Mock).toHaveBeenCalledWith(
							`${SURVEYSTACK_API_URL}/submissions?survey=${STRATIFICATION_SURVEY_ID}`,
							{
								headers: surveyStackRequestHeaders,
							}
						);
						done();
					});
			});

			it("GETs field submissions from surveystack in order to find the fields that contain the areas referenced by the stratifications' object properties in order to populate the stratification's featureOfInterest in the response", (done) => {
				(axios.get as jest.Mock).mockImplementation((url) => {
					if (url.includes(STRATIFICATION_SURVEY_ID)) {
						return Promise.resolve(createMockSubmissionsResponse({ resourceType: ResourceTypes.Stratification }));
					}
					if (url.includes(FIELD_SURVEY_ID)) {
						return Promise.resolve(createMockSubmissionsResponse({ resourceType: ResourceTypes.Field }));
					}
				});

				request(app)
					.get('/stratifications')
					.set('X-Authorization', 'name@example.com abcd1234')
					.end((err, res) => {
						expect(axios.get as jest.Mock).toHaveBeenCalledWith(
							expect.stringContaining(`${SURVEYSTACK_API_URL}/submissions?survey=${FIELD_SURVEY_ID}`),
							{ headers: surveyStackRequestHeaders }
						);
						const responseStratification = JSON.parse(res.text)[0];
						expect(responseStratification).toHaveProperty('featureOfInterest', 'field-id');
						done();
					});
			});

			it('returns 200 on success when response is empty', (done) => {
				(axios.get as jest.Mock).mockResolvedValue({ data: [] });

				request(app)
					.get('/stratifications')
					.set('X-Authorization', 'name@example.com abcd1234')
					.send()
					.end(expect200AndValidateResponse(done));
			});

			it('returns 200 on success when resource is present', (done) => {
				(axios.get as jest.Mock).mockImplementation((url) => {
					if (url.includes(STRATIFICATION_SURVEY_ID)) {
						return Promise.resolve(createMockSubmissionsResponse({ resourceType: ResourceTypes.Stratification }));
					}
					if (url.includes(FIELD_SURVEY_ID)) {
						return Promise.resolve(createMockSubmissionsResponse({ resourceType: ResourceTypes.Field }));
					}
				});

				request(app)
					.get('/stratifications')
					.set('X-Authorization', 'name@example.com abcd1234')
					.send()
					.end(expect200AndValidateResponse(done));
			});

			it('returns stratifications that conform to the ResponseStratification schema when the response is not an empty array', (done) => {
				(axios.get as jest.Mock).mockImplementation((url) => {
					if (url.includes(STRATIFICATION_SURVEY_ID)) {
						return Promise.resolve(createMockSubmissionsResponse({ resourceType: ResourceTypes.Stratification }));
					}
					if (url.includes(FIELD_SURVEY_ID)) {
						return Promise.resolve(createMockSubmissionsResponse({ resourceType: ResourceTypes.Field }));
					}
				});

				request(app)
					.get('/stratifications')
					.set('X-Authorization', 'name@example.com abcd1234')
					.send()
					.end(expect200AndValidateResponse(done));
			});

			it('returns 500 when surveystack returns an error', (done) => {
				(axios.get as jest.Mock).mockRejectedValueOnce('error');

				request(app).get(`/stratifications`).set('X-Authorization', 'name@example.com abcd1234').expect(500, done);
			});

			it("returns 200 when valid ISO8601 timestamps are passed in the 'submittedAfter' query parameter", (done) => {
				(axios.get as jest.Mock).mockResolvedValue({ data: [] });

				request(app)
					.get('/stratifications')
					.set('X-Authorization', 'name@example.com abcd1234')
					.query({ submittedAfter: '2020-01-01T00:00:00.000Z' })
					.send()
					.end(expect200AndValidateResponse(done));
			});

			const invalidSubmittedAfterValues = [
				{ description: 'empty string', submittedAfter: '' },
				{ description: 'invalid date', submittedAfter: '2020-01-01T00:00:00.000' },
				{ description: 'ms since epoch', submittedAfter: '1577836800000' },
			];

			invalidSubmittedAfterValues.forEach(({ description, submittedAfter }) => {
				it(`returns 400 when ${description} is passed in the \'submittedAfter\' query paramter`, (done) => {
					(axios.get as jest.Mock).mockResolvedValue({ data: [] });

					request(app)
						.get('/stratifications')
						.set('X-Authorization', 'name@example.com abcd1234')
						.query({ submittedAfter })
						.send()
						.end(expect400AndValidateResponse(done));
				});
			});
		});

		describe('/stratifications/:id', () => {
			it(`returns 200 when a stratification is found`, (done) => {
				(axios.get as jest.Mock).mockImplementation((url) => {
					if (url.includes(STRATIFICATION_SURVEY_ID)) {
						return Promise.resolve(createMockSubmissionsResponse({ resourceType: ResourceTypes.Stratification }));
					}
					if (url.includes(FIELD_SURVEY_ID)) {
						return Promise.resolve(createMockSubmissionsResponse({ resourceType: ResourceTypes.Field }));
					}
				});

				request(app)
					.get(`/stratifications/123456789012345678901234`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send()
					.end(expect200AndValidateResponse(done));
			});

			it(`returns 404 when an stratification is not found`, (done) => {
				(axios.get as jest.Mock).mockResolvedValue({ data: [] });

				request(app)
					.get(`/stratifications/123456789012345678901234`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send()
					.end(expect404AndValidateResponse(done));
			});

			it(`returns 404 when the stratification is redacted`, (done) => {
				(axios.get as jest.Mock).mockImplementation((url) => {
					if (url.includes(STRATIFICATION_SURVEY_ID)) {
						return Promise.resolve({
							data: [createMockResponseSubmission({ resourceType: ResourceTypes.Stratification, redacted: true })],
						});
					}
					if (url.includes(FIELD_SURVEY_ID)) {
						return Promise.resolve({
							data: [createMockResponseSubmission({ resourceType: ResourceTypes.Field, redacted: true })],
						});
					}
				});

				request(app)
					.get(`/stratifications/123456789012345678901234`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send()
					.end(expect404AndValidateResponse(done));
			});

			it(`returns 400 if stratification id in path is not a valid mongo ObjectId (length of 24)`, (done) => {
				request(app)
					.get(`/stratifications/123`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send()
					.end((err, resp) => {
						expect(resp.status).toEqual(400);
						done();
					});
			});
		});
	});

	describe('DELETE', () => {
		describe('/stratifications/:id', () => {
			const mockStratification = createMockResponseSubmission({ resourceType: ResourceTypes.Stratification });
			const mockLocationCollection = createMockResponseSubmission({ resourceType: ResourceTypes.LocationCollection });

			it("POSTS the submissionIds (stratification, locationCollections, locations) to surveystack's submissions/bulk-archive endpoint", (done) => {
				(axios.get as jest.Mock).mockImplementation((url) => {
					if (url.includes(STRATIFICATION_SURVEY_ID)) {
						return Promise.resolve({
							data: [
								{
									...mockStratification,
									data: {
										...mockStratification.data,
										id: { value: '123456789012345678901234' },
									},
								},
							],
						});
					}
					if (url.includes(LOCATION_COLLECTION_SURVEY_ID)) {
						return Promise.resolve({
							data: [
								{
									...mockLocationCollection,
									data: {
										...mockLocationCollection.data,
										id: { value: 'location-collection-1' },
										features: { value: JSON.stringify(['location-1', 'location-2']) },
									},
								},
								{
									...mockLocationCollection,
									data: {
										...mockLocationCollection.data,
										id: { value: 'location-collection-2' },
										features: { value: JSON.stringify(['location-3']) },
									},
								},
							],
						});
					}
					if (url.includes(SAMPLING_COLLECTION_SURVEY_ID)) {
						return Promise.resolve({ data: [] });
					}
				});

				request(app)
					.delete('/stratifications/123456789012345678901234')
					.set('X-Authorization', 'name@example.com abcd1234')
					.set('X-Super-Authorization', 'password')
					.send()
					.end(() => {
						expect(axios.post as jest.Mock).toHaveBeenCalledWith(
							`${SURVEYSTACK_API_URL}/submissions/bulk-archive?set=true&reason=INCORRECT_DATA`,
							{
								ids: [
									'location-1',
									'location-2',
									'location-3',
									'location-collection-1',
									'location-collection-2',
									'123456789012345678901234',
								],
							},
							{ headers: surveyStackRequestHeaders }
						);
						done();
					});
			});

			it('returns 204 on success when the stratification exists and has no dependent resources', (done) => {
				(axios.get as jest.Mock).mockImplementation((url) => {
					if (url.includes(STRATIFICATION_SURVEY_ID)) {
						return Promise.resolve({ data: [mockStratification] });
					}
					if (url.includes(LOCATION_COLLECTION_SURVEY_ID)) {
						return Promise.resolve({ data: [] });
					}
				});

				request(app)
					.delete('/stratifications/123456789012345678901234')
					.set('X-Authorization', 'name@example.com abcd1234')
					.set('X-Super-Authorization', 'password')
					.send()
					.end(expect204AndValidateResponse(done));
			});

			it('returns 404 when no matching stratification exists', (done) => {
				(axios.get as jest.Mock).mockImplementation((url) => {
					if (url.includes(STRATIFICATION_SURVEY_ID)) {
						return Promise.resolve({ data: [] });
					}
				});

				request(app)
					.delete('/stratifications/123456789012345678901234')
					.set('X-Authorization', 'name@example.com abcd1234')
					.set('X-Super-Authorization', 'password')
					.send()
					.end(expect404AndValidateResponse(done));
			});

			it('returns 409 when any samplingCollection depends on the locationCollections that are related to the stratification', (done) => {
				(axios.get as jest.Mock).mockImplementation((url) => {
					if (url.includes(STRATIFICATION_SURVEY_ID)) {
						return Promise.resolve({ data: [mockStratification] });
					}
					if (url.includes(LOCATION_COLLECTION_SURVEY_ID)) {
						return Promise.resolve({
							data: [
								{
									...mockLocationCollection,
									data: {
										...mockLocationCollection.data,
										id: { value: 'location-collection-1' },
										features: { value: JSON.stringify(['location-1', 'location-2']) },
									},
								},
							],
						});
					}
					if (url.includes(SAMPLING_COLLECTION_SURVEY_ID)) {
						return Promise.resolve(createMockSubmissionsResponse({ resourceType: ResourceTypes.SamplingCollection }));
					}
				});

				request(app)
					.delete('/stratifications/123456789012345678901234')
					.set('X-Authorization', 'name@example.com abcd1234')
					.set('X-Super-Authorization', 'password')
					.send()
					.end(expect409AndValidateResponse(done));
			});

			it('returns 500 when surveystack returns an error', (done) => {
				(axios.get as jest.Mock).mockRejectedValueOnce('error');

				request(app)
					.delete('/stratifications/123456789012345678901234')
					.set('X-Authorization', 'name@example.com abcd1234')
					.set('X-Super-Authorization', 'password')
					.send()
					.expect(500, done);
			});
		});
	});
});

import ObjectId from 'bson-objectid';
import { NextFunction, Request, Response, Router } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import { ResourceTypes } from '../constants';
import { groupIdQueryParam } from '../open-api-spec/common/queryParameters';
import { createSample, createSampling, createSamplingCollection } from '../resources';
import {
	createMatchAfterDateSubmitted,
	createSubmissions,
	getSubmission,
	getSubmissions,
} from '../services/surveystack';
import { LocationCollection, Sample, Sampling, SamplingCollection } from '../types';
import createGetResourceByIdController from './common/createGetResourceByIdController';

const router = Router();

router.post('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const { samples, samplings }: { samples: Sample[]; samplings: Sampling[] } = req.body.members.reduce(
			(r: any, member: any) => {
				const samples: Sample[] = member.results.map((sample: Sample) =>
					createSample({
						id: new ObjectId().toHexString(),
						...sample,
					})
				);

				const sampling: Sampling = createSampling({
					id: new ObjectId().toHexString(),
					...member,
					results: samples.map(({ id }) => id),
				});

				return {
					samples: [...r.samples, ...samples.map((sample) => ({ ...sample, resultOf: sampling.id }))],
					samplings: [...r.samplings, sampling],
				};
			},
			{ samples: [], samplings: [] }
		);

		const samplingCollection = createSamplingCollection({
			id: new ObjectId().toHexString(),
			...req.body,
			members: samplings.map(({ id }) => id),
		});

		const locationCollection = await getSubmission<LocationCollection>({
			resourceType: ResourceTypes.LocationCollection,
			authorizationHeaderValue: req.get('X-Authorization') ?? '',
			id: samplingCollection.object,
		});

		if (!locationCollection) {
			res.status(400).json({ message: `Location Collection ${samplingCollection.object} not found.` });
			return;
		}

		await createSubmissions({
			resources: [
				{
					resource: samplingCollection,
					resourceType: ResourceTypes.SamplingCollection,
				},
				...samplings.map((sampling) => ({
					resource: { ...sampling, memberOf: samplingCollection.id } as Sampling,
					resourceType: ResourceTypes.Sampling,
				})),
				...samples.map((sample) => ({
					resource: sample,
					resourceType: ResourceTypes.Sample,
				})),
			],
			groupId: String(req.query.groupId),
			authorizationHeaderValue: req.get('X-Authorization') ?? '',
		});

		res.status(201).json({ samplingCollections: [samplingCollection], samplings, samples });
	} catch (error) {
		next(error);
	}
});

router.get('/', async (req: Request, res: Response, next: NextFunction) => {
	const submittedAfter = req.query.submittedAfter;
	try {
		const samplingCollections = await getSubmissions<SamplingCollection>({
			resourceType: ResourceTypes.SamplingCollection,
			authorizationHeaderValue: req.get('X-Authorization') ?? '',
			...(submittedAfter !== undefined
				? { matchExpression: createMatchAfterDateSubmitted(new Date(submittedAfter as string)) }
				: {}),
		});
		res.status(200).json(samplingCollections);
	} catch (error) {
		next(error);
	}
});

router.get('/:id', createGetResourceByIdController(ResourceTypes.SamplingCollection));

const openApiPaths: OpenAPIV3.PathsObject = {
	'/sampling-collections': {
		post: {
			tags: ['Sampling Collections'],
			summary: 'Saves a sampling collection.',
			parameters: [groupIdQueryParam],
			security: [{ Authorization: [] }],
			requestBody: {
				description: 'The sampling collections object to be saved.',
				content: {
					'application/json': {
						schema: {
							$ref: '#/components/schemas/PopulatedSamplingCollection',
						},
					},
				},
				required: true,
			},
			responses: {
				'201': {
					description: 'Successfully saved a sampling collection.',
					content: {
						'application/json': {
							schema: {
								type: 'object',
								required: ['samplingCollections', 'samplings', 'samples'],
								properties: {
									samplingCollections: {
										type: 'array',
										items: { $ref: '#/components/schemas/ResponseSamplingCollection' },
									},
									samplings: { type: 'array', items: { $ref: '#/components/schemas/ResponseSampling' } },
									samples: { type: 'array', items: { $ref: '#/components/schemas/ResponseSample' } },
								},
							},
						},
					},
				},
				'400': {
					description: 'Bad request.',
					content: {
						'application/json': {
							schema: {
								type: 'object',
								required: ['message'],
								properties: {
									message: { type: 'string' },
								},
							},
						},
					},
				},
			},
		},
		get: {
			tags: ['Sampling Collections'],
			summary: 'Get all sampling collections.',
			security: [{ Authorization: [] }],
			parameters: [
				{
					name: 'submittedAfter',
					in: 'query',
					description: 'Only return sampling collections submitted after this ISO8601 timestamp.',
					required: false,
					schema: {
						type: 'string',
						format: 'date-time',
					},
				},
			],
			responses: {
				'200': {
					description: 'An array of the sampling collections you have access to.',
					content: {
						'application/json': {
							schema: {
								type: 'array',
								items: { $ref: '#/components/schemas/ResponseSamplingCollection' },
							},
						},
					},
				},
			},
		},
	},
	'/sampling-collections/{samplingCollectionId}': {
		get: {
			tags: ['Sampling Collections'],
			summary: 'Get a sampling collection by its id.',
			security: [{ Authorization: [] }],
			parameters: [
				{
					name: 'samplingCollectionId',
					in: 'path',
					description: 'The id of the sampling collection to get.',
					required: true,
					schema: { type: 'string', minLength: 24, maxLength: 24 },
				},
			],
			responses: {
				'200': {
					description: 'The sampling collection with the given id.',
					content: {
						'application/json': {
							schema: {
								$ref: '#/components/schemas/ResponseSamplingCollection',
							},
						},
					},
				},
				'404': {
					description: 'No sampling collection with the given id exists.',
				},
			},
		},
	},
};

export { openApiPaths, router };

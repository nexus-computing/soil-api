import axios from 'axios';
import request from 'supertest';
import app from '../app';
import { LOCATION_COLLECTION_SURVEY_ID, ResourceTypes, SURVEYSTACK_API_URL } from '../constants';
import {
	createMockSubmissionsResponse,
	expect200AndValidateResponse,
	testGetResourceByIdEndpoint,
} from '../test-utils';

jest.mock('axios');

describe('Location Collection Endpoints', () => {
	describe('GET', () => {
		describe('/location-collections', () => {
			it('GETs location collection submissions from surveystack', (done) => {
				const surveyStackRequestHeaders = { Authorization: 'name@example.com abcd1234' };

				request(app)
					.get('/location-collections')
					.set('X-Authorization', 'name@example.com abcd1234')
					.end(() => {
						expect(axios.get as jest.Mock).toHaveBeenCalledWith(
							`${SURVEYSTACK_API_URL}/submissions?survey=${LOCATION_COLLECTION_SURVEY_ID}`,
							{
								headers: surveyStackRequestHeaders,
							}
						);
						done();
					});
			});

			it('returns 200 on success when response is empty', (done) => {
				(axios.get as jest.Mock).mockResolvedValue({ data: [] });

				request(app)
					.get('/location-collections')
					.set('X-Authorization', 'name@example.com abcd1234')
					.send()
					.end(expect200AndValidateResponse(done));
			});

			it('returns 200 on success when resource is present', (done) => {
				(axios.get as jest.Mock).mockResolvedValue(
					createMockSubmissionsResponse({ resourceType: ResourceTypes.LocationCollection })
				);

				request(app)
					.get('/location-collections')
					.set('X-Authorization', 'name@example.com abcd1234')
					.send()
					.end(expect200AndValidateResponse(done));
			});

			it('returns 500 when surveystack returns an error', (done) => {
				(axios.get as jest.Mock).mockRejectedValueOnce('error');

				request(app).get(`/location-collections`).set('X-Authorization', 'name@example.com abcd1234').expect(500, done);
			});
		});

		testGetResourceByIdEndpoint({
			endpointPath: '/location-collections',
			resourceName: 'location collection',
			resourceType: ResourceTypes.LocationCollection,
		});
	});
});

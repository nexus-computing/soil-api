import { NextFunction, Request, Response, Router } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import { ResourceTypes } from '../constants';
import { getSubmissions } from '../services/surveystack';
import { LocationCollection } from '../types';
import createGetResourceByIdController from './common/createGetResourceByIdController';

const router = Router();

router.get('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const locationCollections = await getSubmissions<LocationCollection>({
			resourceType: ResourceTypes.LocationCollection,
			authorizationHeaderValue: req.get('X-Authorization') ?? '',
		});
		res.status(200).json(locationCollections);
	} catch (error) {
		next(error);
	}
});

router.get('/:id', createGetResourceByIdController(ResourceTypes.LocationCollection));

const openApiPaths: OpenAPIV3.PathsObject = {
	'/location-collections': {
		get: {
			tags: ['Location Collections'],
			summary: 'Get all location collections.',
			security: [{ Authorization: [] }],
			responses: {
				'200': {
					description: 'An array of the location collections you have access to.',
					content: {
						'application/json': {
							schema: {
								type: 'array',
								items: { $ref: '#/components/schemas/ResponseLocationCollection' },
							},
						},
					},
				},
			},
		},
	},
	'/location-collections/{locationCollectionId}': {
		get: {
			tags: ['Location Collections'],
			summary: 'Get a location collection by its id.',
			security: [{ Authorization: [] }],
			parameters: [
				{
					name: 'locationCollectionId',
					in: 'path',
					description: 'The id of the location collection to get.',
					required: true,
					schema: { type: 'string', minLength: 24, maxLength: 24 },
				},
			],
			responses: {
				'200': {
					description: 'The location collection with the given id.',
					content: {
						'application/json': {
							schema: {
								$ref: '#/components/schemas/ResponseLocationCollection',
							},
						},
					},
				},
				'404': {
					description: 'No location collection with the given id exists.',
				},
			},
		},
	},
};

export { openApiPaths, router };

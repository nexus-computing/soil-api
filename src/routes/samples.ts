import { NextFunction, Request, Response, Router } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import { ResourceTypes } from '../constants';
import { getSubmission, getSubmissions, updateSubmission } from '../services/surveystack';
import { Sample } from '../types';
import createGetResourceByIdController from './common/createGetResourceByIdController';

const router = Router();

router.get('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const samples = await getSubmissions<Sample>({
			resourceType: ResourceTypes.Sample,
			authorizationHeaderValue: req.get('X-Authorization') ?? '',
		});
		res.status(200).json(samples);
	} catch (error) {
		next(error);
	}
});

router.get('/:id', createGetResourceByIdController(ResourceTypes.Sample));

router.patch('/:id', async (req: Request, res: Response) => {
	try {
		const id = req.params.id;
		const authorizationHeaderValue = req.get('X-Authorization') ?? '';

		const sample = await getSubmission<Sample>({
			resourceType: ResourceTypes.Sample,
			authorizationHeaderValue,
			id,
		});

		// Validate if sample is exist
		if (!sample) {
			return res.status(404).send();
		}

		// Patch
		const updatedSample: Sample = { ...sample, ...req.body };
		await updateSubmission({
			resourceType: ResourceTypes.Sample,
			resource: updatedSample,
			groupId: updatedSample.meta?.groupId ?? '',
			authorizationHeaderValue,
		});

		return res.status(204).send();
	} catch (error) {
		return res.status(500).send();
	}
});

const openApiPaths: OpenAPIV3.PathsObject = {
	'/samples': {
		get: {
			tags: ['Samples'],
			summary: 'Get all samples.',
			security: [{ Authorization: [] }],
			responses: {
				'200': {
					description: 'An array of the samples you have access to.',
					content: {
						'application/json': {
							schema: {
								type: 'array',
								items: { $ref: '#/components/schemas/ResponseSample' },
							},
						},
					},
				},
			},
		},
	},
	'/samples/{sampleId}': {
		get: {
			tags: ['Samples'],
			summary: 'Get a sample by its id.',
			security: [{ Authorization: [] }],
			parameters: [
				{
					name: 'sampleId',
					in: 'path',
					description: 'The id of the sample to get.',
					required: true,
					schema: { type: 'string', minLength: 24, maxLength: 24 },
				},
			],
			responses: {
				'200': {
					description: 'The sample with the given id.',
					content: {
						'application/json': {
							schema: {
								$ref: '#/components/schemas/ResponseSample',
							},
						},
					},
				},
				'404': {
					description: 'No sample with the given id exists.',
				},
			},
		},

		patch: {
			tags: ['Samples'],
			summary: 'Updates a sample.',
			security: [{ Authorization: [] }],
			parameters: [
				{
					name: 'sampleId',
					in: 'path',
					description: 'The id of the sample to edit.',
					required: true,
					schema: { type: 'string', minLength: 24, maxLength: 24 },
				},
			],
			requestBody: {
				description: 'The sample object to be saved.',
				required: true,
				content: {
					'application/json': {
						schema: {
							$ref: '#/components/schemas/EditableSample',
						},
					},
				},
			},
			responses: {
				'204': {
					description: 'The sample was saved successfully.',
				},
				'404': {
					description: 'No sample with the given id exists.',
				},
			},
		},
	},
};

export { openApiPaths, router };

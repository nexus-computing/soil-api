import axios from 'axios';
import request from 'supertest';
import app from '../app';
import {
	AREA_SURVEY_ID,
	AREA_SURVEY_VERSION,
	ResourceTypes,
	STRATIFICATION_SURVEY_ID,
	SURVEYSTACK_API_URL,
} from '../constants';
import {
	createMockArea,
	createMockPolygon,
	createMockSubmissionsResponse,
	expect200AndValidateResponse,
	expect204AndValidateResponse,
	expect404AndValidateResponse,
	expect409AndValidateResponse,
	testGetResourceByIdEndpoint,
} from '../test-utils';
import { Area } from '../types';

jest.mock('axios');

describe('Area Endpoints', () => {
	const surveyStackRequestHeaders = { Authorization: 'name@example.com abcd1234' };

	describe('GET', () => {
		describe('/areas', () => {
			it('GETs area submissions from surveystack', (done) => {
				request(app)
					.get('/areas')
					.set('X-Authorization', 'name@example.com abcd1234')
					.end(() => {
						expect(axios.get as jest.Mock).toHaveBeenCalledWith(
							`${SURVEYSTACK_API_URL}/submissions?survey=${AREA_SURVEY_ID}`,
							{
								headers: surveyStackRequestHeaders,
							}
						);
						done();
					});
			});

			it('returns 200 on success when no resources are present', (done) => {
				(axios.get as jest.Mock).mockResolvedValue({ data: [] });

				request(app)
					.get(`/areas`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send()
					.end(expect200AndValidateResponse(done));
			});

			it('returns 200 on success when resources are present', (done) => {
				(axios.get as jest.Mock).mockResolvedValueOnce(
					createMockSubmissionsResponse({ resourceType: ResourceTypes.Area })
				);

				request(app)
					.get(`/areas`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send()
					.end(expect200AndValidateResponse(done));
			});

			it('returns 500 when surveystack returns an error', (done) => {
				(axios.get as jest.Mock).mockRejectedValueOnce('error');

				request(app).get(`/areas`).set('X-Authorization', 'name@example.com abcd1234').expect(500, done);
			});
		});

		testGetResourceByIdEndpoint({
			endpointPath: '/areas',
			resourceName: 'area',
			resourceType: ResourceTypes.Area,
		});
	});

	describe('PATCH', () => {
		describe('/areas/:id', () => {
			const areaId = '123456789012345678901234';
			const mockArea = createMockArea({ id: areaId });
			const { type, geometry, properties } = mockArea;
			const mockEditableArea: Partial<Area> = {
				type,
				geometry,
				properties,
			};

			it(`returns 204 when an area is saved`, (done) => {
				(axios.get as jest.Mock).mockImplementation((url) => {
					if (url.includes(AREA_SURVEY_ID)) {
						return Promise.resolve(createMockSubmissionsResponse({ resourceType: ResourceTypes.Area }));
					}
					if (url.includes(STRATIFICATION_SURVEY_ID)) {
						return Promise.resolve({ data: [] });
					}
				});

				request(app)
					.patch(`/areas/${areaId}`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send(mockEditableArea)
					.end(expect204AndValidateResponse(done));
			});

			it(`saves 'type', 'geometry', 'properties' only`, (done) => {
				(axios.get as jest.Mock).mockImplementation((url) => {
					if (url.includes(AREA_SURVEY_ID)) {
						return Promise.resolve(createMockSubmissionsResponse({ resourceType: ResourceTypes.Area }));
					}
					if (url.includes(STRATIFICATION_SURVEY_ID)) {
						return Promise.resolve({ data: [] });
					}
				});

				Date.now = jest.fn(() => 0);

				const updatedArea: Partial<Area> = {
					type: 'Feature',
					geometry: createMockPolygon({
						coordinates: [
							[
								[0, 0],
								[1, 1],
							],
						],
					}),
					properties: {
						featureOfInterest: 'modifed feature of interest',
					},
				};

				const surveyStackRequestBody = expect.objectContaining({
					_id: 'area-id',
					meta: expect.objectContaining({
						survey: {
							id: AREA_SURVEY_ID,
							version: AREA_SURVEY_VERSION,
						},
						group: {
							id: '',
							path: null,
						},
						specVersion: 3,
						dateModified: new Date(0),
						archivedReason: 'RESUBMIT',
					}),
					data: expect.objectContaining({
						id: {
							value: 'area-id',
							meta: expect.anything(),
						},
						geojson: {
							value: {
								type: 'FeatureCollection',
								features: [
									{
										type: updatedArea.type,
										geometry: updatedArea.geometry,
										properties: updatedArea.properties,
									},
								],
							},
							meta: expect.anything(),
						},
					}),
				});

				request(app)
					.patch(`/areas/${areaId}`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send(updatedArea)
					.end(() => {
						expect(axios.put as jest.Mock).toHaveBeenCalledWith(
							`${SURVEYSTACK_API_URL}/submissions/area-id`,
							surveyStackRequestBody,
							{ headers: surveyStackRequestHeaders }
						);
						done();
					});
			});

			it(`returns 404 when an area is not found`, (done) => {
				(axios.get as jest.Mock).mockImplementation((url) => {
					if (url.includes(AREA_SURVEY_ID)) {
						return Promise.resolve({ data: [] });
					}
					if (url.includes(STRATIFICATION_SURVEY_ID)) {
						return Promise.resolve({ data: [] });
					}
				});

				request(app)
					.patch(`/areas/${areaId}`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send(mockEditableArea)
					.end(expect404AndValidateResponse(done));
			});

			it(`returns 409 when there're more than one stratification that depend on it`, (done) => {
				(axios.get as jest.Mock).mockImplementation((url) => {
					if (url.includes(AREA_SURVEY_ID)) {
						return Promise.resolve(createMockSubmissionsResponse({ resourceType: ResourceTypes.Area }));
					}
					if (url.includes(STRATIFICATION_SURVEY_ID)) {
						return Promise.resolve(createMockSubmissionsResponse({ resourceType: ResourceTypes.Stratification }));
					}
				});

				request(app)
					.patch(`/areas/${areaId}`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send(mockEditableArea)
					.end(expect409AndValidateResponse(done));
			});

			it("returns 500 when surveystack's get submission endpoint returns an error", (done) => {
				(axios.get as jest.Mock).mockRejectedValue('error');

				request(app)
					.patch(`/areas/${areaId}`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send(mockEditableArea)
					.expect(500, done);
			});

			it("returns 500 when surveystack's resubmit endpoint returns an error", (done) => {
				(axios.get as jest.Mock).mockImplementation((url) => {
					if (url.includes(AREA_SURVEY_ID)) {
						return Promise.resolve(createMockSubmissionsResponse({ resourceType: ResourceTypes.Area }));
					}
					if (url.includes(STRATIFICATION_SURVEY_ID)) {
						return Promise.resolve({ data: [] });
					}
				});
				(axios.put as jest.Mock).mockRejectedValueOnce('error');

				request(app)
					.patch(`/areas/${areaId}`)
					.set('X-Authorization', 'name@example.com abcd1234')
					.send(mockEditableArea)
					.expect(500, done);
			});
		});
	});
});

import { NextFunction, Request, Response, Router } from 'express';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import { ResourceTypes } from '../constants';
import { getSubmissions } from '../services/surveystack';
import { Location } from '../types';
import createGetResourceByIdController from './common/createGetResourceByIdController';

const router = Router();

router.get('/', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const locations = await getSubmissions<Location>({
			resourceType: ResourceTypes.Location,
			authorizationHeaderValue: req.get('X-Authorization') ?? '',
		});
		res.status(200).json(locations);
	} catch (error) {
		next(error);
	}
});

router.get('/:id', createGetResourceByIdController(ResourceTypes.Location));

const openApiPaths: OpenAPIV3.PathsObject = {
	'/locations': {
		get: {
			tags: ['Locations'],
			summary: 'Get all locations.',
			security: [{ Authorization: [] }],
			responses: {
				'200': {
					description: 'An array of the locations you have access to.',
					content: {
						'application/json': {
							schema: {
								type: 'array',
								items: { $ref: '#/components/schemas/ResponseLocation' },
							},
						},
					},
				},
			},
		},
	},
	'/locations/{locationId}': {
		get: {
			tags: ['Locations'],
			summary: 'Get a location by its id.',
			security: [{ Authorization: [] }],
			parameters: [
				{
					name: 'locationId',
					in: 'path',
					description: 'The id of the location to get.',
					required: true,
					schema: { type: 'string', minLength: 24, maxLength: 24 },
				},
			],
			responses: {
				'200': {
					description: 'The location with the given id.',
					content: {
						'application/json': {
							schema: {
								$ref: '#/components/schemas/ResponseLocation',
							},
						},
					},
				},
				'404': {
					description: 'No location with the given id exists.',
				},
			},
		},
	},
};

export { openApiPaths, router };

/*
	NOTE: Environment variables are not configured in the same way for local development and actual deployments.
	
	For local devleopment, dotenv.config() will pull in environment variables from a .env file in the root of
	the project if one is present. If environment variables specified in the .env file are already set on the machine,
	dotenv.config() will NOT OVERRIDE the variables set on the machine with the variables in the .env file.
	
	For deployed environments, environment variables will NOT come directly from a .env file. They will be present on
	the machine and configured at the time of deployment by ci/cd scripts. This means dotenv.config() will have no
	effect for deployed environments.
*/
import dotenv from 'dotenv';
dotenv.config({
	path: process.env.NODE_ENV === 'test' ? '.env.example' : '.env',
});

const expectedEnvironmentVariables = [
	'surveystackApiUrl',
	'sampleSurveyId',
	'sampleSurveyVersion',
	'samplingSurveyId',
	'samplingSurveyVersion',
	'samplingCollectionSurveyId',
	'samplingCollectionSurveyVersion',
	'areaSurveyId',
	'areaSurveyVersion',
	'fieldSurveyId',
	'fieldSurveyVersion',
	'locationSurveyId',
	'locationSurveyVersion',
	'locationCollectionSurveyId',
	'locationCollectionSurveyVersion',
	'stratificationSurveyId',
	'stratificationSurveyVersion',
	'soilstackBaseGroupPath',
	'elasticEnableLogging',
	'elasticIndexName',
	'elasticCloudId',
	'elasticAuthUsername',
	'elasticAuthPassword',
	'superAdminPassword',
	'internalApiDocsUrl',
];

expectedEnvironmentVariables.forEach((envVar) => {
	if (process.env[envVar] === '' || process.env[envVar] === undefined) {
		throw new Error(
			`Required environment variable ${envVar} is not defined. Make sure it is defined in either a .env file or as an environment variable.`
		);
	}
});

export const FIELD_SURVEY_ID: string = process.env.fieldSurveyId as string;
export const FIELD_SURVEY_VERSION: number = parseInt(process.env.fieldSurveyVersion as string, 10);

export const AREA_SURVEY_ID: string = process.env.areaSurveyId as string;
export const AREA_SURVEY_VERSION: number = parseInt(process.env.areaSurveyVersion as string, 10);

export const STRATIFICATION_SURVEY_ID: string = process.env.stratificationSurveyId as string;
export const STRATIFICATION_SURVEY_VERSION: number = parseInt(process.env.stratificationSurveyVersion as string, 10);

export const LOCATION_COLLECTION_SURVEY_ID: string = process.env.locationCollectionSurveyId as string;
export const LOCATION_COLLECTION_SURVEY_VERSION: number = parseInt(
	process.env.locationCollectionSurveyVersion as string,
	10
);

export const LOCATION_SURVEY_ID: string = process.env.locationSurveyId as string;
export const LOCATION_SURVEY_VERSION: number = parseInt(process.env.locationSurveyVersion as string, 10);

export const SAMPLING_COLLECTION_SURVEY_ID: string = process.env.samplingCollectionSurveyId as string;
export const SAMPLING_COLLECTION_SURVEY_VERSION: number = parseInt(
	process.env.samplingCollectionSurveyVersion as string,
	10
);

export const SAMPLING_SURVEY_ID: string = process.env.samplingSurveyId as string;
export const SAMPLING_SURVEY_VERSION: number = parseInt(process.env.samplingSurveyVersion as string, 10);

export const SAMPLE_SURVEY_ID: string = process.env.sampleSurveyId as string;
export const SAMPLE_SURVEY_VERSION: number = parseInt(process.env.sampleSurveyVersion as string, 10);

export const SURVEYSTACK_API_URL: string = process.env.surveystackApiUrl as string;

export const SOILSTACK_BASE_GROUP_PATH: string = process.env.soilstackBaseGroupPath as string;

export const ELASTIC_ENABLE_LOGGING: boolean = process.env.elasticEnableLogging === 'true';
export const ELASTIC_INDEX_NAME: string = process.env.elasticIndexName as string;
export const ELASTIC_CLOUD_ID: string = process.env.elasticCloudId as string;
export const ELASTIC_AUTH_USERNAME: string = process.env.elasticAuthUsername as string;
export const ELASTIC_AUTH_PASSWORD: string = process.env.elasticAuthPassword as string;

export const SUPER_ADMIN_PASSWORD: string = process.env.superAdminPassword as string;

export const INTERNAL_API_DOCS_URL: string = process.env.internalApiDocsUrl as string;

export enum ResourceTypes {
	Area = 'Area',
	Field = 'Field',
	Stratification = 'Stratification',
	LocationCollection = 'LocationCollection',
	Location = 'Location',
	Sample = 'Sample',
	Sampling = 'Sampling',
	SamplingCollection = 'SamplingCollection',
}

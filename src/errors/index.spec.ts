import request from 'supertest';
import app from '../app';

describe('errorHandler', () => {
	it('returns open api schema validation errors as json with "message" and "errors" properties', (done) => {
		request(app)
			.post('/fields')
			.set('X-Authorization', 'name@example.com abcd1234')
			.send({})
			.end((err, res) => {
				const errorObject = JSON.parse(res.text);
				expect(errorObject).toHaveProperty('errors');
				expect(errorObject).toHaveProperty('message');
				done();
			});
	});

	it('returns 400 when requests violate open api schema validation', (done) => {
		request(app).post('/fields').set('X-Authorization', 'name@example.com abcd1234').send({}).expect(400, done);
	});

	it('returns 401 when X-Authorization header is not included', (done) => {
		request(app).post('/fields').send({}).expect(401, done);
	});

	it('returns 401 when X-Super-Authorization header is not included', (done) => {
		request(app)
			.delete('/fields/123456789012345678901234')
			.set('X-Authorization', 'name@example.com abcd1234')
			.send()
			.expect(401, done);
	});

	it('returns 401 when X-Super-Authorization header is invalid', (done) => {
		request(app)
			.delete('/fields/123456789012345678901234')
			.set('X-Authorization', 'name@example.com abcd1234')
			.set('X-Super-Authorization', 'wrong password')
			.send()
			.expect(401, done);
	});
});

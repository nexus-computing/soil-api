import { NextFunction, Request, Response } from 'express';

// eslint-disable-next-line unused-imports/no-unused-vars
const errorHandler = (err: any, req: Request, res: Response, next: NextFunction): any => {
	const isOpenApiError = Boolean(err?.status);

	if (isOpenApiError) {
		res.status(err.status).json({
			message: err.message,
			errors: err.errors,
		});
	} else {
		if (process.env.NODE_ENV === 'development') {
			console.log('Unhandled Error:', err);
		}
		res.status(500).send();
	}
};

export { errorHandler };

import app from './app';

app.listen(3000, () => {
	console.log('Soil API listening at http://localhost:3000');
});

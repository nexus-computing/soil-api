// eslint-disable-next-line @typescript-eslint/no-var-requires
const { toMatchOneOf, toMatchShapeOf } = require('jest-to-match-shape-of');

expect.extend({
	toMatchOneOf,
	toMatchShapeOf,
});

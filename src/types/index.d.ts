import { Feature, Point, Polygon } from 'geojson';

export type AnyObject = {
	[key: string]: any;
};

export interface Algorithm {
	name?: string;
	alternateName?: string;
	codeRepository?: string;
	version?: string;
	doi?: string;
}

export interface StratificationInput {
	name?: string;
	value: string | number | Array<any>;
}

export interface StratificationResult {
	name?: string;
	value: string | number | Array<any>;
}

export interface Stratification {
	id?: string;
	name: string;
	agent: string;
	dateCreated: string;
	provider: string;
	object: string; // area id
	algorithm: Algorithm;
	input: StratificationInput[];
	result: StratificationResult[];
	meta?: ResourceMeta;
}

export interface LocationCollection {
	id?: string;
	object: string;
	resultOf: string;
	featureOfInterest: string;
	features: string[];
	meta?: ResourceMeta;
}

export interface Location extends Feature<Point, any> {
	meta?: ResourceMeta;
}

type ReferenceIdOwners =
	| 'heartland'
	| 'soilstack-draft'
	| 'carbofarm'
	| 'flurosense-field'
	| 'flurosense-producer'
	| 'flurosense-program'
	| 'flurosense-year';
interface ResourceMeta {
	referenceIds?: {
		owner: ReferenceIdOwners;
		id: string;
	}[];
	groupId?: string;
	submissionId?: string;
	submittedAt?: string;
}

interface Address {
	streetAddress: string;
	addressLocality: string;
	addressRegion: string;
	addressCountry: string;
	postalCode: string;
}

interface ContactPoint {
	telephone: string;
	email: string;
	name: string;
	contactType: string;
	organization: string;
}

export interface Field {
	id?: string;
	name: string;
	alternateName: string;
	producerName: string;
	address: Address;
	contactPoints: Array<ContactPoint>;
	areas: string[];
	meta: ResourceMeta;
}

interface AreaProperties {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	[key: string]: any;
	name?: string;
	description?: string;
	featureOfInterest?: string;
}

export interface Area extends Feature<Polygon, AreaProperties> {
	meta?: ResourceMeta;
}

export interface SoDepthValue {
	value: number;
	unit: string;
}

export interface SoDepth {
	minValue: SoDepthValue;
	maxValue: SoDepthValue;
}

export interface Sample {
	id?: string;
	name: string;
	sampleOf: string; // field id
	results: string[]; // lab results
	resultOf: string; // sampling id
	soDepth: SoDepth;
	meta?: ResourceMeta;
}

export interface Sampling {
	id?: string;
	name: string;
	resultTime: string;
	geometry: Point;
	featureOfInterest: string; // location id
	memberOf: string; // sampling collection id
	results: string[]; // sample ids
	comment: string;
	procedures: string[];
	properties: {
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		[key: string]: any;
	};
	soDepth: SoDepth;
	meta?: ResourceMeta;
}

export interface SamplingCollection {
	id?: string;
	name: string;
	comment?: string;
	creator?: string; // user that created
	featureOfInterest: string; // field id
	participants: string[]; // users that have modified
	object: string; // location collection id
	members: string[]; // sampling ids
	soDepth: SoDepth;
	sampleDepths: SoDepth[];
	dateCreated?: string;
	dateModified?: string;
	meta: ResourceMeta;
}

interface SurveyInfo {
	id: string;
	version: number;
}

interface GroupInfo {
	id: string;
	path: string | null;
}

export interface SubmissionMeta {
	dateModified: Date | string;
	type: string;
	permissions: string[];
}

export interface SubmissionData {
	[key: string]: {
		meta: SubmissionMeta;
		[key: string]: any;
	};
}

export interface SubmissionInfo {
	id: string;
	survey: SurveyInfo;
	group: GroupInfo;
	data: SubmissionData;
}

export interface SurveyStackSubmissionRequestBody {
	_id: string;
	meta: {
		survey: SurveyInfo;
		group: GroupInfo;
		revision?: number;
		specVersion: number;
		dateCreated?: Date;
		dateModified: Date;
		archivedReason?: string;
	};
	data: SubmissionData;
}

export type SurveyStackSubmissionResponseBody = {
	_id: string;
	meta: {
		survey: SurveyInfo;
		group: GroupInfo;
		revision: number;
		specVersion: number;
		dateCreated: string;
		dateModified: string;
		dateSubmitted: string;
		creator: string;
	};
	data: SubmissionData;
};

export interface SurveyStackGroup {
	_id: string;
	meta?: Record<string, unknown>;
	name: string;
	slug: string;
	dir: string;
	path: string;
	surveys?: Record<string, unknown>;
}

export interface SurveyStackMembership {
	_id: string;
	group: SurveyStackGroup;
	roles: string[];
}

export interface Group {
	id: string;
	name: string;
	path: string;
}

export type Resource =
	| Field
	| Area
	| Stratification
	| LocationCollection
	| Location
	| Sample
	| Sampling
	| SamplingCollection;

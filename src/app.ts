import cors from 'cors';
import express from 'express';
import { errorHandler } from './errors';
import { initLogging } from './logging';
import { superAdminMiddleware } from './middlewares';
import setupOpenApiSpec from './open-api-spec';
import { routes } from './routes';

const app = express();

app.use(initLogging);

app.use(cors());

// set up body parsers prior to registering endpoints
app.use(express.json());

// open api
setupOpenApiSpec(app);

app.use(superAdminMiddleware);

// register endpoints
Object.entries(routes).forEach(([path, router]) => {
	app.use(path, router);
});

// register error handler last
app.use(errorHandler);

export default app;

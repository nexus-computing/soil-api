import axios from 'axios';
import {
	AREA_SURVEY_ID,
	FIELD_SURVEY_ID,
	LOCATION_COLLECTION_SURVEY_ID,
	LOCATION_SURVEY_ID,
	ResourceTypes,
	SAMPLE_SURVEY_ID,
	SAMPLING_COLLECTION_SURVEY_ID,
	SAMPLING_SURVEY_ID,
	SOILSTACK_BASE_GROUP_PATH,
	STRATIFICATION_SURVEY_ID,
} from '../../constants';
import { createArea, createField, createGroup } from '../../resources';
import {
	createMockArea,
	createMockField,
	createMockGroupsResponse,
	createMockMembershipsTreeResponse,
	createMockResponseSubmission,
	createMockSubmissionsResponse,
} from '../../test-utils';
import { Area, Field, Group } from '../../types';
import {
	bulkArchive,
	bulkReassign,
	createSubmission,
	createSubmissions,
	getAllResources,
	getGroups,
	getSubmission,
	getSubmissions,
	updateSubmission,
} from './index';

jest.mock('axios');

describe('createSubmission', () => {
	it('includes Authorization header', async () => {
		(axios.post as jest.Mock).mockResolvedValueOnce({});

		await createSubmission({
			resource: createField(),
			resourceType: ResourceTypes.Field,
			groupId: 'groupId',
			authorizationHeaderValue: 'test',
		});

		expect((axios.post as jest.Mock).mock.calls[0][2].headers).toEqual(
			expect.objectContaining({
				Authorization: 'test',
			})
		);
	});

	it('includes survey id matching resource type in request body', async () => {
		(axios.post as jest.Mock).mockResolvedValue({});

		await createSubmission({
			resource: createField(),
			resourceType: ResourceTypes.Field,
			groupId: 'groupId',
			authorizationHeaderValue: 'test',
		});

		await createSubmission({
			resource: createArea(),
			resourceType: ResourceTypes.Area,
			groupId: 'groupId',
			authorizationHeaderValue: 'test',
		});

		expect((axios.post as jest.Mock).mock.calls[0][1].meta.survey.id).toEqual(FIELD_SURVEY_ID);
		expect((axios.post as jest.Mock).mock.calls[1][1].meta.survey.id).toEqual(AREA_SURVEY_ID);
	});
});

describe('createSubmissions', () => {
	it('includes Authorization header', async () => {
		(axios.post as jest.Mock).mockResolvedValueOnce({});

		await createSubmissions({
			resources: [{ resource: createField(), resourceType: ResourceTypes.Field }],
			groupId: 'groupId',
			authorizationHeaderValue: 'test',
		});

		expect((axios.post as jest.Mock).mock.calls[0][2].headers).toEqual(
			expect.objectContaining({
				Authorization: 'test',
			})
		);
	});

	it('includes survey ids matching resource types in request body', async () => {
		(axios.post as jest.Mock).mockResolvedValue({});

		await createSubmissions({
			resources: [
				{ resource: createField(), resourceType: ResourceTypes.Field },
				{ resource: createArea(), resourceType: ResourceTypes.Area },
			],
			groupId: 'groupId',
			authorizationHeaderValue: 'test',
		});

		expect((axios.post as jest.Mock).mock.calls[0][1][0].meta.survey.id).toEqual(FIELD_SURVEY_ID);
		expect((axios.post as jest.Mock).mock.calls[0][1][1].meta.survey.id).toEqual(AREA_SURVEY_ID);
	});
});

describe('getAllResources', () => {
	it('gets submissions for every resource type', async () => {
		(axios.get as jest.Mock).mockResolvedValue({ data: [] });

		await getAllResources({ authorizationHeaderValue: 'test' });

		expect(axios.get as jest.Mock).toHaveBeenCalledWith(expect.stringContaining(FIELD_SURVEY_ID), expect.anything());
		expect(axios.get as jest.Mock).toHaveBeenCalledWith(expect.stringContaining(AREA_SURVEY_ID), expect.anything());
		expect(axios.get as jest.Mock).toHaveBeenCalledWith(
			expect.stringContaining(STRATIFICATION_SURVEY_ID),
			expect.anything()
		);
		expect(axios.get as jest.Mock).toHaveBeenCalledWith(
			expect.stringContaining(LOCATION_COLLECTION_SURVEY_ID),
			expect.anything()
		);
		expect(axios.get as jest.Mock).toHaveBeenCalledWith(expect.stringContaining(LOCATION_SURVEY_ID), expect.anything());
		expect(axios.get as jest.Mock).toHaveBeenCalledWith(expect.stringContaining(SAMPLE_SURVEY_ID), expect.anything());
		expect(axios.get as jest.Mock).toHaveBeenCalledWith(expect.stringContaining(SAMPLING_SURVEY_ID), expect.anything());
		expect(axios.get as jest.Mock).toHaveBeenCalledWith(
			expect.stringContaining(SAMPLING_COLLECTION_SURVEY_ID),
			expect.anything()
		);
	});
});

describe('getSubmissions', () => {
	it('includes Authorization header', async () => {
		(axios.get as jest.Mock).mockResolvedValueOnce({ data: [] });

		await getSubmissions<Field>({
			resourceType: ResourceTypes.Field,
			authorizationHeaderValue: 'test',
		});

		expect((axios.get as jest.Mock).mock.calls[0][1].headers).toEqual(
			expect.objectContaining({
				Authorization: 'test',
			})
		);
	});

	it('includes survey id matching resource type in query parameter', async () => {
		(axios.get as jest.Mock).mockResolvedValue({ data: [] });

		await getSubmissions<Field>({
			resourceType: ResourceTypes.Field,
			authorizationHeaderValue: 'test',
		});

		await getSubmissions<Area>({
			resourceType: ResourceTypes.Area,
			authorizationHeaderValue: 'test',
		});

		expect((axios.get as jest.Mock).mock.calls[0][0]).toMatch(new RegExp(`survey=${FIELD_SURVEY_ID}`));
		expect((axios.get as jest.Mock).mock.calls[1][0]).toMatch(new RegExp(`survey=${AREA_SURVEY_ID}`));
	});

	it('includes match query parameter when submission ids are specified', async () => {
		(axios.get as jest.Mock).mockResolvedValueOnce({ data: [] });

		await getSubmissions<Field>({
			resourceType: ResourceTypes.Field,
			authorizationHeaderValue: 'test',
			ids: ['608c3c250173a10214c87176', '608c3c0c0173a10214c87175'],
		});

		const matchExpression = {
			_id: {
				$in: [{ $oid: '608c3c250173a10214c87176' }, { $oid: '608c3c0c0173a10214c87175' }],
			},
		};
		const expectedQueryParam = `match=${encodeURIComponent(JSON.stringify(matchExpression))}`;
		expect((axios.get as jest.Mock).mock.calls[0][0]).toMatch(new RegExp(expectedQueryParam));
	});

	it('transforms field submissions to field resources', async () => {
		(axios.get as jest.Mock).mockResolvedValueOnce(
			createMockSubmissionsResponse({ resourceType: ResourceTypes.Field })
		);

		const resources = await getSubmissions<Field>({
			resourceType: ResourceTypes.Field,
			authorizationHeaderValue: 'test',
		});

		(expect(resources[0]) as any).toMatchShapeOf(
			createMockField({
				id: '1',
				areas: [''],
			})
		);
	});

	it('transforms area submissions to area resources', async () => {
		(axios.get as jest.Mock).mockResolvedValueOnce(createMockSubmissionsResponse({ resourceType: ResourceTypes.Area }));

		const resources = await getSubmissions<Area>({
			resourceType: ResourceTypes.Area,
			authorizationHeaderValue: 'test',
		});

		(expect(resources[0]) as any).toMatchShapeOf(createMockArea({ id: '1' }));
	});

	it('excludes redacted/private submissions from response', async () => {
		const mockResponse = {
			data: [
				createMockResponseSubmission({ resourceType: ResourceTypes.Area, redacted: true }),
				createMockResponseSubmission({ resourceType: ResourceTypes.Area, redacted: true }),
				createMockResponseSubmission({ resourceType: ResourceTypes.Area, redacted: false }),
				createMockResponseSubmission({ resourceType: ResourceTypes.Area, redacted: false }),
			],
		};
		(axios.get as jest.Mock).mockResolvedValueOnce(mockResponse);

		const resources = await getSubmissions<Area>({
			resourceType: ResourceTypes.Area,
			authorizationHeaderValue: 'test',
		});

		expect(resources.length).toEqual(2);
	});
});

describe('getSubmission', () => {
	it('returns undefined if submission is redacted', async () => {
		const mockResponse = {
			data: [createMockResponseSubmission({ resourceType: ResourceTypes.Area, redacted: true })],
		};
		(axios.get as jest.Mock).mockResolvedValueOnce(mockResponse);

		const resource = await getSubmission<Area>({
			resourceType: ResourceTypes.Area,
			authorizationHeaderValue: 'test',
			id: '1',
		});

		expect(resource).toBeUndefined();
	});

	it('includes authorization header', async () => {
		(axios.get as jest.Mock).mockResolvedValueOnce({ data: [] });

		await getSubmission<Field>({
			resourceType: ResourceTypes.Field,
			authorizationHeaderValue: 'test',
			id: '1',
		});

		expect((axios.get as jest.Mock).mock.calls[0][1].headers).toEqual(
			expect.objectContaining({
				Authorization: 'test',
			})
		);
	});

	it('includes survey id matching resource type in query parameter', async () => {
		(axios.get as jest.Mock).mockResolvedValue({ data: [] });

		await getSubmission<Field>({
			resourceType: ResourceTypes.Field,
			authorizationHeaderValue: 'test',
			id: '1',
		});

		await getSubmission<Area>({
			resourceType: ResourceTypes.Area,
			authorizationHeaderValue: 'test',
			id: '1',
		});

		expect((axios.get as jest.Mock).mock.calls[0][0]).toMatch(new RegExp(`survey=${FIELD_SURVEY_ID}`));
		expect((axios.get as jest.Mock).mock.calls[1][0]).toMatch(new RegExp(`survey=${AREA_SURVEY_ID}`));
	});

	it('transforms submission to resource', async () => {
		(axios.get as jest.Mock).mockResolvedValueOnce(createMockSubmissionsResponse({ resourceType: ResourceTypes.Area }));

		const resource = await getSubmission<Area>({
			resourceType: ResourceTypes.Area,
			authorizationHeaderValue: 'test',
			id: '1',
		});

		(expect(resource) as any).toMatchShapeOf(createMockArea({ id: '1' }));
	});
});

describe('updateSubmission', () => {
	it('includes Authorization header', async () => {
		(axios.put as jest.Mock).mockResolvedValueOnce({});

		await updateSubmission({
			resource: createField(),
			resourceType: ResourceTypes.Field,
			groupId: 'groupId',
			authorizationHeaderValue: 'test',
		});

		expect((axios.put as jest.Mock).mock.calls[0][2].headers).toEqual(
			expect.objectContaining({
				Authorization: 'test',
			})
		);
	});

	it('includes survey id matching resource type in request body', async () => {
		(axios.put as jest.Mock).mockResolvedValue({});

		await updateSubmission({
			resource: createField(),
			resourceType: ResourceTypes.Field,
			groupId: 'groupId',
			authorizationHeaderValue: 'test',
		});

		await updateSubmission({
			resource: createArea(),
			resourceType: ResourceTypes.Area,
			groupId: 'groupId',
			authorizationHeaderValue: 'test',
		});

		expect((axios.put as jest.Mock).mock.calls[0][1].meta.survey.id).toEqual(FIELD_SURVEY_ID);
		expect((axios.put as jest.Mock).mock.calls[1][1].meta.survey.id).toEqual(AREA_SURVEY_ID);
	});

	it('includes RESUBMIT as meta.archivedReason in request body', async () => {
		(axios.put as jest.Mock).mockResolvedValue({});

		await updateSubmission({
			resource: createField(),
			resourceType: ResourceTypes.Field,
			groupId: 'groupId',
			authorizationHeaderValue: 'test',
		});

		expect((axios.put as jest.Mock).mock.calls[0][1].meta.archivedReason).toEqual('RESUBMIT');
	});
});

describe('getGroups', () => {
	let groups: Group[];
	beforeEach(async () => {
		(axios.get as jest.Mock).mockImplementation((url) => {
			if (url.includes('/groups')) {
				return Promise.resolve(createMockGroupsResponse());
			}
			if (url.includes('/memberships/tree')) {
				return Promise.resolve(createMockMembershipsTreeResponse());
			}
		});

		groups = await getGroups({ authorizationHeaderValue: 'email token' });
	});

	it('transforms surveystack groups to group resources', () => {
		(expect(groups[0]) as any).toMatchShapeOf(createGroup());
	});

	it('returns the intersection of (1) groups the user is an admin of and (2) groups within the soilstack base group', () => {
		expect(groups.length).toBe(1);
		expect(groups[0].id).toBe('mock group id 1');
	});

	describe('calling surveystack /groups endpoint', () => {
		it('passes soilstack base group as "dir" query parameter', () => {
			expect(axios.get as jest.Mock).toHaveBeenCalledWith(
				expect.stringContaining(`dir=${encodeURIComponent(SOILSTACK_BASE_GROUP_PATH)}`)
			);
		});

		it('passes "tree=true" query paramter so that we get back all descendant groups, not just one level of subgroups', () => {
			expect(axios.get as jest.Mock).toHaveBeenCalledWith(expect.stringContaining('tree=true'));
		});

		it('passes showArchived=false query paramter', () => {
			expect(axios.get as jest.Mock).toHaveBeenCalledWith(expect.stringContaining('showArchived=false'));
		});
	});

	describe('calling surveystack /memberships/tree endpoint', () => {
		it('passes Authorization header', async () => {
			expect(axios.get as jest.Mock).toHaveBeenCalledWith(
				expect.anything(),
				expect.objectContaining({
					headers: { Authorization: 'email token' },
				})
			);
		});
	});
});

describe('bulkReassign', () => {
	beforeEach(async () => {
		(axios.post as jest.Mock).mockResolvedValueOnce({});

		await bulkReassign({
			submissionIds: ['1', '2', '3'],
			groupId: 'groupId',
			authorizationHeaderValue: 'test',
		});
	});

	it('includes Authorization header', async () => {
		expect((axios.post as jest.Mock).mock.calls[0][2].headers).toEqual(
			expect.objectContaining({
				Authorization: 'test',
			})
		);
	});

	it('includes submissionIds as "ids" in request body', () => {
		expect((axios.post as jest.Mock).mock.calls[0][1]).toEqual(
			expect.objectContaining({
				ids: ['1', '2', '3'],
			})
		);
	});

	it('includes groupId as "group" in request body', () => {
		expect((axios.post as jest.Mock).mock.calls[0][1]).toEqual(
			expect.objectContaining({
				group: 'groupId',
			})
		);
	});
});

describe('bulkArchive', () => {
	beforeEach(async () => {
		(axios.post as jest.Mock).mockResolvedValueOnce({});

		await bulkArchive({
			ids: ['3', '4', '5'],
			authorizationHeaderValue: 'test',
			reason: 'some reason',
			set: false,
		});
	});

	it('includes Authorization header', async () => {
		expect((axios.post as jest.Mock).mock.calls[0][2].headers).toEqual(
			expect.objectContaining({
				Authorization: 'test',
			})
		);
	});

	it('includes submissionIds as "ids" in request body', () => {
		expect((axios.post as jest.Mock).mock.calls[0][1]).toEqual(
			expect.objectContaining({
				ids: ['3', '4', '5'],
			})
		);
	});

	it('includes encoded reason in request url', () => {
		expect((axios.post as jest.Mock).mock.calls[0][0]).toEqual(expect.stringContaining('reason=some%20reason'));
	});

	it('includes set in request url', () => {
		expect((axios.post as jest.Mock).mock.calls[0][0]).toEqual(expect.stringContaining('set=false'));
	});
});

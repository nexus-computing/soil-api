import { AnyObject } from '../../types';

const createMatchIdsExpression = (ids: string[]): AnyObject => ({
	_id: {
		$in: ids.map((id) => ({ $oid: id })),
	},
});

const createMatchFieldsByAreaIds = (areaIds: string[]): AnyObject => ({
	'data.areas.value': {
		$regex: `\"(${areaIds.join('|')})\"`,
	},
});

const createMatchAfterDateSubmitted = (timestamp: Date): AnyObject => {
	const timestampString = timestamp.toISOString();
	return {
		'meta.dateCreated': {
			$gte: { $date: timestampString },
		},
	};
};

const createMatchStratificationsByAreaIds = (areaIds: string[]): AnyObject => {
	return {
		'data.object.value': { $in: areaIds },
	};
};

const createMatchSamplingCollectionsByFieldId = (fieldId: string): AnyObject => {
	return {
		'data.feature_of_interest.value': fieldId,
	};
};

const createMatchLocationCollectionsByStratificationId = (stratificationId: string): AnyObject => {
	return {
		'data.result_of.value': stratificationId,
	};
};

const createMatchSamplingCollectionsByLocationCollectionIds = (locationCollectionIds: string[]): AnyObject => {
	return {
		'data.object.value': { $in: locationCollectionIds },
	};
};

export {
	createMatchAfterDateSubmitted,
	createMatchFieldsByAreaIds,
	createMatchIdsExpression,
	createMatchLocationCollectionsByStratificationId,
	createMatchSamplingCollectionsByFieldId,
	createMatchSamplingCollectionsByLocationCollectionIds,
	createMatchStratificationsByAreaIds,
};

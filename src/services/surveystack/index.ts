import { URLSearchParams } from 'url';
import axios, { AxiosResponse } from 'axios';
import { ResourceTypes, SOILSTACK_BASE_GROUP_PATH, SURVEYSTACK_API_URL } from '../../constants';
import { createGroup } from '../../resources';
import {
	AnyObject,
	Group,
	Resource,
	SubmissionInfo,
	SurveyStackGroup,
	SurveyStackMembership,
	SurveyStackSubmissionRequestBody,
	SurveyStackSubmissionResponseBody,
} from '../../types/index';
import { resourceToSubmission, submissionToResource, surveyInfo } from './mappings';
import {
	createMatchAfterDateSubmitted,
	createMatchFieldsByAreaIds,
	createMatchIdsExpression,
	createMatchLocationCollectionsByStratificationId,
	createMatchSamplingCollectionsByFieldId,
	createMatchSamplingCollectionsByLocationCollectionIds,
	createMatchStratificationsByAreaIds,
} from './matchExpressions';

const getResourceId = (resource: Resource): string => String(resource.id ?? '');

const isRedacted = (submission: SurveyStackSubmissionResponseBody) => Object.keys(submission.data).length === 0;

const createSubmissionRequestBody = (
	{ survey, group, data, id }: SubmissionInfo,
	isModify?: boolean
): SurveyStackSubmissionRequestBody => {
	const now = new Date(Date.now());
	const requestBody: SurveyStackSubmissionRequestBody = {
		_id: id,
		meta: {
			survey,
			group,
			specVersion: 3,
			dateModified: now,
		},
		data,
	};

	if (isModify) {
		requestBody.meta.archivedReason = 'RESUBMIT';
	} else {
		requestBody.meta.revision = 1;
		requestBody.meta.dateCreated = now;
	}

	return requestBody;
};

const createSubmissions = ({
	resources,
	groupId,
	authorizationHeaderValue,
}: {
	resources: { resource: Resource; resourceType: ResourceTypes }[];
	groupId: string;
	authorizationHeaderValue: string;
}): Promise<AxiosResponse> => {
	const requestBody = resources.map(({ resource, resourceType }) =>
		createSubmissionRequestBody({
			id: getResourceId(resource),
			survey: surveyInfo[resourceType],
			data: resourceToSubmission[resourceType](resource),
			group: {
				id: groupId,
				path: null,
			},
		})
	);

	return axios.post(`${SURVEYSTACK_API_URL}/submissions`, requestBody, {
		headers: {
			Authorization: authorizationHeaderValue,
		},
	});
};

const createSubmission = ({
	resource,
	resourceType,
	groupId,
	authorizationHeaderValue,
}: {
	resource: Resource;
	resourceType: ResourceTypes;
	groupId: string;
	authorizationHeaderValue: string;
}): Promise<AxiosResponse> => {
	const requestBody = createSubmissionRequestBody({
		id: getResourceId(resource),
		survey: surveyInfo[resourceType],
		data: resourceToSubmission[resourceType](resource),
		group: {
			id: groupId,
			path: null,
		},
	});
	return axios.post(`${SURVEYSTACK_API_URL}/submissions`, requestBody, {
		headers: {
			Authorization: authorizationHeaderValue,
		},
	});
};

const getSubmissions = async <T extends Resource>({
	resourceType,
	authorizationHeaderValue,
	ids,
	matchExpression,
}: {
	resourceType: ResourceTypes;
	authorizationHeaderValue: string;
	ids?: string[];
	matchExpression?: AnyObject;
}): Promise<T[]> => {
	if (ids?.length && matchExpression) {
		throw new Error('Cannot specify both ids and matchExpression');
	}

	const searchParams = new URLSearchParams();
	searchParams.set('survey', surveyInfo[resourceType].id);

	if (ids) {
		searchParams.set('match', JSON.stringify(createMatchIdsExpression(ids)));
	}
	if (matchExpression) {
		searchParams.set('match', JSON.stringify(matchExpression));
	}

	const queryParams = searchParams.toString() ? `?${searchParams.toString()}` : '';

	const response = await axios.get(`${SURVEYSTACK_API_URL}/submissions${queryParams}`, {
		headers: {
			Authorization: authorizationHeaderValue,
		},
	});

	const resources =
		response.data
			?.filter((submission: SurveyStackSubmissionResponseBody) => !isRedacted(submission))
			.map(submissionToResource[resourceType]) ?? [];

	return resources as T[];
};

const getSubmission = async <T extends Resource>({
	resourceType,
	authorizationHeaderValue,
	id,
}: {
	resourceType: ResourceTypes;
	authorizationHeaderValue: string;
	id: string;
}): Promise<T> => (await getSubmissions<T>({ resourceType, authorizationHeaderValue, ids: [id] }))[0];

const getAllResources = ({ authorizationHeaderValue }: { authorizationHeaderValue: string }): Promise<Resource[][]> => {
	const resourceTypes = [
		ResourceTypes.Field,
		ResourceTypes.Area,
		ResourceTypes.Stratification,
		ResourceTypes.LocationCollection,
		ResourceTypes.Location,
		ResourceTypes.Sample,
		ResourceTypes.Sampling,
		ResourceTypes.SamplingCollection,
	];

	return Promise.all(
		resourceTypes.map((resourceType) =>
			getSubmissions<Resource>({
				resourceType,
				authorizationHeaderValue,
			})
		)
	);
};

const updateSubmission = async ({
	resource,
	resourceType,
	groupId,
	authorizationHeaderValue,
}: {
	resource: Resource;
	resourceType: ResourceTypes;
	groupId: string;
	authorizationHeaderValue: string;
}): Promise<void> => {
	const requestBody = createSubmissionRequestBody(
		{
			id: getResourceId(resource),
			survey: surveyInfo[resourceType],
			data: resourceToSubmission[resourceType](resource),
			group: {
				id: groupId,
				path: null,
			},
		},
		true
	);

	await axios.put(`${SURVEYSTACK_API_URL}/submissions/${resource.id}`, requestBody, {
		headers: {
			Authorization: authorizationHeaderValue,
		},
	});
};

const getGroups = async ({ authorizationHeaderValue }: { authorizationHeaderValue: string }): Promise<Group[]> => {
	const searchParams = new URLSearchParams();
	searchParams.set('tree', 'true');
	searchParams.set('showArchived', 'false');
	searchParams.set('dir', SOILSTACK_BASE_GROUP_PATH);

	const [soilstackGroups, memberships] = await Promise.all([
		axios.get(`${SURVEYSTACK_API_URL}/groups?${searchParams.toString()}`),
		axios.get(`${SURVEYSTACK_API_URL}/memberships/tree`, {
			headers: { Authorization: authorizationHeaderValue },
		}),
	]);

	const groupIdsWhereAdmin = memberships.data
		?.filter((membership: SurveyStackMembership) => membership.roles.includes('admin'))
		.map(({ group }: SurveyStackMembership) => group._id);

	const soilstackGroupsWhereAdmin = soilstackGroups.data?.filter(({ _id }: SurveyStackGroup) =>
		groupIdsWhereAdmin.includes(_id)
	);

	const groups = soilstackGroupsWhereAdmin?.map((group: SurveyStackGroup) =>
		createGroup({
			id: group._id,
			name: group.name,
			path: group.path,
		})
	);

	return groups;
};

const bulkReassign = async ({
	submissionIds,
	groupId,
	authorizationHeaderValue,
}: {
	submissionIds: string[];
	groupId: string;
	authorizationHeaderValue: string;
}): Promise<void> =>
	await axios.post(
		`${SURVEYSTACK_API_URL}/submissions/bulk-reassign`,
		{ ids: submissionIds, group: groupId },
		{ headers: { Authorization: authorizationHeaderValue } }
	);

const bulkArchive = async ({
	ids,
	authorizationHeaderValue,
	reason = 'INCORRECT_DATA',
	set = true,
}: {
	ids: string[];
	authorizationHeaderValue: string;
	reason?: string;
	set?: boolean;
}): Promise<void> =>
	await axios.post(
		`${SURVEYSTACK_API_URL}/submissions/bulk-archive?set=${set}&reason=${encodeURI(reason)}`,
		{ ids },
		{ headers: { Authorization: authorizationHeaderValue } }
	);

export {
	bulkArchive,
	bulkReassign,
	createMatchAfterDateSubmitted,
	createMatchFieldsByAreaIds,
	createMatchIdsExpression,
	createMatchLocationCollectionsByStratificationId,
	createMatchSamplingCollectionsByFieldId,
	createMatchSamplingCollectionsByLocationCollectionIds,
	createMatchStratificationsByAreaIds,
	createSubmission,
	createSubmissions,
	getAllResources,
	getGroups,
	getSubmission,
	getSubmissions,
	updateSubmission,
};

import {
	AREA_SURVEY_ID,
	AREA_SURVEY_VERSION,
	FIELD_SURVEY_ID,
	FIELD_SURVEY_VERSION,
	LOCATION_COLLECTION_SURVEY_ID,
	LOCATION_COLLECTION_SURVEY_VERSION,
	LOCATION_SURVEY_ID,
	LOCATION_SURVEY_VERSION,
	ResourceTypes,
	SAMPLE_SURVEY_ID,
	SAMPLE_SURVEY_VERSION,
	SAMPLING_COLLECTION_SURVEY_ID,
	SAMPLING_COLLECTION_SURVEY_VERSION,
	SAMPLING_SURVEY_ID,
	SAMPLING_SURVEY_VERSION,
	STRATIFICATION_SURVEY_ID,
	STRATIFICATION_SURVEY_VERSION,
} from '../../constants';
import {
	areaToSubmissionData,
	fieldToSubmissionData,
	locationCollectionToSubmissionData,
	locationToSubmissionData,
	sampleToSubmissionData,
	samplingCollectionToSubmissionData,
	samplingToSubmissionData,
	stratificationToSubmissionData,
	submissionResponseToArea,
	submissionResponseToField,
	submissionResponseToLocation,
	submissionResponseToLocationCollection,
	submissionResponseToSample,
	submissionResponseToSampling,
	submissionResponseToSamplingCollection,
	submissionResponseToStratification,
} from '../../transforms';
import { Resource, SubmissionData, SurveyStackSubmissionResponseBody } from '../../types';

const surveyInfo = {
	[ResourceTypes.Field]: {
		id: FIELD_SURVEY_ID,
		version: FIELD_SURVEY_VERSION,
	},
	[ResourceTypes.Area]: {
		id: AREA_SURVEY_ID,
		version: AREA_SURVEY_VERSION,
	},
	[ResourceTypes.Stratification]: {
		id: STRATIFICATION_SURVEY_ID,
		version: STRATIFICATION_SURVEY_VERSION,
	},
	[ResourceTypes.LocationCollection]: {
		id: LOCATION_COLLECTION_SURVEY_ID,
		version: LOCATION_COLLECTION_SURVEY_VERSION,
	},
	[ResourceTypes.Location]: {
		id: LOCATION_SURVEY_ID,
		version: LOCATION_SURVEY_VERSION,
	},
	[ResourceTypes.Sample]: {
		id: SAMPLE_SURVEY_ID,
		version: SAMPLE_SURVEY_VERSION,
	},
	[ResourceTypes.Sampling]: {
		id: SAMPLING_SURVEY_ID,
		version: SAMPLING_SURVEY_VERSION,
	},
	[ResourceTypes.SamplingCollection]: {
		id: SAMPLING_COLLECTION_SURVEY_ID,
		version: SAMPLING_COLLECTION_SURVEY_VERSION,
	},
};

const resourceToSubmission: Record<ResourceTypes, (resource: Resource) => SubmissionData> = {
	[ResourceTypes.Field]: fieldToSubmissionData,
	[ResourceTypes.Area]: areaToSubmissionData,
	[ResourceTypes.Stratification]: stratificationToSubmissionData,
	[ResourceTypes.LocationCollection]: locationCollectionToSubmissionData,
	[ResourceTypes.Location]: locationToSubmissionData,
	[ResourceTypes.Sample]: sampleToSubmissionData,
	[ResourceTypes.Sampling]: samplingToSubmissionData,
	[ResourceTypes.SamplingCollection]: samplingCollectionToSubmissionData,
};

const submissionToResource: Record<ResourceTypes, (response: SurveyStackSubmissionResponseBody) => Resource> = {
	[ResourceTypes.Field]: submissionResponseToField,
	[ResourceTypes.Area]: submissionResponseToArea,
	[ResourceTypes.Stratification]: submissionResponseToStratification,
	[ResourceTypes.LocationCollection]: submissionResponseToLocationCollection,
	[ResourceTypes.Location]: submissionResponseToLocation,
	[ResourceTypes.Sample]: submissionResponseToSample,
	[ResourceTypes.Sampling]: submissionResponseToSampling,
	[ResourceTypes.SamplingCollection]: submissionResponseToSamplingCollection,
};

export { resourceToSubmission, submissionToResource, surveyInfo };

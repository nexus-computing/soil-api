import { NextFunction, Request, Response } from 'express';
import superAdminMiddleware from './superAdmin';

const mockRequest = (): Request => {
	const request: any = {};
	request.headers = {};
	request.get = jest.fn((key: string) => request.headers[key]);
	return request as Request;
};

const mockResponse = (): Response => {
	const response: any = {};
	response.status = jest.fn().mockReturnValue(response);
	response.send = jest.fn();
	return response as Response;
};

describe('superAdminMiddleware', () => {
	let request: Request;
	let response: Response;
	const next = jest.fn() as NextFunction;

	beforeEach(() => {
		request = mockRequest();
		response = mockResponse();
	});

	it('pass to next when X-Super-Authorization header is not included', () => {
		superAdminMiddleware(request, response, next);
		expect(next).toHaveBeenCalledTimes(1);
	});

	it('pass to next when the X-Super-Authorization header is correct', () => {
		request.headers = { 'x-super-authorization': 'password' };
		superAdminMiddleware(request, response, next);
		expect(next).toHaveBeenCalledTimes(1);
	});

	it('returns 401 when X-Super-authorization header is not valid', () => {
		request.headers = { 'x-super-authorization': 'wrong password' };
		superAdminMiddleware(request, response, next);
		expect(response.status).toHaveBeenCalledWith(401);
	});
});

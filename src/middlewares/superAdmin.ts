import { NextFunction, Request, Response } from 'express';
import { SUPER_ADMIN_PASSWORD } from '../constants';

const superAdminAuthorizationKey = 'x-super-authorization';

const superAdminMiddleware = (req: Request, res: Response, next: NextFunction): any => {
	if (superAdminAuthorizationKey in req.headers) {
		if (req.get(superAdminAuthorizationKey) !== SUPER_ADMIN_PASSWORD) {
			res.status(401).send();
			return;
		}
	}

	next();
};

export default superAdminMiddleware;

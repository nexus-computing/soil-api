import request from 'supertest';
import app from './app';
import { INTERNAL_API_DOCS_URL } from './constants';
import { internalOpenApiSpecDocument, openApiSpecDocument, publicOpenApiSpecDocument } from './open-api-spec';

describe('/open-api-spec.json', () => {
	it('returns the public open api spec document as json', (done) => {
		request(app)
			.get('/open-api-spec.json')
			.end((err, res) => {
				expect(res.text).toEqual(JSON.stringify(publicOpenApiSpecDocument));
				done();
			});
	});
});

describe(`${INTERNAL_API_DOCS_URL}/all.json`, () => {
	it('returns the entire open api spec document as json', (done) => {
		request(app)
			.get(`${INTERNAL_API_DOCS_URL}/all.json`)
			.end((err, res) => {
				expect(res.text).toEqual(JSON.stringify(openApiSpecDocument));
				done();
			});
	});
});

describe(`${INTERNAL_API_DOCS_URL}/internal.json`, () => {
	it('returns the internal open api spec document as json', (done) => {
		request(app)
			.get(`${INTERNAL_API_DOCS_URL}/internal.json`)
			.end((err, res) => {
				expect(res.text).toEqual(JSON.stringify(internalOpenApiSpecDocument));
				done();
			});
	});
});

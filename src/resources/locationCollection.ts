import { LocationCollection } from '../types';

const createLocationCollection = ({
	id,
	object = '',
	resultOf = '',
	featureOfInterest = '',
	features = [],
	meta = {},
}: Partial<LocationCollection> = {}): LocationCollection =>
	id
		? {
				id,
				object,
				resultOf,
				featureOfInterest,
				features,
				meta,
		  }
		: {
				object,
				resultOf,
				featureOfInterest,
				features,
				meta,
		  };

export { createLocationCollection };

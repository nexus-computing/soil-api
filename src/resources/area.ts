import { Polygon } from 'geojson';
import { Area } from '../types';

const createPolygon = ({ coordinates = [] } = {}): Polygon => {
	return {
		type: 'Polygon',
		coordinates,
	};
};

const createArea = ({ id, properties = {}, geometry = createPolygon(), meta = {} }: Partial<Area> = {}): Area =>
	id
		? {
				id,
				type: 'Feature',
				properties,
				geometry,
				meta,
		  }
		: {
				type: 'Feature',
				properties,
				geometry,
				meta,
		  };

export { createArea, createPolygon };

import { Group } from '../types';

const createGroup = ({ id = '', name = '', path = '' } = {}): Group => {
	return {
		id,
		name,
		path,
	};
};

export { createGroup };

import { Point } from 'geojson';

const createPoint = ({ coordinates = [0, 0] } = {}): Point => ({
	type: 'Point',
	coordinates,
});

export { createPoint };

import { Location } from '../types';
import { createPoint } from './geojson';

const createLocation = ({
	id,
	geometry = createPoint(),
	properties = {},
	meta = {},
}: Partial<Location> = {}): Location =>
	id
		? {
				id,
				type: 'Feature',
				geometry,
				properties,
				meta,
		  }
		: {
				type: 'Feature',
				geometry,
				properties,
				meta,
		  };

export { createLocation };

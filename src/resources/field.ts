import { Address, ContactPoint, Field } from '../types';

const createAddress = ({
	streetAddress = '',
	addressLocality = '',
	addressRegion = '',
	addressCountry = '',
	postalCode = '',
} = {}): Address => {
	return {
		streetAddress,
		addressLocality,
		addressRegion,
		addressCountry,
		postalCode,
	};
};

const createContactPoint = ({
	telephone = '',
	email = '',
	name = '',
	contactType = '',
	organization = '',
} = {}): ContactPoint => {
	return {
		telephone,
		email,
		name,
		contactType,
		organization,
	};
};

const createField = ({
	id,
	name = '',
	alternateName = '',
	producerName = '',
	address = createAddress(),
	contactPoints = [createContactPoint()],
	areas = [] as string[],
	meta = {},
}: Partial<Field> = {}): Field =>
	id
		? {
				id,
				name,
				alternateName,
				producerName,
				address,
				contactPoints,
				areas,
				meta,
		  }
		: {
				name,
				alternateName,
				producerName,
				address,
				contactPoints,
				areas,
				meta,
		  };

export { createAddress, createContactPoint, createField };

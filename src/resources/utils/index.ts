import {
	Area,
	Field,
	Location,
	LocationCollection,
	Sample,
	Sampling,
	SamplingCollection,
	Stratification,
} from '../../types';

interface Resources {
	fields: Field[];
	areas: Area[];
	stratifications: Stratification[];
	locationCollections: LocationCollection[];
	locations: Location[];
	samples: Sample[];
	samplings: Sampling[];
	samplingCollections: SamplingCollection[];
}

const findResourcesRelatedToField = (resources: Resources, fieldId: string): Resources => {
	const fields: Field[] = resources.fields.filter((field) => field.id === fieldId);
	const areaIds: string[] = fields.flatMap((field: Field) => field.areas);
	const locationCollections: LocationCollection[] = resources.locationCollections.filter(
		(locationCollection) => locationCollection.featureOfInterest === fieldId
	);
	const locationIds = locationCollections.flatMap(
		(locationCollection: LocationCollection) => locationCollection.features
	);
	const samplingCollections: SamplingCollection[] = resources.samplingCollections.filter(
		(samplingCollection) => samplingCollection.featureOfInterest === fieldId
	);
	const samplingCollectionIds = samplingCollections.map((samplingCollection) => samplingCollection.id);

	return {
		fields,
		areas: resources.areas.filter((area) => areaIds.includes(area.id as string)),
		stratifications: resources.stratifications.filter((stratification) => areaIds.includes(stratification.object)),
		locationCollections,
		locations: resources.locations.filter((location) => locationIds.includes(location.id as string)),
		samples: resources.samples.filter((sample) => sample.sampleOf === fieldId),
		samplings: resources.samplings.filter((sampling) => samplingCollectionIds.includes(sampling.memberOf)),
		samplingCollections,
	};
};

const resourcesToSubmissionIds = (resources: Resources) =>
	Object.fromEntries(
		Object.entries(resources).map(([key, value]) => [key, value.map((resource: any) => resource.meta.submissionId)])
	);

const getResourceSubmissionIdsRelatedToField = (resources: Resources, fieldId: string) =>
	Object.values(resourcesToSubmissionIds(findResourcesRelatedToField(resources, fieldId))).flat();

export { findResourcesRelatedToField, getResourceSubmissionIdsRelatedToField };

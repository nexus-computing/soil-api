import {
	createMockArea,
	createMockField,
	createMockLocation,
	createMockLocationCollection,
	createMockSample,
	createMockSampling,
	createMockSamplingCollection,
	createMockStratification,
} from '../../test-utils';
import { findResourcesRelatedToField, getResourceSubmissionIdsRelatedToField } from './index';

const mockFieldId = 'relatedField';
const mockResources = {
	fields: [
		createMockField({ id: mockFieldId, areas: ['relatedArea'], meta: { submissionId: `_${mockFieldId}` } }),
		createMockField({ id: 'unrelatedField', meta: { submissionId: '_unrelatedField' } }),
	],
	areas: [
		createMockArea({ id: 'relatedArea', meta: { submissionId: '_relatedArea' } }),
		createMockArea({ id: 'unrelatedArea', meta: { submissionId: '_unrelatedArea' } }),
	],
	stratifications: [
		createMockStratification({
			id: 'relatedStratification',
			object: 'relatedArea',
			meta: { submissionId: '_relatedStratification' },
		}),
		createMockStratification({
			id: 'relatedStratification',
			object: 'unrelatedArea',
			meta: { submissionId: '_relatedStratification' },
		}),
	],
	locationCollections: [
		createMockLocationCollection({
			id: 'relatedLocationCollection',
			object: 'relatedArea',
			resultOf: 'relatedStratification',
			featureOfInterest: 'relatedField',
			features: ['relatedLocation1', 'relatedLocation2'],
			meta: { submissionId: '_relatedLocationCollection' },
		}),
		createMockLocationCollection({
			id: 'unrelatedLocationCollection',
			object: 'unrelatedArea',
			resultOf: 'unrelatedStratification',
			featureOfInterest: 'unrelatedField',
			features: ['unrelatedLocation1', 'unrelatedLocation2'],
			meta: { submissionId: '_unrelatedLocationCollection' },
		}),
	],
	locations: [
		createMockLocation({ id: 'relatedLocation1', meta: { submissionId: '_relatedLocation1' } }),
		createMockLocation({ id: 'relatedLocation2', meta: { submissionId: '_relatedLocation2' } }),
		createMockLocation({ id: 'unrelatedLocation1', meta: { submissionId: '_unrelatedLocation1' } }),
		createMockLocation({ id: 'unrelatedLocation1', meta: { submissionId: '_unrelatedLocation1' } }),
	],
	samples: [
		createMockSample({
			id: 'relatedSample1',
			sampleOf: 'relatedField',
			resultOf: 'relatedSampling',
			meta: { submissionId: '_relatedSample1' },
		}),
		createMockSample({
			id: 'relatedSample2',
			sampleOf: 'relatedField',
			resultOf: 'relatedSampling',
			meta: { submissionId: '_relatedSample2' },
		}),
		createMockSample({
			id: 'unrelatedSample1',
			sampleOf: 'unrelatedField',
			resultOf: 'unrelatedSampling',
			meta: { submissionId: '_unrelatedSample1' },
		}),
		createMockSample({
			id: 'unrelatedSample2',
			sampleOf: 'unrelatedField',
			resultOf: 'unrelatedSampling',
			meta: { submissionId: '_unrelatedSample2' },
		}),
	],
	samplings: [
		createMockSampling({
			id: 'relatedSampling',
			featureOfInterest: 'relatedLocation',
			memberOf: 'relatedSamplingCollection',
			results: ['relatedSample1', 'relatedSample2'],
			meta: { submissionId: '_relatedSampling' },
		}),
		createMockSampling({
			id: 'unrelatedSampling',
			featureOfInterest: 'unrelatedLocation',
			memberOf: 'unrelatedSamplingCollection',
			results: ['unrelatedSample1', 'unrelatedSample2'],
			meta: { submissionId: '_unrelatedSampling' },
		}),
	],
	samplingCollections: [
		createMockSamplingCollection({
			id: 'relatedSamplingCollection',
			featureOfInterest: 'relatedField',
			object: 'relatedLocationCollection',
			members: ['relatedSampling'],
			meta: { submissionId: '_relatedSamplingCollection' },
		}),
		createMockSamplingCollection({
			id: 'unrelatedSamplingCollection',
			featureOfInterest: 'unrelatedField',
			object: 'unrelatedLocationCollection',
			members: ['unrelatedSampling'],
			meta: { submissionId: '_unrelatedSamplingCollection' },
		}),
	],
};

describe('findResourcesRelatedToField', () => {
	const result = findResourcesRelatedToField(mockResources, mockFieldId);

	it('returns the field with the passed in id', () => {
		expect(result.fields).toContainEqual(expect.objectContaining({ id: 'relatedField' }));
	});

	it('returns the areas belonging to the field', () => {
		expect(result.areas).toContainEqual(expect.objectContaining({ id: 'relatedArea' }));
	});

	it('returns the stratifications belonging to the field', () => {
		expect(result.stratifications).toContainEqual(expect.objectContaining({ id: 'relatedStratification' }));
	});

	it('returns the location collections that reference the field', () => {
		expect(result.locationCollections).toContainEqual(expect.objectContaining({ id: 'relatedLocationCollection' }));
	});

	it('returns the locations referenced by the location collections that reference the field', () => {
		expect(result.locations).toContainEqual(expect.objectContaining({ id: 'relatedLocation1' }));
		expect(result.locations).toContainEqual(expect.objectContaining({ id: 'relatedLocation2' }));
	});

	it('returns the samples that reference the field', () => {
		expect(result.samples).toContainEqual(expect.objectContaining({ id: 'relatedSample1' }));
		expect(result.samples).toContainEqual(expect.objectContaining({ id: 'relatedSample2' }));
	});

	it('returns the samplings referenced by the sampling collection that references the field', () => {
		expect(result.samplings).toContainEqual(expect.objectContaining({ id: 'relatedSampling' }));
	});

	it('returns the sampling collections that reference the field', () => {
		expect(result.samplingCollections).toContainEqual(expect.objectContaining({ id: 'relatedSamplingCollection' }));
	});
});

describe('getResourceSubmissionIdsRelatedToField', () => {
	const result = getResourceSubmissionIdsRelatedToField(mockResources, mockFieldId);

	it("returns a flat array of all related resources' ids", () => {
		expect(result).toEqual([
			'_relatedField',
			'_relatedArea',
			'_relatedStratification',
			'_relatedLocationCollection',
			'_relatedLocation1',
			'_relatedLocation2',
			'_relatedSample1',
			'_relatedSample2',
			'_relatedSampling',
			'_relatedSamplingCollection',
		]);
	});
});

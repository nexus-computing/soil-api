import { Sample, Sampling, SamplingCollection, SoDepth, SoDepthValue } from '../types';
import { createPoint } from './geojson';

const createSoDepthValue = ({ value = 0, unit = '' }: Partial<SoDepthValue> = {}) => ({
	value,
	unit,
});

const createSoDepth = ({
	minValue = createSoDepthValue(),
	maxValue = createSoDepthValue(),
}: Partial<SoDepth> = {}): SoDepth => ({
	minValue,
	maxValue,
});

const createSample = ({
	id,
	name = '',
	sampleOf = '',
	results = [],
	resultOf = '',
	soDepth = createSoDepth(),
	meta = {},
}: Partial<Sample> = {}): Sample =>
	id
		? {
				id,
				name,
				sampleOf,
				results,
				resultOf,
				soDepth,
				meta,
		  }
		: {
				name,
				sampleOf,
				results,
				resultOf,
				soDepth,
				meta,
		  };

const createSampling = ({
	id,
	name = '',
	resultTime = '',
	geometry = createPoint(),
	featureOfInterest = '',
	memberOf = '',
	results = [],
	comment = '',
	procedures = [],
	properties = {},
	soDepth = createSoDepth(),
	meta = {},
}: Partial<Sampling> = {}): Sampling =>
	id
		? {
				id,
				name,
				resultTime,
				geometry,
				featureOfInterest,
				memberOf,
				results,
				comment,
				procedures,
				properties,
				soDepth,
				meta,
		  }
		: {
				name,
				resultTime,
				geometry,
				featureOfInterest,
				memberOf,
				results,
				comment,
				procedures,
				properties,
				soDepth,
				meta,
		  };

const createSamplingCollection = ({
	id,
	name = '',
	comment = '',
	creator = '',
	featureOfInterest = '',
	participants = [],
	object = '',
	members = [],
	soDepth = createSoDepth(),
	sampleDepths = [],
	dateCreated = '',
	dateModified = '',
	meta = {},
}: Partial<SamplingCollection> = {}): SamplingCollection =>
	id
		? {
				id,
				name,
				comment,
				creator,
				featureOfInterest,
				participants,
				object,
				members,
				soDepth,
				sampleDepths,
				dateCreated,
				dateModified,
				meta,
		  }
		: {
				name,
				comment,
				creator,
				featureOfInterest,
				participants,
				object,
				members,
				soDepth,
				sampleDepths,
				dateCreated,
				dateModified,
				meta,
		  };

export { createSample, createSampling, createSamplingCollection };

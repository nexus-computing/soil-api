import { Algorithm, Stratification } from '../types';

const createAlgorithm = ({
	name = '',
	alternateName = '',
	codeRepository = '',
	version = '',
	doi = '',
}: Partial<Algorithm> = {}): Algorithm => ({
	name,
	alternateName,
	codeRepository,
	version,
	doi,
});

const createStratification = ({
	id,
	name = '',
	agent = '',
	dateCreated = '',
	provider = '',
	object = '',
	algorithm = createAlgorithm(),
	input = [],
	result = [],
	meta = {},
}: Partial<Stratification> = {}): Stratification =>
	id
		? {
				id,
				name,
				agent,
				dateCreated,
				provider,
				object,
				algorithm,
				input,
				result,
				meta,
		  }
		: {
				name,
				agent,
				dateCreated,
				provider,
				object,
				algorithm,
				input,
				result,
				meta,
		  };

export { createAlgorithm, createStratification };

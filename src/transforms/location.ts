import { createLocation } from '../resources';
import { Location, Resource, SubmissionData, SurveyStackSubmissionResponseBody } from '../types';
import { createMeta } from './common/resourceToSubmission';
import { idFromSubmission, surveystackMetaFromSubmission } from './common/submissionToResource';

const submissionResponseToLocation = (submission: SurveyStackSubmissionResponseBody): Location => {
	return createLocation({
		id: idFromSubmission(submission),
		type: submission.data.geojson.value.features[0].type,
		geometry: submission.data.geojson.value.features[0].geometry,
		properties: submission.data.geojson.value.features[0].properties,
		meta: surveystackMetaFromSubmission(submission),
	});
};

const locationToSubmissionData = (value: Resource): SubmissionData => {
	const location = value as Location;
	const now = new Date(Date.now());
	const metaType = createMeta(now);
	return {
		id: {
			value: location.id,
			meta: metaType('string'),
		},
		geojson: {
			value: {
				type: 'FeatureCollection',
				features: [
					{
						type: 'Feature',
						geometry: location.geometry,
						properties: location.properties,
					},
				],
			},
			meta: metaType('geoJSON'),
		},
	};
};

export { locationToSubmissionData, submissionResponseToLocation };

import { createLocationCollection } from '../resources';
import { LocationCollection, Resource, SubmissionData, SurveyStackSubmissionResponseBody } from '../types';
import { createMeta } from './common/resourceToSubmission';
import { idFromSubmission, surveystackMetaFromSubmission } from './common/submissionToResource';

const submissionResponseToLocationCollection = (submission: SurveyStackSubmissionResponseBody): LocationCollection => {
	return createLocationCollection({
		id: idFromSubmission(submission),
		object: submission.data.object.value,
		resultOf: submission.data.result_of.value,
		featureOfInterest: submission.data.feature_of_interest.value,
		features: JSON.parse(submission.data.features.value),
		meta: surveystackMetaFromSubmission(submission),
	});
};

const locationCollectionToSubmissionData = (value: Resource): SubmissionData => {
	const locationCollection = value as LocationCollection;
	const now = new Date(Date.now());
	const metaType = createMeta(now);
	return {
		id: {
			value: locationCollection.id,
			meta: metaType('string'),
		},
		object: {
			value: locationCollection.object,
			meta: metaType('string'),
		},
		result_of: {
			value: locationCollection.resultOf,
			meta: metaType('string'),
		},
		feature_of_interest: {
			value: locationCollection.featureOfInterest,
			meta: metaType('string'),
		},
		features: {
			value: JSON.stringify(locationCollection.features),
			meta: metaType('string'),
		},
	};
};

export { locationCollectionToSubmissionData, submissionResponseToLocationCollection };

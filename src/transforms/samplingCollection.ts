import { createSamplingCollection } from '../resources';
import { Resource, SamplingCollection, SubmissionData, SurveyStackSubmissionResponseBody } from '../types';
import { createMeta } from './common/resourceToSubmission';
import { idFromSubmission, surveystackMetaFromSubmission } from './common/submissionToResource';

const submissionResponseToSamplingCollection = (submission: SurveyStackSubmissionResponseBody): SamplingCollection => {
	return createSamplingCollection({
		id: idFromSubmission(submission),
		name: submission.data.name.value,
		comment: submission.data.comment.value,
		creator: submission.data.creator.value,
		featureOfInterest: submission.data.feature_of_interest.value,
		participants: JSON.parse(submission.data.participants.value),
		object: submission.data.object.value,
		members: JSON.parse(submission.data.members.value),
		soDepth: JSON.parse(submission.data.so_depth.value),
		sampleDepths: JSON.parse(submission.data.sample_depths.value),
		dateCreated: submission.data.date_created.value,
		dateModified: submission.data.date_modified.value,
		meta: {
			referenceIds: submission.data.meta.reference_ids.value?.map(
				(refId: { owner: { value: string }; id: { value: string } }) => ({
					owner: refId.owner.value,
					id: refId.id.value,
				})
			),
			...surveystackMetaFromSubmission(submission),
		},
	});
};

const samplingCollectionToSubmissionData = (value: Resource): SubmissionData => {
	const samplingCollection = value as SamplingCollection;
	const now = new Date(Date.now());
	const metaType = createMeta(now);
	return {
		id: {
			value: samplingCollection.id,
			meta: metaType('string'),
		},
		name: {
			value: samplingCollection.name,
			meta: metaType('string'),
		},
		comment: {
			value: samplingCollection.comment,
			meta: metaType('string'),
		},
		creator: {
			value: samplingCollection.creator,
			meta: metaType('string'),
		},
		feature_of_interest: {
			value: samplingCollection.featureOfInterest,
			meta: metaType('string'),
		},
		participants: {
			value: JSON.stringify(samplingCollection.participants),
			meta: metaType('string'),
		},
		object: {
			value: samplingCollection.object,
			meta: metaType('string'),
		},
		members: {
			value: JSON.stringify(samplingCollection.members),
			meta: metaType('string'),
		},
		so_depth: {
			value: JSON.stringify(samplingCollection.soDepth),
			meta: metaType('string'),
		},
		sample_depths: {
			value: JSON.stringify(samplingCollection.sampleDepths),
			meta: metaType('string'),
		},
		date_created: {
			value: samplingCollection.dateCreated,
			meta: metaType('date'),
		},
		date_modified: {
			value: samplingCollection.dateModified,
			meta: metaType('date'),
		},
		meta: {
			reference_ids: {
				meta: metaType('matrix'),
				value:
					samplingCollection.meta.referenceIds?.map((refId) => ({
						owner: { value: refId.owner },
						id: { value: refId.id },
					})) ?? [],
			},
			meta: metaType('group'),
		},
	};
};

export { samplingCollectionToSubmissionData, submissionResponseToSamplingCollection };

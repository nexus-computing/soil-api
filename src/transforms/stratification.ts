import { createAlgorithm, createStratification } from '../resources';
import { Resource, Stratification, SubmissionData, SurveyStackSubmissionResponseBody } from '../types';
import { createMeta, permissionsOnlyMeta } from './common/resourceToSubmission';
import { idFromSubmission, surveystackMetaFromSubmission } from './common/submissionToResource';

const submissionResponseToStratification = (submission: SurveyStackSubmissionResponseBody): Stratification => {
	return createStratification({
		id: idFromSubmission(submission),
		name: submission.data.name.value,
		agent: submission.data.agent.value,
		dateCreated: submission.data.date_created.value,
		provider: submission.data.provider.value,
		object: submission.data.object.value,
		algorithm: createAlgorithm({
			name: submission.data.algorithm.name.value,
			alternateName: submission.data.algorithm.alternate_name.value,
			codeRepository: submission.data.algorithm.code_repository.value,
			version: submission.data.algorithm.version.value,
			doi: submission.data.algorithm.doi.value,
		}),
		input: submission.data.input.value?.map(({ name, value }: { name: { value: string }; value: { value: any } }) => ({
			name: name.value,
			value: JSON.parse(value.value),
		})),
		result: submission.data.result.value?.map(
			({ name, value }: { name: { value: string }; value: { value: any } }) => ({
				name: name.value,
				value: JSON.parse(value.value),
			})
		),
		meta: surveystackMetaFromSubmission(submission),
	});
};

const stratificationToSubmissionData = (value: Resource): SubmissionData => {
	const stratification = value as Stratification;
	const now = new Date(Date.now());
	const metaType = createMeta(now);
	return {
		id: {
			value: stratification.id,
			meta: metaType('string'),
		},
		name: {
			value: stratification.name,
			meta: metaType('string'),
		},
		agent: {
			value: stratification.agent,
			meta: metaType('string'),
		},
		date_created: {
			value: stratification.dateCreated,
			meta: metaType('date'),
		},
		provider: {
			value: stratification.provider,
			meta: metaType('string'),
		},
		object: {
			value: stratification.object,
			meta: metaType('string'),
		},
		algorithm: {
			name: {
				value: stratification.algorithm.name,
				meta: metaType('string'),
			},
			alternate_name: {
				value: stratification.algorithm.alternateName,
				meta: metaType('string'),
			},
			code_repository: {
				value: stratification.algorithm.codeRepository,
				meta: metaType('string'),
			},
			version: {
				value: stratification.algorithm.version,
				meta: metaType('string'),
			},
			doi: {
				value: stratification.algorithm.doi,
				meta: metaType('string'),
			},
			meta: metaType('group'),
		},
		input: {
			value: stratification.input?.map((item) => ({
				name: { value: item.name, meta: permissionsOnlyMeta },
				value: { value: JSON.stringify(item.value), meta: permissionsOnlyMeta },
			})),
			meta: metaType('matrix'),
		},
		result: {
			value: stratification.result?.map((item) => ({
				name: { value: item.name, meta: permissionsOnlyMeta },
				value: { value: JSON.stringify(item.value), meta: permissionsOnlyMeta },
			})),
			meta: metaType('matrix'),
		},
	};
};

export { stratificationToSubmissionData, submissionResponseToStratification };

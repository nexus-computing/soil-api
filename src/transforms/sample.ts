import { createSample } from '../resources';
import { Resource, Sample, SubmissionData, SurveyStackSubmissionResponseBody } from '../types';
import { createMeta } from './common/resourceToSubmission';
import { idFromSubmission, surveystackMetaFromSubmission } from './common/submissionToResource';

const submissionResponseToSample = (submission: SurveyStackSubmissionResponseBody): Sample => {
	return createSample({
		id: idFromSubmission(submission),
		name: submission.data.name.value,
		sampleOf: submission.data.sample_of.value,
		results: JSON.parse(submission.data.results.value),
		resultOf: submission.data.result_of.value,
		soDepth: JSON.parse(submission.data.so_depth.value),
		meta: surveystackMetaFromSubmission(submission),
	});
};

const sampleToSubmissionData = (value: Resource): SubmissionData => {
	const sample = value as Sample;
	const now = new Date(Date.now());
	const metaType = createMeta(now);
	return {
		id: {
			value: sample.id,
			meta: metaType('string'),
		},
		name: {
			value: sample.name,
			meta: metaType('string'),
		},
		sample_of: {
			value: sample.sampleOf,
			meta: metaType('string'),
		},
		results: {
			value: JSON.stringify(sample.results),
			meta: metaType('string'),
		},
		result_of: {
			value: sample.resultOf,
			meta: metaType('string'),
		},
		so_depth: {
			value: JSON.stringify(sample.soDepth),
			meta: metaType('string'),
		},
	};
};

export { sampleToSubmissionData, submissionResponseToSample };

import { ResourceTypes } from '../constants';
import { createMockResponseSubmission, createMockSample } from '../test-utils';
import { sampleToSubmissionData, submissionResponseToSample } from './sample';

describe('submissionResponseToSample', () => {
	const mockSubmissionResponse = createMockResponseSubmission({ resourceType: ResourceTypes.Sample });
	const sample = submissionResponseToSample(mockSubmissionResponse);

	it('maps id value', () => {
		expect(sample.id).toEqual(mockSubmissionResponse.data.id.value);
	});

	it('maps sampleOf value', () => {
		expect(sample.sampleOf).toEqual(mockSubmissionResponse.data.sample_of.value);
	});

	it('maps results value, parsing JSON', () => {
		expect(sample.results).toEqual(JSON.parse(mockSubmissionResponse.data.results.value));
	});

	it('maps resultOf value', () => {
		expect(sample.resultOf).toEqual(mockSubmissionResponse.data.result_of.value);
	});

	it('maps soDepth value, parsing JSON', () => {
		expect(sample.soDepth).toEqual(JSON.parse(mockSubmissionResponse.data.so_depth.value));
	});

	it('maps dateCreated from response meta object into meta.submittedAt', () => {
		expect(sample.meta?.submittedAt).toEqual(mockSubmissionResponse.meta.dateCreated);
	});
});

describe('sampleToSubmissionData', () => {
	const mockSample = createMockSample();
	const submissionData = sampleToSubmissionData(mockSample);

	it('maps name to string question type', () => {
		expect(submissionData.name.value).toEqual(mockSample.name);
		expect(submissionData.name.meta.type).toEqual('string');
	});

	it('maps sampleOf to string question type', () => {
		expect(submissionData.sample_of.value).toEqual(mockSample.sampleOf);
		expect(submissionData.sample_of.meta.type).toEqual('string');
	});

	it('maps results to string question type, stringifying JSON', () => {
		expect(submissionData.results.value).toEqual(JSON.stringify(mockSample.results));
		expect(submissionData.results.meta.type).toEqual('string');
	});

	it('maps resultOf to string question type', () => {
		expect(submissionData.result_of.value).toEqual(mockSample.resultOf);
		expect(submissionData.result_of.meta.type).toEqual('string');
	});

	it('maps soDepth to string question type, stringifying JSON', () => {
		expect(submissionData.so_depth.value).toEqual(JSON.stringify(mockSample.soDepth));
		expect(submissionData.so_depth.meta.type).toEqual('string');
	});

	it('has meta.type on each property', () => {
		expect(Object.values(submissionData).every((val) => Boolean(val.meta.type))).toBe(true);
	});

	it('has meta.dateModified on each property', () => {
		expect(Object.values(submissionData).every((val) => Boolean(val.meta.dateModified))).toBe(true);
	});

	it('has meta.permission on each property', () => {
		expect(Object.values(submissionData).every((val) => val.meta.permissions.includes('admin'))).toBe(true);
	});
});

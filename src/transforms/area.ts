import { createArea } from '../resources';
import { Area, Resource, SubmissionData, SurveyStackSubmissionResponseBody } from '../types';
import { createMeta } from './common/resourceToSubmission';
import { idFromSubmission, surveystackMetaFromSubmission } from './common/submissionToResource';

const submissionResponseToArea = (submission: SurveyStackSubmissionResponseBody): Area => {
	return createArea({
		id: idFromSubmission(submission),
		type: submission.data.geojson.value.features[0].type,
		geometry: submission.data.geojson.value.features[0].geometry,
		properties: submission.data.geojson.value.features[0].properties,
		meta: surveystackMetaFromSubmission(submission),
	});
};

const areaToSubmissionData = (value: Resource): SubmissionData => {
	const area = value as Area;
	const now = new Date(Date.now());
	const metaType = createMeta(now);
	return {
		id: {
			value: area.id,
			meta: metaType('string'),
		},
		geojson: {
			value: {
				type: 'FeatureCollection',
				features: [
					{
						type: 'Feature',
						geometry: area.geometry,
						properties: area.properties,
					},
				],
			},
			meta: metaType('geoJSON'),
		},
	};
};

export { areaToSubmissionData, submissionResponseToArea };

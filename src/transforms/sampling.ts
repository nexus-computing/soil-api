import { createSampling } from '../resources';
import { Resource, Sampling, SubmissionData, SurveyStackSubmissionResponseBody } from '../types';
import { createMeta } from './common/resourceToSubmission';
import { idFromSubmission, surveystackMetaFromSubmission } from './common/submissionToResource';

const submissionResponseToSampling = (submission: SurveyStackSubmissionResponseBody): Sampling => {
	return createSampling({
		id: idFromSubmission(submission),
		name: submission.data.name.value,
		resultTime: submission.data.result_time.value,
		geometry: submission.data.geometry.value.features[0].geometry,
		featureOfInterest: submission.data.feature_of_interest.value,
		memberOf: submission.data.member_of.value,
		results: JSON.parse(submission.data.results.value),
		comment: submission.data.comment.value,
		procedures: JSON.parse(submission.data.procedures.value),
		properties: JSON.parse(submission.data.properties.value),
		soDepth: JSON.parse(submission.data.so_depth.value),
		meta: surveystackMetaFromSubmission(submission),
	});
};

const samplingToSubmissionData = (value: Resource): SubmissionData => {
	const sampling = value as Sampling;
	const now = new Date(Date.now());
	const metaType = createMeta(now);
	return {
		id: {
			value: sampling.id,
			meta: metaType('string'),
		},
		name: {
			value: sampling.name,
			meta: metaType('string'),
		},
		result_time: {
			value: sampling.resultTime,
			meta: metaType('date'),
		},
		geometry: {
			value: {
				type: 'FeatureCollection',
				features: [
					{
						type: 'Feature',
						geometry: sampling.geometry,
					},
				],
			},
			meta: metaType('geoJSON'),
		},
		feature_of_interest: {
			value: sampling.featureOfInterest,
			meta: metaType('string'),
		},
		member_of: {
			value: sampling.memberOf,
			meta: metaType('string'),
		},
		results: {
			value: JSON.stringify(sampling.results),
			meta: metaType('string'),
		},
		comment: {
			value: sampling.comment,
			meta: metaType('string'),
		},
		procedures: {
			value: JSON.stringify(sampling.procedures),
			meta: metaType('string'),
		},
		properties: {
			value: JSON.stringify(sampling.properties),
			meta: metaType('string'),
		},
		so_depth: {
			value: JSON.stringify(sampling.soDepth),
			meta: metaType('string'),
		},
	};
};

export { samplingToSubmissionData, submissionResponseToSampling };

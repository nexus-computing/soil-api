import { ResourceMeta, SurveyStackSubmissionResponseBody } from '../../types';

const propertyFromSubmission = (propertyName: string) => (submission: SurveyStackSubmissionResponseBody): any =>
	submission.data[propertyName].value;

const idFromSubmission = propertyFromSubmission('id');

const surveystackMetaFromSubmission = (submission: SurveyStackSubmissionResponseBody): ResourceMeta => ({
	groupId: submission.meta?.group?.id,
	submissionId: submission._id,
	submittedAt: submission.meta?.dateCreated,
});

export { idFromSubmission, propertyFromSubmission, surveystackMetaFromSubmission };

import { SubmissionMeta } from '../../types';

const createMeta = (dateModified: Date) => (type: string): SubmissionMeta => ({
	dateModified,
	type,
	permissions: ['admin'],
});

const permissionsOnlyMeta = { permissions: ['admin'] };

export { createMeta, permissionsOnlyMeta };

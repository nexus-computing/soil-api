import { ResourceTypes } from '../constants';
import { createLegacyMockFieldResponseSubmission, createMockField } from '../test-utils';
import { createMockResponseSubmission } from '../test-utils/surveystackMockGenerators';
import { fieldToSubmissionData, submissionResponseToField } from './field';

describe('fieldToSubmissionData', () => {
	const mockField = createMockField();
	const submissionData = fieldToSubmissionData(mockField);

	it('maps name value', () => {
		expect(submissionData.name.value).toEqual('mock field name');
	});

	it('maps alternateName value', () => {
		expect(submissionData.alternate_name.value).toEqual('mock alternate name');
	});

	it('maps producerName value', () => {
		expect(submissionData.producer_name.value).toEqual('mock producer name');
	});

	it('maps address value to JSON', () => {
		expect(submissionData.address.value).toEqual(JSON.stringify(mockField.address));
	});

	it('maps contactPoints value to JSON', () => {
		expect(submissionData.contact_points.value).toEqual(JSON.stringify(mockField.contactPoints));
	});

	it('maps areas value to JSON', () => {
		expect(submissionData.areas.value).toEqual(JSON.stringify(mockField.areas));
	});

	describe('meta property', () => {
		it('maps meta value to group question type', () => {
			expect(submissionData.meta.meta.type).toEqual('group');
		});

		it('maps meta.referenceIds to matrix question type', () => {
			expect(submissionData.meta.reference_ids.meta.type).toEqual('matrix');
		});

		it('maps meta.referenceIds to empty array value if none present', () => {
			expect(submissionData.meta.reference_ids.value).toEqual([]);
		});

		it('maps meta.referenceIds to matrix row format', () => {
			const mockFieldWithRefIds = createMockField({
				meta: {
					referenceIds: [{ owner: 'heartland', id: '123' }],
				},
			});
			const submissionDataWithRefIds = fieldToSubmissionData(mockFieldWithRefIds);

			expect(submissionDataWithRefIds.meta.reference_ids.value).toEqual([
				{
					owner: { value: 'heartland' },
					id: { value: '123' },
				},
			]);
		});
	});

	it('has meta.type on each property', () => {
		expect(Object.values(submissionData).every((val) => Boolean(val.meta.type))).toBe(true);
	});

	it('has meta.dateModified on each property', () => {
		expect(Object.values(submissionData).every((val) => Boolean(val.meta.dateModified))).toBe(true);
	});

	it('has meta.permission on each property', () => {
		expect(Object.values(submissionData).every((val) => val.meta.permissions.includes('admin'))).toBe(true);
	});
});

describe('submissionResponseToField', () => {
	const mockLegacySubmissionResponse = createLegacyMockFieldResponseSubmission();
	const mockSubmissionResponse = createMockResponseSubmission({ resourceType: ResourceTypes.Field });
	const legacyField = submissionResponseToField(mockLegacySubmissionResponse);
	const field = submissionResponseToField(mockSubmissionResponse);

	describe('behavior specific to current submission shape', () => {
		it('maps producerName value', () => {
			expect(field.producerName).toEqual(mockSubmissionResponse.data.producer_name.value);
		});
	});

	describe('behavior specific to legacy submission shape', () => {
		it('makes producerName an empty string when producer_name is absent', () => {
			expect(legacyField.producerName).toEqual('');
		});
	});

	describe('common behavior between current and legacy submission shapes', () => {
		[
			{ description: 'current submission shape', field: field, mockSubmissionResponse: mockSubmissionResponse },
			{
				description: 'legacy submission shape',
				field: legacyField,
				mockSubmissionResponse: mockLegacySubmissionResponse,
			},
		].forEach(({ description, field, mockSubmissionResponse }) => {
			describe(description, () => {
				it('maps id value', () => {
					expect(field.id).toEqual(mockSubmissionResponse.data.id.value);
				});

				it('maps name value', () => {
					expect(field.name).toEqual(mockSubmissionResponse.data.name.value);
				});

				it('maps alternateName value', () => {
					expect(field.alternateName).toEqual(mockSubmissionResponse.data.alternate_name.value);
				});

				it('maps address value, parsing JSON', () => {
					expect(field.address).toEqual(JSON.parse(mockSubmissionResponse.data.address.value));
				});

				it('maps contactPoints value, parsing JSON', () => {
					expect(field.contactPoints).toEqual(JSON.parse(mockSubmissionResponse.data.contact_points.value));
				});

				it('maps areas value, parsing JSON', () => {
					expect(field.areas).toEqual(JSON.parse(mockSubmissionResponse.data.areas.value));
				});

				it('maps meta value, converting matrix question type', () => {
					expect(field.meta).toEqual(
						expect.objectContaining({
							referenceIds: [{ owner: 'heartland', id: 'id1' }],
						})
					);
				});

				it('maps group id from response meta object into meta value on the field object', () => {
					const mockResponseWithGroupId = {
						...mockSubmissionResponse,
						meta: {
							...mockSubmissionResponse.meta,
							group: {
								id: '12345',
								path: '/path',
							},
						},
					};

					const actual = submissionResponseToField(mockResponseWithGroupId);

					expect(actual.meta.groupId).toEqual('12345');
				});
			});

			it('maps dateCreated from response meta object into meta.submittedAt', () => {
				expect(field.meta.submittedAt).toEqual(mockSubmissionResponse.meta.dateCreated);
			});
		});
	});
});

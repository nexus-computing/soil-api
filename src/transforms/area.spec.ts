import { ResourceTypes } from '../constants';
import { createMockArea, createMockResponseSubmission } from '../test-utils';
import { areaToSubmissionData, submissionResponseToArea } from './area';

describe('submissionResponseToArea', () => {
	const mockSubmissionResponse = createMockResponseSubmission({ resourceType: ResourceTypes.Area });
	const area = submissionResponseToArea(mockSubmissionResponse);

	it('maps id value', () => {
		expect(area.id).toEqual(mockSubmissionResponse.data.id.value);
	});

	it('maps type value from first Feature in FeatureCollection', () => {
		expect(area.type).toEqual(mockSubmissionResponse.data.geojson.value.features[0].type);
	});

	it('maps geometry value from first Feature in FeatureCollection', () => {
		expect(area.geometry).toEqual(mockSubmissionResponse.data.geojson.value.features[0].geometry);
	});

	it('maps properties value from first Feature in FeatureCollection', () => {
		expect(area.properties).toEqual(mockSubmissionResponse.data.geojson.value.features[0].properties);
	});

	it('maps dateCreated from response meta object into meta.submittedAt', () => {
		expect(area.meta?.submittedAt).toEqual(mockSubmissionResponse.meta.dateCreated);
	});
});

describe('areaToSubmissionData', () => {
	const mockArea = createMockArea({ properties: { featureOfInterest: '1' } });
	const submissionData = areaToSubmissionData(mockArea);

	it('maps area into geoJSON question format', () => {
		expect(submissionData.geojson.value).toHaveProperty('type', 'FeatureCollection');
		expect(submissionData.geojson.value).toHaveProperty('features');
		expect(Array.isArray(submissionData.geojson.value.features)).toBe(true);
		expect(submissionData.geojson.meta).toHaveProperty('type', 'geoJSON');
	});

	it('maps the area into the first Feature of the FeatureCollection', () => {
		expect(submissionData.geojson.value.features[0]).toHaveProperty('type', 'Feature');
		expect(submissionData.geojson.value.features[0]).toHaveProperty('geometry', mockArea.geometry);
		expect(submissionData.geojson.value.features[0]).toHaveProperty('properties', mockArea.properties);
	});

	it('has meta.type on each property', () => {
		expect(Object.values(submissionData).every((val) => Boolean(val.meta.type))).toBe(true);
	});

	it('has meta.dateModified on each property', () => {
		expect(Object.values(submissionData).every((val) => Boolean(val.meta.dateModified))).toBe(true);
	});

	it('has meta.permission on each property', () => {
		expect(Object.values(submissionData).every((val) => val.meta.permissions.includes('admin'))).toBe(true);
	});
});

import { ResourceTypes } from '../constants';
import { createMockLocation, createMockResponseSubmission } from '../test-utils';
import { locationToSubmissionData, submissionResponseToLocation } from './location';

describe('submissionResponseToLocation', () => {
	const mockSubmissionResponse = createMockResponseSubmission({ resourceType: ResourceTypes.Location });
	const location = submissionResponseToLocation(mockSubmissionResponse);

	it('maps id value', () => {
		expect(location.id).toEqual(mockSubmissionResponse.data.id.value);
	});

	it('maps type value from first Feature in FeatureCollection', () => {
		expect(location.type).toEqual(mockSubmissionResponse.data.geojson.value.features[0].type);
	});

	it('maps geometry value from first Feature in FeatureCollection', () => {
		expect(location.geometry).toEqual(mockSubmissionResponse.data.geojson.value.features[0].geometry);
	});

	it('maps properties value from first Feature in FeatureCollection', () => {
		expect(location.properties).toEqual(mockSubmissionResponse.data.geojson.value.features[0].properties);
	});

	it('maps dateCreated from response meta object into meta.submittedAt', () => {
		expect(location.meta?.submittedAt).toEqual(mockSubmissionResponse.meta.dateCreated);
	});
});

describe('locationToSubmissionData', () => {
	const mockLocation = createMockLocation({ properties: { featureOfInterest: '1' } });
	const submissionData = locationToSubmissionData(mockLocation);

	it('maps location into geoJSON question format', () => {
		expect(submissionData.geojson.value).toHaveProperty('type', 'FeatureCollection');
		expect(submissionData.geojson.value).toHaveProperty('features');
		expect(Array.isArray(submissionData.geojson.value.features)).toBe(true);
		expect(submissionData.geojson.meta).toHaveProperty('type', 'geoJSON');
	});

	it('maps the location into the first Feature of the FeatureCollection', () => {
		expect(submissionData.geojson.value.features[0]).toHaveProperty('type', 'Feature');
		expect(submissionData.geojson.value.features[0]).toHaveProperty('geometry', mockLocation.geometry);
		expect(submissionData.geojson.value.features[0]).toHaveProperty('properties', mockLocation.properties);
	});

	it('has meta.type on each property', () => {
		expect(Object.values(submissionData).every((val) => Boolean(val.meta.type))).toBe(true);
	});

	it('has meta.dateModified on each property', () => {
		expect(Object.values(submissionData).every((val) => Boolean(val.meta.dateModified))).toBe(true);
	});

	it('has meta.permission on each property', () => {
		expect(Object.values(submissionData).every((val) => val.meta.permissions.includes('admin'))).toBe(true);
	});
});

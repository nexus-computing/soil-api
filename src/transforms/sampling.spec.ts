import { ResourceTypes } from '../constants';
import { createMockResponseSubmission, createMockSampling } from '../test-utils';
import { samplingToSubmissionData, submissionResponseToSampling } from './sampling';

describe('submissionResponseToSampling', () => {
	const mockSubmissionResponse = createMockResponseSubmission({ resourceType: ResourceTypes.Sampling });
	const sampling = submissionResponseToSampling(mockSubmissionResponse);

	it('maps id value', () => {
		expect(sampling.id).toEqual(mockSubmissionResponse.data.id.value);
	});

	it('maps name value', () => {
		expect(sampling.name).toEqual(mockSubmissionResponse.data.name.value);
	});

	it('maps resultTime value', () => {
		expect(sampling.resultTime).toEqual(mockSubmissionResponse.data.result_time.value);
	});

	it('maps geometry value from first Feature in FeatureCollection', () => {
		expect(sampling.geometry).toEqual(mockSubmissionResponse.data.geometry.value.features[0].geometry);
	});

	it('maps memberOf value', () => {
		expect(sampling.memberOf).toEqual(mockSubmissionResponse.data.member_of.value);
	});

	it('maps results value, parsing JSON', () => {
		expect(sampling.results).toEqual(JSON.parse(mockSubmissionResponse.data.results.value));
	});

	it('maps comment value', () => {
		expect(sampling.comment).toEqual(mockSubmissionResponse.data.comment.value);
	});

	it('maps procedures value, parsing JSON', () => {
		expect(sampling.procedures).toEqual(JSON.parse(mockSubmissionResponse.data.procedures.value));
	});

	it('maps properties value, parsing JSON', () => {
		expect(sampling.properties).toEqual(JSON.parse(mockSubmissionResponse.data.properties.value));
	});

	it('maps soDepth value, parsing JSON', () => {
		expect(sampling.soDepth).toEqual(JSON.parse(mockSubmissionResponse.data.so_depth.value));
	});

	it('maps dateCreated from response meta object into meta.submittedAt', () => {
		expect(sampling.meta?.submittedAt).toEqual(mockSubmissionResponse.meta.dateCreated);
	});
});

describe('samplingToSubmissionData', () => {
	const mockSampling = createMockSampling();
	const submissionData = samplingToSubmissionData(mockSampling);

	it('maps name to string question type', () => {
		expect(submissionData.name.value).toEqual(mockSampling.name);
		expect(submissionData.name.meta.type).toEqual('string');
	});

	it('maps resultTime to date question type', () => {
		expect(submissionData.result_time.value).toEqual(mockSampling.resultTime);
		expect(submissionData.result_time.meta.type).toEqual('date');
	});

	it('maps geometry to geoJSON question type', () => {
		expect(submissionData.geometry.value).toHaveProperty('type', 'FeatureCollection');
		expect(submissionData.geometry.value).toHaveProperty('features');
		expect(Array.isArray(submissionData.geometry.value.features)).toBe(true);
		expect(submissionData.geometry.meta).toHaveProperty('type', 'geoJSON');
	});

	it('maps the geometry value into the first Feature of the FeatureCollection', () => {
		expect(submissionData.geometry.value.features[0]).toHaveProperty('type', 'Feature');
		expect(submissionData.geometry.value.features[0]).toHaveProperty('geometry', mockSampling.geometry);
	});

	it('maps featureOfInterest to string question type', () => {
		expect(submissionData.feature_of_interest.value).toEqual(mockSampling.featureOfInterest);
		expect(submissionData.feature_of_interest.meta.type).toEqual('string');
	});

	it('maps memberOf to string question type', () => {
		expect(submissionData.member_of.value).toEqual(mockSampling.memberOf);
		expect(submissionData.member_of.meta.type).toEqual('string');
	});

	it('maps results to string question type, stringifying JSON', () => {
		expect(submissionData.results.value).toEqual(JSON.stringify(mockSampling.results));
		expect(submissionData.results.meta.type).toEqual('string');
	});

	it('maps comment to string question type', () => {
		expect(submissionData.comment.value).toEqual(mockSampling.comment);
		expect(submissionData.comment.meta.type).toEqual('string');
	});

	it('maps procedures to string question type, stringifying JSON', () => {
		expect(submissionData.procedures.value).toEqual(JSON.stringify(mockSampling.procedures));
		expect(submissionData.procedures.meta.type).toEqual('string');
	});

	it('maps properties to string question type, stringifying JSON', () => {
		expect(submissionData.properties.value).toEqual(JSON.stringify(mockSampling.properties));
		expect(submissionData.properties.meta.type).toEqual('string');
	});

	it('maps soDepth to string question type, stringifying JSON', () => {
		expect(submissionData.so_depth.value).toEqual(JSON.stringify(mockSampling.soDepth));
		expect(submissionData.so_depth.meta.type).toEqual('string');
	});

	it('has meta.type on each property', () => {
		expect(Object.values(submissionData).every((val) => Boolean(val.meta.type))).toBe(true);
	});

	it('has meta.dateModified on each property', () => {
		expect(Object.values(submissionData).every((val) => Boolean(val.meta.dateModified))).toBe(true);
	});

	it('has meta.permission on each property', () => {
		expect(Object.values(submissionData).every((val) => val.meta.permissions.includes('admin'))).toBe(true);
	});
});

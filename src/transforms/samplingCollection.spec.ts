import { ResourceTypes } from '../constants';
import { createMockResponseSubmission, createMockSamplingCollection } from '../test-utils';
import { samplingCollectionToSubmissionData, submissionResponseToSamplingCollection } from './samplingCollection';

describe('submissionResponseToSamplingCollection', () => {
	const mockSubmissionResponse = createMockResponseSubmission({ resourceType: ResourceTypes.SamplingCollection });
	const samplingCollection = submissionResponseToSamplingCollection(mockSubmissionResponse);

	it('maps id value', () => {
		expect(samplingCollection.id).toEqual(mockSubmissionResponse.data.id.value);
	});

	it('maps name value', () => {
		expect(samplingCollection.name).toEqual(mockSubmissionResponse.data.name.value);
	});

	it('maps comment value', () => {
		expect(samplingCollection.comment).toEqual(mockSubmissionResponse.data.comment.value);
	});

	it('maps creator value', () => {
		expect(samplingCollection.creator).toEqual(mockSubmissionResponse.data.creator.value);
	});

	it('maps featureOfInterest value', () => {
		expect(samplingCollection.featureOfInterest).toEqual(mockSubmissionResponse.data.feature_of_interest.value);
	});

	it('maps participants value, parsing JSON', () => {
		expect(samplingCollection.participants).toEqual(JSON.parse(mockSubmissionResponse.data.participants.value));
	});

	it('maps object value', () => {
		expect(samplingCollection.object).toEqual(mockSubmissionResponse.data.object.value);
	});

	it('maps members value, parsing JSON', () => {
		expect(samplingCollection.members).toEqual(JSON.parse(mockSubmissionResponse.data.members.value));
	});

	it('maps soDepth value, parsing JSON', () => {
		expect(samplingCollection.soDepth).toEqual(JSON.parse(mockSubmissionResponse.data.so_depth.value));
	});

	it('maps sampleDepths value, parsing JSON', () => {
		expect(samplingCollection.sampleDepths).toEqual(JSON.parse(mockSubmissionResponse.data.sample_depths.value));
	});

	it('maps dateCreated value', () => {
		expect(samplingCollection.dateCreated).toEqual(mockSubmissionResponse.data.date_created.value);
	});

	it('maps dateModified value', () => {
		expect(samplingCollection.dateModified).toEqual(mockSubmissionResponse.data.date_modified.value);
	});

	it('maps meta value, converting matrix question type', () => {
		expect(samplingCollection.meta).toEqual(
			expect.objectContaining({
				referenceIds: [{ owner: 'soilstack-draft', id: 'id1' }],
			})
		);
	});
});

describe('sampleToSubmissionData', () => {
	const mockSamplingCollection = createMockSamplingCollection();
	const submissionData = samplingCollectionToSubmissionData(mockSamplingCollection);

	it('maps name to string question type', () => {
		expect(submissionData.name.value).toEqual(mockSamplingCollection.name);
		expect(submissionData.name.meta.type).toEqual('string');
	});

	it('maps comment to string question type', () => {
		expect(submissionData.comment.value).toEqual(mockSamplingCollection.comment);
		expect(submissionData.comment.meta.type).toEqual('string');
	});

	it('maps creator to string question type', () => {
		expect(submissionData.creator.value).toEqual(mockSamplingCollection.creator);
		expect(submissionData.creator.meta.type).toEqual('string');
	});

	it('maps featureOfInterest to string question type', () => {
		expect(submissionData.feature_of_interest.value).toEqual(mockSamplingCollection.featureOfInterest);
		expect(submissionData.feature_of_interest.meta.type).toEqual('string');
	});

	it('maps participants to string question type, stringifying JSON', () => {
		expect(submissionData.participants.value).toEqual(JSON.stringify(mockSamplingCollection.participants));
		expect(submissionData.participants.meta.type).toEqual('string');
	});

	it('maps object to string question type', () => {
		expect(submissionData.object.value).toEqual(mockSamplingCollection.object);
		expect(submissionData.object.meta.type).toEqual('string');
	});

	it('maps members to string question type, stringifying JSON', () => {
		expect(submissionData.members.value).toEqual(JSON.stringify(mockSamplingCollection.members));
		expect(submissionData.members.meta.type).toEqual('string');
	});

	it('maps soDepth to string question type, stringifying JSON', () => {
		expect(submissionData.so_depth.value).toEqual(JSON.stringify(mockSamplingCollection.soDepth));
		expect(submissionData.so_depth.meta.type).toEqual('string');
	});

	it('maps sampleDepths to string question type, stringifying JSON', () => {
		expect(submissionData.sample_depths.value).toEqual(JSON.stringify(mockSamplingCollection.sampleDepths));
		expect(submissionData.sample_depths.meta.type).toEqual('string');
	});

	it('maps dateCreated to date question type', () => {
		expect(submissionData.date_created.value).toEqual(mockSamplingCollection.dateCreated);
		expect(submissionData.date_created.meta.type).toEqual('date');
	});

	it('maps dateModified to date question type', () => {
		expect(submissionData.date_modified.value).toEqual(mockSamplingCollection.dateModified);
		expect(submissionData.date_modified.meta.type).toEqual('date');
	});

	describe('meta property', () => {
		it('maps meta value to group question type', () => {
			expect(submissionData.meta.meta.type).toEqual('group');
		});

		it('maps meta.referenceIds to matrix question type', () => {
			expect(submissionData.meta.reference_ids.meta.type).toEqual('matrix');
		});

		it('maps meta.referenceIds to empty array value if none present', () => {
			expect(submissionData.meta.reference_ids.value).toEqual([]);
		});

		it('maps meta.referenceIds to matrix row format', () => {
			const mockSamplingCollectionWithRefIds = createMockSamplingCollection({
				meta: {
					referenceIds: [{ owner: 'soilstack-draft', id: '123' }],
				},
			});
			const submissionDataWithRefIds = samplingCollectionToSubmissionData(mockSamplingCollectionWithRefIds);

			expect(submissionDataWithRefIds.meta.reference_ids.value).toEqual([
				{
					owner: { value: 'soilstack-draft' },
					id: { value: '123' },
				},
			]);
		});
	});

	it('has meta.type on each property', () => {
		expect(Object.values(submissionData).every((val) => Boolean(val.meta.type))).toBe(true);
	});

	it('has meta.dateModified on each property', () => {
		expect(Object.values(submissionData).every((val) => Boolean(val.meta.dateModified))).toBe(true);
	});

	it('has meta.permission on each property', () => {
		expect(Object.values(submissionData).every((val) => val.meta.permissions.includes('admin'))).toBe(true);
	});
});

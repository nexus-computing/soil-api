import { ResourceTypes } from '../constants';
import { createMockLocationCollection, createMockResponseSubmission } from '../test-utils';
import { locationCollectionToSubmissionData, submissionResponseToLocationCollection } from './locationCollection';

describe('submissionResponseToLocationCollection', () => {
	const mockSubmissionResponse = createMockResponseSubmission({ resourceType: ResourceTypes.LocationCollection });
	const locationCollection = submissionResponseToLocationCollection(mockSubmissionResponse);

	it('maps id value', () => {
		expect(locationCollection.id).toEqual(mockSubmissionResponse.data.id.value);
	});

	it('maps object value', () => {
		expect(locationCollection.object).toEqual(mockSubmissionResponse.data.object.value);
	});

	it('maps resultOf value', () => {
		expect(locationCollection.resultOf).toEqual(mockSubmissionResponse.data.result_of.value);
	});

	it('maps featureOfInterest value', () => {
		expect(locationCollection.featureOfInterest).toEqual(mockSubmissionResponse.data.feature_of_interest.value);
	});

	it('maps features value, parsing JSON', () => {
		expect(locationCollection.features).toEqual(JSON.parse(mockSubmissionResponse.data.features.value));
	});

	it('maps dateCreated from response meta object into meta.submittedAt', () => {
		expect(locationCollection.meta?.submittedAt).toEqual(mockSubmissionResponse.meta.dateCreated);
	});
});

describe('locationCollectionToSubmissionData', () => {
	const mockLocationCollection = createMockLocationCollection();
	const submissionData = locationCollectionToSubmissionData(mockLocationCollection);

	it('maps object to string question type', () => {
		expect(submissionData.object.value).toEqual(mockLocationCollection.object);
		expect(submissionData.object.meta.type).toEqual('string');
	});

	it('maps resultOf to string question type', () => {
		expect(submissionData.result_of.value).toEqual(mockLocationCollection.resultOf);
		expect(submissionData.result_of.meta.type).toEqual('string');
	});

	it('maps featureOfInterest to string question type', () => {
		expect(submissionData.feature_of_interest.value).toEqual(mockLocationCollection.featureOfInterest);
		expect(submissionData.feature_of_interest.meta.type).toEqual('string');
	});

	it('maps features to string question type, stringifying JSON', () => {
		expect(submissionData.features.value).toEqual(JSON.stringify(mockLocationCollection.features));
		expect(submissionData.features.meta.type).toEqual('string');
	});

	it('has meta.type on each property', () => {
		expect(Object.values(submissionData).every((val) => Boolean(val.meta.type))).toBe(true);
	});

	it('has meta.dateModified on each property', () => {
		expect(Object.values(submissionData).every((val) => Boolean(val.meta.dateModified))).toBe(true);
	});

	it('has meta.permission on each property', () => {
		expect(Object.values(submissionData).every((val) => val.meta.permissions.includes('admin'))).toBe(true);
	});
});

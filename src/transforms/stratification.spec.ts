import { ResourceTypes } from '../constants';
import { createMockResponseSubmission, createMockStratification } from '../test-utils';
import { stratificationToSubmissionData, submissionResponseToStratification } from './stratification';

describe('submissionResponseToStratification', () => {
	const mockSubmissionResponse = createMockResponseSubmission({ resourceType: ResourceTypes.Stratification });
	const stratification = submissionResponseToStratification(mockSubmissionResponse);

	it('maps id value', () => {
		expect(stratification.id).toEqual(mockSubmissionResponse.data.id.value);
	});

	it('maps name value', () => {
		expect(stratification.name).toEqual(mockSubmissionResponse.data.name.value);
	});

	it('maps agent value', () => {
		expect(stratification.agent).toEqual(mockSubmissionResponse.data.agent.value);
	});

	it('maps dateCreated value', () => {
		expect(stratification.dateCreated).toEqual(mockSubmissionResponse.data.date_created.value);
	});

	it('maps provider value', () => {
		expect(stratification.provider).toEqual(mockSubmissionResponse.data.provider.value);
	});

	it('maps object value', () => {
		expect(stratification.object).toEqual(mockSubmissionResponse.data.object.value);
	});

	it('maps algorithm.name value', () => {
		expect(stratification.algorithm.name).toEqual(mockSubmissionResponse.data.algorithm.name.value);
	});

	it('maps algorithm.alternateName value', () => {
		expect(stratification.algorithm.alternateName).toEqual(mockSubmissionResponse.data.algorithm.alternate_name.value);
	});

	it('maps algorithm.codeRepository value', () => {
		expect(stratification.algorithm.codeRepository).toEqual(
			mockSubmissionResponse.data.algorithm.code_repository.value
		);
	});

	it('maps algorithm.version value', () => {
		expect(stratification.algorithm.version).toEqual(mockSubmissionResponse.data.algorithm.version.value);
	});

	it('maps algorithm.doi value', () => {
		expect(stratification.algorithm.doi).toEqual(mockSubmissionResponse.data.algorithm.doi.value);
	});

	it('maps input matrix, parsing JSON value', () => {
		expect(stratification.input).toEqual([
			{
				name: mockSubmissionResponse.data.input.value[0].name.value,
				value: JSON.parse(mockSubmissionResponse.data.input.value[0].value.value),
			},
		]);
	});

	it('maps result matrix, parsing JSON value', () => {
		expect(stratification.result).toEqual([
			{
				name: mockSubmissionResponse.data.result.value[0].name.value,
				value: JSON.parse(mockSubmissionResponse.data.result.value[0].value.value),
			},
		]);
	});

	it('maps dateCreated from response meta object into meta.submittedAt', () => {
		expect(stratification.meta?.submittedAt).toEqual(mockSubmissionResponse.meta.dateCreated);
	});
});

describe('stratificationToSubmissionData', () => {
	const mockStratification = createMockStratification();
	const submissionData = stratificationToSubmissionData(mockStratification);

	it('maps name to string question type', () => {
		expect(submissionData.name.value).toEqual(mockStratification.name);
		expect(submissionData.name.meta.type).toEqual('string');
	});

	it('maps agent to string question type', () => {
		expect(submissionData.agent.value).toEqual(mockStratification.agent);
		expect(submissionData.agent.meta.type).toEqual('string');
	});

	it('maps dateCreated to date question type', () => {
		expect(submissionData.date_created.value).toEqual(mockStratification.dateCreated);
		expect(submissionData.date_created.meta.type).toEqual('date');
	});

	it('maps provider to string question type', () => {
		expect(submissionData.provider.value).toEqual(mockStratification.provider);
		expect(submissionData.provider.meta.type).toEqual('string');
	});

	it('maps object to string question type', () => {
		expect(submissionData.object.value).toEqual(mockStratification.object);
		expect(submissionData.object.meta.type).toEqual('string');
	});

	describe('mapping algorithm value', () => {
		it('maps algorithm to group question type', () => {
			expect(submissionData.algorithm.meta.type).toEqual('group');
		});

		it('maps algorithm name to string question type', () => {
			expect(submissionData.algorithm.name.value).toEqual(mockStratification.algorithm.name);
			expect(submissionData.algorithm.name.meta.type).toEqual('string');
		});

		it('maps algorithm alternateName to string question type', () => {
			expect(submissionData.algorithm.alternate_name.value).toEqual(mockStratification.algorithm.alternateName);
			expect(submissionData.algorithm.alternate_name.meta.type).toEqual('string');
		});

		it('maps algorithm codeRepository to string question type', () => {
			expect(submissionData.algorithm.code_repository.value).toEqual(mockStratification.algorithm.codeRepository);
			expect(submissionData.algorithm.code_repository.meta.type).toEqual('string');
		});

		it('maps algorithm version to string question type', () => {
			expect(submissionData.algorithm.version.value).toEqual(mockStratification.algorithm.version);
			expect(submissionData.algorithm.version.meta.type).toEqual('string');
		});

		it('maps algorithm doi to string question type', () => {
			expect(submissionData.algorithm.doi.value).toEqual(mockStratification.algorithm.doi);
			expect(submissionData.algorithm.doi.meta.type).toEqual('string');
		});
	});

	describe('mapping input value', () => {
		it('maps input value to matrix question type', () => {
			expect(submissionData.input.meta.type).toEqual('matrix');
		});

		it('maps each item in input array to a row in the matrix', () => {
			expect(submissionData.input.value.length).toEqual(mockStratification.input.length);
		});

		it('maps the name from input object in each row', () => {
			expect(submissionData.input.value[0].name).toEqual({
				value: mockStratification.input[0].name,
				meta: { permissions: ['admin'] },
			});
		});

		it('maps the value from input object in each row, stringifying it to JSON', () => {
			expect(submissionData.input.value[0].value).toEqual({
				value: JSON.stringify(mockStratification.input[0].value),
				meta: { permissions: ['admin'] },
			});
		});
	});

	it('has meta.type on each property', () => {
		expect(Object.values(submissionData).every((val) => Boolean(val.meta.type))).toBe(true);
	});

	it('has meta.dateModified on each property', () => {
		expect(Object.values(submissionData).every((val) => Boolean(val.meta.dateModified))).toBe(true);
	});

	it('has meta.permission on each property', () => {
		expect(Object.values(submissionData).every((val) => val.meta.permissions.includes('admin'))).toBe(true);
	});
});

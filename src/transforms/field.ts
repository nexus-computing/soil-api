import { createAddress, createContactPoint, createField } from '../resources';
import { Field, Resource, SubmissionData, SurveyStackSubmissionResponseBody } from '../types';
import { createMeta } from './common/resourceToSubmission';
import { idFromSubmission, surveystackMetaFromSubmission } from './common/submissionToResource';

const submissionResponseToField = (submission: SurveyStackSubmissionResponseBody): Field => {
	return createField({
		id: idFromSubmission(submission),
		name: submission.data.name.value,
		alternateName: submission.data.alternate_name.value,
		producerName: submission?.data?.producer_name?.value ?? '',
		address: createAddress(JSON.parse(submission.data.address.value)),
		contactPoints: JSON.parse(submission.data.contact_points.value).map(createContactPoint),
		areas: JSON.parse(submission.data.areas.value),
		meta: {
			referenceIds: submission.data.meta.reference_ids.value?.map(
				(refId: { owner: { value: string }; id: { value: string } }) => ({
					owner: refId.owner.value,
					id: refId.id.value,
				})
			),
			...surveystackMetaFromSubmission(submission),
		},
	});
};

const fieldToSubmissionData = (value: Resource): SubmissionData => {
	const field = value as Field;
	const now = new Date(Date.now());
	const metaType = createMeta(now);
	return {
		id: {
			value: field.id,
			meta: metaType('string'),
		},
		name: {
			value: field.name,
			meta: metaType('string'),
		},
		alternate_name: {
			value: field.alternateName,
			meta: metaType('string'),
		},
		producer_name: {
			value: field.producerName,
			meta: metaType('string'),
		},
		address: {
			value: JSON.stringify(field.address),
			meta: metaType('string'),
		},
		contact_points: {
			value: JSON.stringify(field.contactPoints),
			meta: metaType('string'),
		},
		areas: {
			value: JSON.stringify(field.areas),
			meta: metaType('string'),
		},
		meta: {
			reference_ids: {
				meta: metaType('matrix'),
				value:
					field.meta.referenceIds?.map((refId) => ({
						owner: { value: refId.owner },
						id: { value: refId.id },
					})) ?? [],
			},
			meta: metaType('group'),
		},
	};
};

export { fieldToSubmissionData, submissionResponseToField };

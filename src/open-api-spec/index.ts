import { Application, Request, Response } from 'express';
import * as OpenApiValidator from 'express-openapi-validator';
import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import openapiFilter from 'openapi-filter';
import swaggerUi, { SwaggerUiOptions } from 'swagger-ui-express';
import { INTERNAL_API_DOCS_URL } from '../constants';
import { paths } from '../routes';
import schemas from './schemas';
import { removeUnusedTags } from './utils';

type ApiTypes = 'all' | 'public' | 'internal';

const urls: { [key in ApiTypes]: string } = {
	all: `${INTERNAL_API_DOCS_URL}/all.json`,
	internal: `${INTERNAL_API_DOCS_URL}/internal.json`,
	public: '/open-api-spec.json',
};

const openApiSpecDocument = {
	openapi: '3.0.1',
	info: {
		title: 'Soilstack API',
		version: '0.0.1',
		description: `
[Open API Schema JSON (all)](${urls.all})

[Open API Schema JSON (internal only)](${urls.internal})

[Open API Schema JSON (public only)](${urls.public})

[Polling for newly created resources](https://gitlab.com/our-sci/software/soil-api/-/wikis/Polling-for-newly-created-resources)
		`,
		'x-public-description': `
[Open API Schema JSON](${urls.public})

[Polling for newly created resources](https://gitlab.com/our-sci/software/soil-api/-/wikis/Polling-for-newly-created-resources)
		`,
	},
	paths,
	components: {
		securitySchemes: {
			Authorization: {
				'x-internal': true,
				type: 'apiKey',
				in: 'header',
				name: 'X-Authorization',
				description:
					'Your username (email) and authentication token, separated by a single space. Ex. "name@example.com abcd1234"',
			},
			'X-Super-Authorization': {
				'x-internal': true,
				type: 'apiKey',
				in: 'header',
				name: 'X-Super-Authorization',
				description: 'Super admin password. Ex. "abcd1234"',
			},
		},
		'x-public-securitySchemes': {
			Authorization: {
				type: 'apiKey',
				in: 'header',
				name: 'X-Authorization',
				description:
					'Your username (email) and authentication token, separated by a single space. Ex. "name@example.com abcd1234"',
			},
		},
		schemas,
	},
	tags: [
		{ name: 'Fields', description: 'Information about a field like address, name, area and contact info.' },
		{ name: 'Areas', description: 'geoJSON polygons or multipolygons that represent the boundaries of a field.' },
		{
			name: 'Stratifications',
			description: 'Information about the inputs and outputs of a stratification performed on an area.',
		},
		{ name: 'Location Collections', description: 'A list of locations outputted by a stratification.' },
		{ name: 'Locations', description: 'geoJSON points.' },
		{ name: 'Sampling Collections', description: 'A list of samplings and some metadata.' },
		{ name: 'Samplings', description: 'A list of samples.' },
		{ name: 'Samples', description: 'Individual samples taken in the field.' },
	],
} as OpenAPIV3.Document;

const commonFilterOptions = {
	flags: ['x-internal'],
	info: true,
	valid: true,
	strip: true,
};
const internalOpenApiSpecDocument: OpenAPIV3.Document = removeUnusedTags(
	openapiFilter.filter(openApiSpecDocument, { ...commonFilterOptions, inverse: true })
);
const publicOpenApiSpecDocument: OpenAPIV3.Document = removeUnusedTags(
	openapiFilter.filter(openApiSpecDocument, { ...commonFilterOptions, overrides: ['x-public-'] })
);

const setupOpenApiSpec = (app: Application): void => {
	// serve up the open api spec as json
	app.get(urls.public, (req: Request, res: Response) => res.json(publicOpenApiSpecDocument));
	app.get(urls.all, (req: Request, res: Response) => res.json(openApiSpecDocument));
	app.get(urls.internal, (req: Request, res: Response) => res.json(internalOpenApiSpecDocument));

	// register swagger ui prior to open api validator
	// public api docs
	app.use(
		'/api-docs',
		swaggerUi.serveFiles(publicOpenApiSpecDocument),
		swaggerUi.setup(undefined, {
			customSiteTitle: 'Soilstack API',
			customCss: `
				.swagger-ui .topbar { display: none }
				.swagger-ui .info { margin: 24px 0; }
				.swagger-ui .scheme-container { background: inherit; padding: 12px 0; }
			`,
		})
	);

	// internal api docs
	const options: SwaggerUiOptions = {
		explorer: true,
		customSiteTitle: 'Soilstack API (internal)',
		customCss: `
		.swagger-ui .topbar { position: absolute; left: 50%; top: 20px; width: 100%; max-width: 1460px; padding: 0 10px; transform: translateX(-50%); background: none; display: flex; justify-content: end; }
		.swagger-ui .topbar .topbar-wrapper { display: flex; justify-content: end; }
		.swagger-ui .topbar form { max-width: 150px; }
		.swagger-ui .topbar .link, .swagger-ui .topbar .select-label span { display: none; }
		.swagger-ui .topbar form select { height: 36px; border-color: #49cc90 !important; }
		.swagger-ui .info { margin: 24px 0; }
		.swagger-ui .info .link  { display: none; }
		.swagger-ui .scheme-container { background: inherit; padding: 12px 0; }
	`,
		swaggerOptions: {
			urls: [
				{ url: urls.all, name: 'All' },
				{ url: urls.internal, name: 'Internal' },
				{ url: urls.public, name: 'Public' },
			],
		},
	};
	app.use(INTERNAL_API_DOCS_URL, swaggerUi.serveFiles(undefined, options), swaggerUi.setup(undefined, options));

	// register open api validator
	app.use(
		OpenApiValidator.middleware({
			apiSpec: openApiSpecDocument,
			validateRequests: true,
			validateResponses: ['development', 'test'].includes(String(process.env.NODE_ENV).trim()),
		})
	);
};

export { internalOpenApiSpecDocument, openApiSpecDocument, publicOpenApiSpecDocument };

export default setupOpenApiSpec;

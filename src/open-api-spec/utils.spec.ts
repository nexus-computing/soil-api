import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import { removeUnusedTags } from './utils';

describe('removeUnusedTags', () => {
	it('removes tags from the top level of a spec document if they are not used by any endpoints', () => {
		const specDocWithExtraTags: OpenAPIV3.Document = {
			openapi: '3.0.1',
			info: { title: 'title', version: '0.0.1' },
			tags: [{ name: 'a' }, { name: 'b' }, { name: 'c' }],
			paths: {
				'/endpointA': {
					get: {
						tags: ['a'],
					},
				},
			},
		};

		const actual = removeUnusedTags(specDocWithExtraTags);

		expect(actual.tags).toHaveLength(1);
		expect(actual.tags).toContainEqual({ name: 'a' });
	});

	it('handles endpoints with multiple tags', () => {
		const specDocWithExtraTags: OpenAPIV3.Document = {
			openapi: '3.0.1',
			info: { title: 'title', version: '0.0.1' },
			tags: [{ name: 'a' }, { name: 'b' }, { name: 'c' }],
			paths: {
				'/endpointAB': {
					get: {
						tags: ['a', 'b'],
					},
				},
			},
		};

		const actual = removeUnusedTags(specDocWithExtraTags);

		expect(actual.tags).toHaveLength(2);
		expect(actual.tags).toContainEqual({ name: 'a' });
		expect(actual.tags).toContainEqual({ name: 'b' });
	});

	it('handles multiple endpoints with tags', () => {
		const specDocWithExtraTags: OpenAPIV3.Document = {
			openapi: '3.0.1',
			info: { title: 'title', version: '0.0.1' },
			tags: [{ name: 'a' }, { name: 'b' }, { name: 'c' }],
			paths: {
				'/endpointA': {
					get: {
						tags: ['a'],
					},
				},
				'/endpointB': {
					get: {
						tags: ['b'],
					},
				},
			},
		};

		const actual = removeUnusedTags(specDocWithExtraTags);

		expect(actual.tags).toHaveLength(2);
		expect(actual.tags).toContainEqual({ name: 'a' });
		expect(actual.tags).toContainEqual({ name: 'b' });
	});
});

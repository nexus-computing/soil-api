import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

const operationTypes = ['get', 'put', 'post', 'delete', 'options', 'head', 'patch', 'trace'];

const getTagsFromPath = (path: OpenAPIV3.PathItemObject) =>
	Object.entries(path)
		.filter(([key]) => operationTypes.includes(key))
		.flatMap(([_, operation]: [string, OpenAPIV3.OperationObject]) => operation.tags ?? []);

const removeUnusedTags = (specDocument: OpenAPIV3.Document): OpenAPIV3.Document => {
	const usedTags = Object.values(specDocument.paths).flatMap(getTagsFromPath);
	return {
		...specDocument,
		tags: specDocument.tags?.filter((tag) => usedTags.includes(tag.name)),
	};
};

export { removeUnusedTags };

import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

const SamplingCollection: OpenAPIV3.SchemaObject = {
	type: 'object',
	required: ['soDepth', 'sampleDepths', 'dateCreated', 'dateModified'],
	additionalProperties: false,
	properties: {
		name: { type: 'string' },
		comment: { type: 'string' },
		creator: { type: 'string' },
		featureOfInterest: {
			type: 'string',
			description: 'The Field (id) the sampling collection was created in.',
		},
		participants: { type: 'array', items: { type: 'string' } },
		object: {
			type: 'string',
			description: 'The Location Collection (id) the sampling collection was derived from.',
		},
		members: {
			type: 'array',
			items: { type: 'string' },
			description: 'The Samplings (ids) belonging to the sampling collection.',
		},
		soDepth: { $ref: '#/components/schemas/SoDepth' },
		sampleDepths: {
			type: 'array',
			items: { $ref: '#/components/schemas/SoDepth' },
		},
		dateCreated: { type: 'string', format: 'date-time' },
		dateModified: { type: 'string', format: 'date-time' },
		meta: { $ref: '#/components/schemas/ResourceMeta' },
	},
};

const ResponseSamplingCollection: OpenAPIV3.SchemaObject = {
	...SamplingCollection,
	required: [
		'id',
		'name',
		'comment',
		'creator',
		'featureOfInterest',
		'participants',
		'object',
		'members',
		'soDepth',
		'sampleDepths',
		'dateCreated',
		'dateModified',
		'meta',
	],
	properties: {
		...SamplingCollection.properties,
		id: { type: 'string' },
		meta: { $ref: '#/components/schemas/ResponseResourceMeta' },
	},
};

const PopulatedSamplingCollection: OpenAPIV3.SchemaObject = {
	...SamplingCollection,
	properties: {
		...SamplingCollection.properties,
		members: {
			type: 'array',
			items: { $ref: '#/components/schemas/PopulatedSampling' },
			description: 'The Samplings belonging to the sampling collection.',
		},
	},
};

export { PopulatedSamplingCollection, ResponseSamplingCollection, SamplingCollection };

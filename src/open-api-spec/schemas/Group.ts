import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

const Group: OpenAPIV3.SchemaObject = {
	type: 'object',
	required: ['id', 'path'],
	additionalProperties: false,
	properties: {
		id: { type: 'string' },
		name: { type: 'string' },
		path: { type: 'string' },
	},
};

export { Group };

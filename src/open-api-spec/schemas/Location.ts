import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

const Location: OpenAPIV3.SchemaObject = {
	type: 'object',
	required: ['geometry'],
	additionalProperties: false,
	properties: {
		type: { type: 'string', enum: ['Feature'], default: 'Feature' },
		geometry: { $ref: '#/components/schemas/Point' },
		properties: { type: 'object' },
	},
};

const ResponseLocation: OpenAPIV3.SchemaObject = {
	...Location,
	required: ['id', 'meta'],
	properties: {
		id: { type: 'string' },
		...Location.properties,
		meta: { $ref: '#/components/schemas/SurveystackOnlyResponseResourceMeta' },
	},
};

export { Location, ResponseLocation };

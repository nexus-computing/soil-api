import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

const Error: OpenAPIV3.SchemaObject = {
	type: 'object',
	properties: {
		message: {
			type: 'string',
		},
	},
};

export default Error;

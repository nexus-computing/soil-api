import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

const LocationCollection: OpenAPIV3.SchemaObject = {
	type: 'object',
	required: ['features', 'featureOfInterest', 'object'],
	additionalProperties: false,
	properties: {
		object: {
			type: 'string',
			description: 'The Area (id) the location collection is in.',
		},
		resultOf: {
			type: 'string',
			description: 'The Stratification (id) the location collection belongs to.',
		},
		featureOfInterest: {
			type: 'string',
			description: 'The Field (id) that the location collection is in.',
		},
		features: {
			type: 'array',
			items: { type: 'string' },
			description: 'The Locations (ids) belonging to the location collection.',
		},
	},
};

const ResponseLocationCollection: OpenAPIV3.SchemaObject = {
	...LocationCollection,
	required: ['id', 'meta'],
	properties: {
		id: { type: 'string' },
		...LocationCollection.properties,
		meta: { $ref: '#/components/schemas/SurveystackOnlyResponseResourceMeta' },
	},
};

const PopulatedLocationCollection: OpenAPIV3.SchemaObject = {
	...LocationCollection,
	properties: {
		...LocationCollection.properties,
		features: {
			...LocationCollection.properties?.features,
			items: { $ref: '#/components/schemas/Location' },
			description: 'The Locations belonging to the location collection.',
		} as OpenAPIV3.SchemaObject,
	},
};

export { LocationCollection, PopulatedLocationCollection, ResponseLocationCollection };

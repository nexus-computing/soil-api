import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';
import Address from './Address';
import { Algorithm } from './Algorithm';
import { Area, EditableArea, ResponseArea } from './Area';
import { AreaProperties, EditableAreaProperties } from './AreaProperties';
import ContactPoint from './ContactPoint';
import Error from './Error';
import { EditableField, Field, PopulatedField, ResponseField } from './Field';
import { Group } from './Group';
import { Location, ResponseLocation } from './Location';
import { LocationCollection, PopulatedLocationCollection, ResponseLocationCollection } from './LocationCollection';
import MultiPolygon from './MultiPolygon';
import { Point } from './Point';
import Polygon from './Polygon';
import Position from './Position';
import { ResourceMeta, ResponseResourceMeta, SurveystackOnlyResponseResourceMeta } from './ResourceMeta';
import { EditableSample, ResponseSample, Sample } from './Sample';
import { PopulatedSampling, ResponseSampling, Sampling } from './Sampling';
import { PopulatedSamplingCollection, ResponseSamplingCollection, SamplingCollection } from './SamplingCollection';
import { SoDepth, SoDepthValue } from './SoDepth';
import { ResponseStratification, Stratification } from './Stratification';
import { StratificationInput } from './StratificationInput';
import { StratificationResult } from './StratificationResult';

const schemas: { [key: string]: OpenAPIV3.SchemaObject } = {
	Address,
	ContactPoint,
	Field,
	ResponseField,
	PopulatedField,
	EditableField,
	AreaProperties,
	EditableAreaProperties,
	Area,
	ResponseArea,
	EditableArea,
	Polygon,
	MultiPolygon,
	Position,
	Point,
	ResourceMeta,
	ResponseResourceMeta,
	SurveystackOnlyResponseResourceMeta,
	Algorithm,
	Stratification,
	StratificationInput,
	StratificationResult,
	ResponseStratification,
	LocationCollection,
	ResponseLocationCollection,
	PopulatedLocationCollection,
	Location,
	ResponseLocation,
	SoDepthValue,
	SoDepth,
	Sample,
	ResponseSample,
	EditableSample,
	Sampling,
	ResponseSampling,
	PopulatedSampling,
	SamplingCollection,
	ResponseSamplingCollection,
	PopulatedSamplingCollection,
	Group,
	Error,
};

export default schemas;

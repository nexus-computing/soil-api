import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

const Area: OpenAPIV3.SchemaObject = {
	type: 'object',
	required: ['geometry'],
	additionalProperties: false,
	properties: {
		type: { type: 'string', enum: ['Feature'], default: 'Feature' },
		geometry: {
			type: 'object',
			oneOf: [{ $ref: '#/components/schemas/Polygon' }, { $ref: '#/components/schemas/MultiPolygon' }],
		},
		properties: { $ref: '#/components/schemas/AreaProperties' },
	},
};

const ResponseArea: OpenAPIV3.SchemaObject = {
	...Area,
	required: ['id', 'type', 'geometry', 'properties', 'meta'],
	properties: {
		id: { type: 'string' },
		...Area.properties,
		meta: { $ref: '#/components/schemas/SurveystackOnlyResponseResourceMeta' },
	},
};

const EditableArea: OpenAPIV3.SchemaObject = {
	type: 'object',
	additionalProperties: false,
	properties: {
		...Area.properties,
		properties: { $ref: '#/components/schemas/EditableAreaProperties' },
	},
};

export { Area, EditableArea, ResponseArea };

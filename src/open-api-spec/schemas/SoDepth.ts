import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

const SoDepthValue: OpenAPIV3.SchemaObject = {
	type: 'object',
	required: ['value', 'unit'],
	additionalProperties: false,
	properties: {
		value: { type: 'number' },
		unit: { type: 'string', enum: ['unit:CM'] },
	},
};

const SoDepth: OpenAPIV3.SchemaObject = {
	type: 'object',
	required: ['minValue', 'maxValue'],
	additionalProperties: false,
	properties: {
		minValue: { $ref: '#/components/schemas/SoDepthValue' },
		maxValue: { $ref: '#/components/schemas/SoDepthValue' },
	},
};

export { SoDepth, SoDepthValue };

import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

const EditableAreaProperties: OpenAPIV3.SchemaObject = {
	type: 'object',
	additionalProperties: false,
	properties: {
		featureOfInterest: {
			type: 'string',
			description: 'The Field (id) that the area belongs to.',
		},
		name: { type: 'string' },
		description: { type: 'string' },
	},
};

const AreaProperties: OpenAPIV3.SchemaObject = {
	type: 'object',
	additionalProperties: false,
	properties: {
		...EditableAreaProperties.properties,
		drawn: { type: 'boolean' },
	},
};

export { AreaProperties, EditableAreaProperties };

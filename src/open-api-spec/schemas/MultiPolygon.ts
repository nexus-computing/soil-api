import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

const MultiPolygon: OpenAPIV3.SchemaObject = {
	type: 'object',
	additionalProperties: false,
	required: ['coordinates', 'type'],
	properties: {
		type: { type: 'string', default: 'MultiPolygon', enum: ['MultiPolygon'] },
		coordinates: {
			type: 'array',
			items: {
				type: 'array',
				items: {
					type: 'array',
					items: { $ref: '#/components/schemas/Position' },
				},
			},
		},
	},
};

export default MultiPolygon;

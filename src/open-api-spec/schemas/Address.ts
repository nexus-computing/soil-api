import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

const Address: OpenAPIV3.SchemaObject = {
	type: 'object',
	additionalProperties: false,
	properties: {
		streetAddress: { type: 'string' },
		addressLocality: { type: 'string' },
		addressRegion: { type: 'string' },
		addressCountry: { type: 'string' },
		postalCode: { type: 'string' },
	},
};

export default Address;

import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

const Field: OpenAPIV3.SchemaObject = {
	type: 'object',
	required: ['name', 'areas'],
	additionalProperties: false,
	properties: {
		name: { type: 'string', minLength: 4 },
		alternateName: { type: 'string' },
		producerName: { type: 'string' },
		address: { $ref: '#/components/schemas/Address' },
		contactPoints: {
			type: 'array',
			items: { $ref: '#/components/schemas/ContactPoint' },
		},
		areas: {
			type: 'array',
			items: { type: 'string' },
			maxItems: 1,
			description: 'The Area (id) belonging to the field.',
		},
		meta: { $ref: '#/components/schemas/ResourceMeta' },
	},
};

const ResponseField: OpenAPIV3.SchemaObject = {
	...Field,
	required: ['id', 'name', 'alternateName', 'address', 'contactPoints', 'meta', 'areas'],
	properties: {
		...Field.properties,
		id: { type: 'string' },
		// TODO: Response validation should also have minLength: 4 like the request validation, but we need to solve for deletion or migration first, so for now we let previously entered fields with names shorter than 4 characters slide.
		name: { type: 'string' },
		// TODO: Response validation should also have maxItems: 1 like the request validation, but we need to solve for deletion or migration first, so for now we let previously entered fields with multiple areas slide.
		areas: {
			type: 'array',
			items: { type: 'string' },
			description: 'The Area (id) belonging to the field.',
		},
		meta: { $ref: '#/components/schemas/ResponseResourceMeta' },
	},
};

const PopulatedField: OpenAPIV3.SchemaObject = {
	...Field,
	properties: {
		...Field.properties,
		areas: {
			...Field.properties?.areas,
			items: { $ref: '#/components/schemas/Area' },
			description: 'The Area belonging to the field.',
		} as OpenAPIV3.SchemaObject,
	},
};

const EditableField: OpenAPIV3.SchemaObject = {
	type: 'object',
	additionalProperties: false,
	properties: {
		name: Field.properties?.name as OpenAPIV3.SchemaObject,
		producerName: Field.properties?.producerName as OpenAPIV3.SchemaObject,
		address: Field.properties?.address as OpenAPIV3.ReferenceObject,
		contactPoints: Field.properties?.contactPoints as OpenAPIV3.SchemaObject,
	},
};

export { EditableField, Field, PopulatedField, ResponseField };

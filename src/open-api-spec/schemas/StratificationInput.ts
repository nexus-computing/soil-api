import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

const StratificationInput: OpenAPIV3.SchemaObject = {
	type: 'object',
	additionalProperties: false,
	properties: {
		name: { type: 'string' },
		value: {} as OpenAPIV3.SchemaObject,
	},
};

export { StratificationInput };

import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

const metaReferenceIdsProperty: OpenAPIV3.SchemaObject = {
	type: 'array',
	items: {
		type: 'object',
		required: ['owner', 'id'],
		properties: {
			owner: {
				type: 'string',
				enum: [
					'heartland',
					'soilstack-draft',
					'carbofarm',
					'flurosense-field',
					'flurosense-producer',
					'flurosense-program',
					'flurosense-year',
				],
			},
			id: { type: 'string' },
		},
	},
};

const metaSurveystackProperties: { [key: string]: OpenAPIV3.SchemaObject } = {
	groupId: { type: 'string' },
	submissionId: { type: 'string' },
	submittedAt: { type: 'string', format: 'date-time' },
};

const ResourceMeta: OpenAPIV3.SchemaObject = {
	type: 'object',
	additionalProperties: false,
	properties: {
		referenceIds: metaReferenceIdsProperty,
	},
};

const ResponseResourceMeta: OpenAPIV3.SchemaObject = {
	type: 'object',
	additionalProperties: false,
	properties: {
		referenceIds: metaReferenceIdsProperty,
		...metaSurveystackProperties,
	},
};

const SurveystackOnlyResponseResourceMeta: OpenAPIV3.SchemaObject = {
	type: 'object',
	additionalProperties: false,
	properties: metaSurveystackProperties,
};

export { ResourceMeta, ResponseResourceMeta, SurveystackOnlyResponseResourceMeta };

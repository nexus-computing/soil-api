import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

const Stratification: OpenAPIV3.SchemaObject = {
	type: 'object',
	required: ['name', 'agent', 'dateCreated', 'provider', 'object', 'algorithm'],
	additionalProperties: false,
	properties: {
		name: { type: 'string' },
		agent: { type: 'string' },
		dateCreated: { type: 'string', format: 'date-time' },
		provider: { type: 'string' },
		object: {
			type: 'string',
			description: 'The Area (id) that the stratification was performed on.',
		},
		algorithm: { $ref: '#/components/schemas/Algorithm' },
		input: { type: 'array', items: { $ref: '#/components/schemas/StratificationInput' } },
		result: { type: 'array', items: { $ref: '#/components/schemas/StratificationResult' } },
	},
};

const ResponseStratification: OpenAPIV3.SchemaObject = {
	...Stratification,
	required: [
		'id',
		'name',
		'agent',
		'dateCreated',
		'provider',
		'object',
		'featureOfInterest',
		'algorithm',
		'input',
		'result',
		'meta',
	],
	properties: {
		id: { type: 'string' },
		...Stratification.properties,
		featureOfInterest: {
			type: 'string',
			description: 'The Field (id) that the area referenced by the stratification belongs to.',
		},
		meta: { $ref: '#/components/schemas/SurveystackOnlyResponseResourceMeta' },
	},
};

export { ResponseStratification, Stratification };

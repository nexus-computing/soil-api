import { OpenAPIV3 } from 'express-openapi-validator/dist/framework/types';

const ContactPoint: OpenAPIV3.SchemaObject = {
	type: 'object',
	additionalProperties: false,
	properties: {
		telephone: { type: 'string' },
		email: { type: 'string' },
		name: { type: 'string' },
		contactType: { type: 'string' },
		organization: { type: 'string' },
	},
};

export default ContactPoint;

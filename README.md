# Soil API

### Requirements
  - `node` (see version in `package.json` `engines` entry)
  - `yarn` `>=1.21.1`

### Local Development
  - `yarn`: install dependencies
  - `yarn start`: run local development server on port 3000, watching for changes
  - `yarn debug`: like `yarn start` but also exposes node debugger
  - `yarn test --watch`: run tests

### Build
  - `yarn build`: output bundle to `dist` directory

### Deploy
  - see gitlab ci/cd pipelines

### API Endpoints Documentation
  Documentation can be found at the path `/api-docs`.

  The production docs are at [api.soilstack.io/api-docs](https://api.soilstack.io/api-docs)

### Running Surveystack and Soilstack together locally
  - Run the following repositories locally. You may have to configure ports so they don't overlap between applications, and so that each application can talk to the other as appropriate.
    - [Surveystack Client and Server](https://gitlab.com/our-sci/software/surveystack)
    - [Soil API](https://gitlab.com/our-sci/software/soil-api)
    - [Soil Client](https://gitlab.com/our-sci/software/soil-client)
  - Create Survey Definitions and Seed Data
    - Create a group for soilstack in your local surveystack instance (via surveystack web app).
      - Take note of the `group path` and `group id` by looking in the DevTools Network tab for a request to `/api/memberships`. The response will be an array of membership objects. Look for `group.path` and `group._id` for the membership to the group you just created.
      - Again using the Network tab, take note of your `Authorization header value` by looking at the `Authorization` request header for any request to the surveystack api.
    - Create survey definitions in surveystack for soilstack resources:
      - Run `yarn survey-definitions-cli create`, passing the values you took note of above:
        - Run with the `-d` flag (dry run) first and confirm the values you are passing look correct. Ex: `yarn survey-definitions-cli create --group-id YOUR_GROUP_ID --group-path /your-group-path/ --auth "you@domain.tld e9245678-8e31-4bee-b357-7dc38197a78f" --api-url http://localhost:3001/api --resource all -d`
          - Note that `--api-url` here is the Surveystack server url, not the Soil API url.
        - Then run the command without the final `-d` to have the CLI create survey definitions for you.
        - Confirm via the surveystack web ui that surveys were created within the correct group.
        - Copy the example `.env` file entries from the CLI output into a `.env` file in the root of the project.
    - Create some test data
      - Run `yarn test-data-cli create`, passing the values you took note of above:
        - Run with the `-d` flag (dry run) first and confirm the values you are passing look correct. Ex: `yarn test-data-cli create --group-id YOUR_GROUP_ID --auth "you@domain.tld e9245678-8e31-4bee-b357-7dc38197a78f" --api-url http://localhost:3000 --lat 39.1358135 --lng -84.4439568 -d`
          - Pass a `lat` and `lng` value for your current location so that it will be easier to test GPS based features. The `test-data-cli` will create a field centered around the coordinates you pass.
          - Note that `--api-url` here is the Soil API url, not the Surveystack server.
        - Then run the command without the final `-d` to have the CLI create survey definitions for you.
      - Confirm your test data exists (either as submissions to surveys within the surveystack web ui, or as fields in the soilstack web ui). Make sure you are logged in as the user that corresponds with the `Authorization` header you passed to the `test-data-cli`.



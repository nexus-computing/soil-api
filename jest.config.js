module.exports = {
	verbose: true,
	testMatch: ['**/?(*.)+(spec).ts'],
	resetModules: true,
	resetMocks: true,
	moduleFileExtensions: ['js', 'ts', 'd.ts'],
	preset: 'ts-jest',
	setupFilesAfterEnv: ['<rootDir>/src/setupTests.ts', 'jest-expect-message'],
};

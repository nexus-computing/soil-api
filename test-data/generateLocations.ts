import { bbox, pointsWithinPolygon } from '@turf/turf';
import { randomPoint } from '@turf/random';
import { Polygon } from 'geojson';

function generateRandomPointsInPolygon(polygon: Polygon, count = 10) {
	const polygonBbox = bbox(polygon);
	const pointsInPolygon = [];
	let remainder = Number(String(count));
	while (pointsInPolygon.length < count) {
		const points = randomPoint(remainder, { bbox: polygonBbox });
		const ptsInPoly = pointsWithinPolygon(points, polygon);
		pointsInPolygon.push(...ptsInPoly.features.slice(0, remainder));
		remainder -= ptsInPoly.features.length;
	}
	return pointsInPolygon.map((feature) => feature.geometry);
}

export { generateRandomPointsInPolygon };

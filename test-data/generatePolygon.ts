import { createPolygon } from '../src/resources/area';
import { Polygon } from 'geojson';

function generatePolygon(lng: number, lat: number, buffer = 1e-3): Polygon {
	return createPolygon({
		coordinates: [
			[
				[lng - buffer, lat + buffer] as any,
				[lng + buffer, lat + buffer] as any,
				[lng + buffer, lat - buffer] as any,
				[lng - buffer, lat - buffer] as any,
				[lng - buffer, lat + buffer] as any,
			] as any,
		] as any,
	}) as any;
}

export { generatePolygon };
